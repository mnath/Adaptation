# Obtained from: https://github.com/lhoyer/DAFormer
# Modifications:
# - Delete tensors after usage to free GPU memory
# - Add HRDA debug visualizations
# - Support ImageNet feature distance for LR and HR predictions of HRDA
# - Add masked image consistency
# - Update debug image system
# ---------------------------------------------------------------
# Copyright (c) 2021-2022 ETH Zurich, Lukas Hoyer. All rights reserved.
# Licensed under the Apache License, Version 2.0
# ---------------------------------------------------------------

# The ema model update and the domain-mixing are based on:
# https://github.com/vikolss/DACS
# Copyright (c) 2020 vikolss. Licensed under the MIT License.
# A copy of the license is available at resources/license_dacs

from abc import ABC
import torch.nn.functional as F
from torch import Tensor
import math
import random
import tempfile
from copy import deepcopy
import torchvision.transforms as T
from matplotlib import pyplot as plt
from copy import deepcopy
import numpy as np
import torch
from torchvision import transforms
from PIL import Image
from mmseg.registry import MODELS
from mmseg.utils import (ConfigType, OptConfigType, OptMultiConfig,
                         OptSampleList, SampleList, add_prefix)
from mmseg.models import build_segmentor

import loralib as lora
from mmseg.models.segmentors import EncoderDecoder
from mmseg.registry import MODELS
import cv2
import os.path as osp
from ..builder import IDA
from mmengine.model import MMDistributedDataParallel
import torch.nn as nn
import albumentations as A
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
import numpy as np
from albumentations.pytorch import ToTensorV2
import torch.nn.functional as F
from torchvision import transforms


def extract_ampl_phase(fft_im):
    # fft_im: size should be bx3xhxwx2
    fft_amp = fft_im[:, :, :, :, 0] ** 2 + fft_im[:, :, :, :, 1] ** 2
    fft_amp = torch.sqrt(fft_amp)
    fft_pha = torch.atan2(fft_im[:, :, :, :, 1], fft_im[:, :, :, :, 0])
    return fft_amp, fft_pha


def low_freq_mutate(amp_src, amp_trg, L=0.1):
    _, _, h, w = amp_src.size()
    b = (np.floor(np.amin((h, w)) * L)).astype(int)  # get b
    amp_src[:, :, 0:b, 0:b] = amp_trg[:, :, 0:b, 0:b]  # top left
    amp_src[:, :, 0:b, w - b:w] = amp_trg[:, :, 0:b, w - b:w]  # top right
    amp_src[:, :, h - b:h, 0:b] = amp_trg[:, :, h - b:h, 0:b]  # bottom left
    amp_src[:, :, h - b:h, w - b:w] = amp_trg[:, :, h - b:h, w - b:w]  # bottom right
    return amp_src


def low_freq_mutate_np(amp_src, amp_trg, L=0.1):
    a_src = np.fft.fftshift(amp_src, axes=(-2, -1))
    a_trg = np.fft.fftshift(amp_trg, axes=(-2, -1))

    _, h, w = a_src.shape
    b = (np.floor(np.amin((h, w)) * L)).astype(int)
    c_h = np.floor(h / 2.0).astype(int)
    c_w = np.floor(w / 2.0).astype(int)

    h1 = c_h - b
    h2 = c_h + b + 1
    w1 = c_w - b
    w2 = c_w + b + 1

    a_src[:, h1:h2, w1:w2] = a_trg[:, h1:h2, w1:w2]
    a_src = np.fft.ifftshift(a_src, axes=(-2, -1))
    return a_src


def FDA_source_to_target_np(src_img, trg_img, L=0.1):
    # exchange magnitude
    # input: src_img, trg_img
    src_img = src_img.detach().cpu().numpy()
    trg_img = trg_img.detach().cpu().numpy()
    src_img_np = src_img  # .cpu().numpy()
    trg_img_np = trg_img  # .cpu().numpy()

    # get fft of both source and target
    fft_src_np = np.fft.fft2(src_img_np, axes=(-2, -1))

    fft_trg_np = np.fft.fft2(trg_img_np, axes=(-2, -1))

    # extract amplitude and phase of both ffts
    amp_src, pha_src = np.abs(fft_src_np), np.angle(fft_src_np)
    amp_trg, pha_trg = np.abs(fft_trg_np), np.angle(fft_trg_np)

    # mutate the amplitude part of source with target
    amp_src_ = low_freq_mutate_np(amp_src, amp_trg, L=L)

    # mutated fft of source
    fft_src_ = amp_src_ * np.exp(1j * pha_src)

    # get the mutated image
    src_in_trg = np.fft.ifft2(fft_src_, axes=(-2, -1))
    src_in_trg = np.real(src_in_trg)

    return src_in_trg


def np2tmp(array, temp_file_name=None):
    """Save ndarray to local numpy file.

    Args:
        array (ndarray): Ndarray to save.
        temp_file_name (str): Numpy file name. If 'temp_file_name=None', this
            function will generate a file name with tempfile.NamedTemporaryFile
            to save ndarray. Default: None.

    Returns:
        str: The numpy file name.
    """

    if temp_file_name is None:
        temp_file_name = tempfile.NamedTemporaryFile(
            suffix='.npy', delete=False).name
    np.save(temp_file_name, array)
    return temp_file_name


def _params_equal(ema_model, model):
    for ema_param, param in zip(ema_model.named_parameters(), model.named_parameters()):
        if not torch.equal(ema_param[1].data, param[1].data):
            return False
    return True


def calc_grad_magnitude(grads, norm_type=2.0):
    norm_type = float(norm_type)
    if norm_type == math.inf:
        norm = max(p.abs().max() for p in grads)
    else:
        norm = torch.norm(
            torch.stack([torch.norm(p, norm_type) for p in grads]), norm_type)
    return norm

def get_mean_std(images):
    # Reshape the images to have N * H * W pixels in total
    reshaped_images = images.view(images.size(0), images.size(1), -1)
    
    # Calculate mean and std along the batch and spatial dimensions
    mean = reshaped_images.mean(dim=(0, 2))
    std = reshaped_images.std(dim=(0, 2))
    return mean, std

def denorm(img, mean, std):
    # Assuming img is in the range [0, 1]
    mean = mean.view(1, 3, 1, 1)  # Reshape mean to match img dimensions
    std = std.view(1, 3, 1, 1)  # Reshape std to match img dimensions
    return img.mul(std).add(mean)

def renorm(img, mean, std):
    # Assuming img is normalized with mean and std, and should be in the range [0, 1]
    mean = mean.view(1, 3, 1, 1)  # Reshape mean to match img dimensions
    std = std.view(1, 3, 1, 1)  # Reshape std to match img dimensions
    return img.sub(mean).div(std)

@MODELS.register_module()
class IDASS(EncoderDecoder):

    def __init__(self, backbone, decode_head, init_cfg, threshold=0.96, ema_alpha=0.99, **kwargs):
        super().__init__(backbone=backbone, decode_head=decode_head, init_cfg=init_cfg, **kwargs)

        self.local_iter = 0
        self.threshold = threshold
        self.ema_alpha = ema_alpha

        self.teacher_backb = MODELS.build(backbone)
        self.teacher_dec = MODELS.build(decode_head)

    def _update_teacher(self, local_iter):
        alpha_teacher = min(1 - 1 / (local_iter + 1), self.ema_alpha)

        for param_name, param in self.backbone.named_parameters():
            if 'lora' in param_name:
                ema_param = self.teacher_backb.state_dict()[param_name]
                ema_param.data[:] = alpha_teacher * ema_param.data + (1 - alpha_teacher) * param.data


    def loss(self, inputs: Tensor, data_samples: SampleList) -> dict:
        """Calculate losses from a batch of inputs and data samples.

        Args:
            inputs (Tensor): Input images.
            data_samples (list[:obj:`SegDataSample`]): The seg data samples.
                It usually includes information such as `metainfo` and
                `gt_sem_seg`.

        Returns:
            dict[str, Tensor]: a dictionary of loss components
        """
        losses = dict()

        if self.local_iter == 0:
            self.backbone_source_state_dict = deepcopy(self.backbone.state_dict())
            self.decoder_source_state_dict = deepcopy(self.decode_head.state_dict())
            prefix = 'teacher_backb.'
            modified_backbone_state_dict = {prefix + k: v for k, v in self.backbone_source_state_dict.items()}
            self.teacher_backb.load_state_dict(modified_backbone_state_dict, strict=False)

            dec_prefix = 'teacher_dec.'
            modified_decoder_state_dict = {dec_prefix + k: v for k, v in self.decoder_source_state_dict.items()}
            self.teacher_dec.load_state_dict(modified_decoder_state_dict, strict=False)

            for n, p in self.teacher_backb.named_parameters():
                p.requires_grad = False

            for n, p in self.teacher_dec.named_parameters():
                p.requires_grad = False

        self.teacher_backb.eval()
        self.teacher_dec.eval()
        
        if self.local_iter > 0:
            self._update_teacher(self.local_iter)

        # get weakly augmented image
        transform = A.Compose([
            A.HorizontalFlip(p=1.0),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_weak_images = []
        transformed_weak_metas = []

        for i in range(inputs.shape[0]):
            image_data = transform(image=inputs[i].permute(1, 2, 0).detach().cpu().numpy())
            image_weak_flipped = image_data["image"].unsqueeze(0).cuda()
            transformed_weak_images.append(image_weak_flipped)
            # create meta
            image_meta_weak_flipped = deepcopy(data_samples[i].metainfo)
            image_meta_weak_flipped['flip'] = True
            image_meta_weak_flipped['flip_direction'] = 'horizontal'
            transformed_weak_metas.append(image_meta_weak_flipped)

        weak_images_batch = torch.cat(transformed_weak_images, dim=0)

        with torch.no_grad():
            # whole inference
            x = self.teacher_backb(weak_images_batch)
            logits = self.teacher_dec.predict(x ,transformed_weak_metas,self.test_cfg)
            ema_softmax = torch.softmax(logits.detach(), dim=1)

            # flip back
            flip = transformed_weak_metas[0]['flip']
            if flip:
                flip_direction = transformed_weak_metas[0]['flip_direction']
                assert flip_direction in ['horizontal', 'vertical']
                if flip_direction == 'horizontal':
                    ema_softmax = ema_softmax.flip(dims=(3,))
                elif flip_direction == 'vertical':
                    ema_softmax = ema_softmax.flip(dims=(2,))

            # compute pseudo weight
            teacher_prob, teacher_pred = torch.max(ema_softmax, dim=1)
            #import pdb; pdb.set_trace()

            ps_large_p = teacher_prob.ge(self.threshold).long() == 1
            ps_size = torch.sum(ps_large_p).item()  # Compute the total number of elements in ps_large_p
            pseudo_weight = ps_size / (inputs.shape[0] * inputs.shape[2] * inputs.shape[3])  # Compute pseudo_weight

            # select most confident predictions
            mask = teacher_prob >= self.threshold
            teacher_pred = torch.where(mask, teacher_pred, 255)
            result = teacher_pred.unsqueeze(1)
            
        del transformed_weak_images
        del weak_images_batch
        
        # get strong augmentation
        strong_transform = A.Compose([
            A.Rotate(limit=30, p=1.0),
            A.Resize(960, 540), # same as crop_size, change this when using differnt cropsize
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_strong_images = []
        results_strong = []

        # To apply Color transf we need to norm and denorm images
        img_to_transform = deepcopy(inputs)
        # denormalize
        means, stds = get_mean_std(inputs)
        img_to_transform = denorm(img_to_transform, means, stds)

        for i in range(inputs.shape[0]):

            image_data = strong_transform(image=img_to_transform[i].permute(1, 2, 0).detach().cpu().numpy(),
                                          mask=result[i].permute(1, 2, 0).detach().cpu().numpy())
    
            
            image_strong_aug = image_data["image"].unsqueeze(0).cuda()
            transformed_strong_images.append(image_strong_aug)

            masked_result = image_data["mask"].permute(2, 0, 1).unsqueeze(0).type(torch.long).cuda()
            results_strong.append(masked_result)

        
        # Detach result and move to CPU
        del result, img_to_transform

        strong_images_batch = torch.cat(transformed_strong_images, dim=0)
        pseudo_labels = torch.cat(results_strong, dim=0)
        # renormalize
        strong_images_batch = renorm(strong_images_batch, means, stds)

        for sample, pseudo_label in zip(data_samples, pseudo_labels):
            sample.gt_sem_seg.data = pseudo_label
        
        #student_losses = self.loss(strong_images_batch,data_samples)
        x = self.extract_feat(strong_images_batch)

        student_losses = dict()     

        loss_decode = self._decode_head_forward_train(x, data_samples)
        
        student_losses.update(loss_decode) 

        student_losses['decode.loss_ce'] *= pseudo_weight
        
        losses.update(student_losses)

        self.local_iter += 1

        return losses
    
# IDASS ro train for all parameters without lora
@MODELS.register_module()
class IDASS2(EncoderDecoder):

    def __init__(self, backbone, decode_head, init_cfg, threshold=0.96, ema_alpha=0.999, **kwargs):
        super().__init__(backbone=backbone, decode_head=decode_head, init_cfg=init_cfg, **kwargs)

        self.local_iter = 0
        self.threshold = threshold
        self.ema_alpha = ema_alpha

        self.teacher_backb = MODELS.build(backbone)
        self.teacher_dec = MODELS.build(decode_head)

    def _update_teacher(self, local_iter):
        alpha_teacher = min(1 - 1 / (local_iter + 1), self.ema_alpha)

        for param_name, param in self.backbone.named_parameters():
            ema_param = self.teacher_backb.state_dict()[param_name]
            ema_param.data[:] = alpha_teacher * ema_param.data + (1 - alpha_teacher) * param.data
        
        for param_name, param in self.decode_head.named_parameters():
            ema_param = self.teacher_dec.state_dict()[param_name]
            ema_param.data[:] = alpha_teacher * ema_param.data + (1 - alpha_teacher) * param.data


    def loss(self, inputs: Tensor, data_samples: SampleList) -> dict:
        """Calculate losses from a batch of inputs and data samples.

        Args:
            inputs (Tensor): Input images.
            data_samples (list[:obj:`SegDataSample`]): The seg data samples.
                It usually includes information such as `metainfo` and
                `gt_sem_seg`.

        Returns:
            dict[str, Tensor]: a dictionary of loss components
        """
        losses = dict()

        if self.local_iter == 0:
            self.backbone_source_state_dict = deepcopy(self.backbone.state_dict())
            self.decoder_source_state_dict = deepcopy(self.decode_head.state_dict())
            prefix = 'teacher_backb.'
            modified_backbone_state_dict = {prefix + k: v for k, v in self.backbone_source_state_dict.items()}
            self.teacher_backb.load_state_dict(modified_backbone_state_dict, strict=False)

            dec_prefix = 'teacher_dec.'
            modified_decoder_state_dict = {dec_prefix + k: v for k, v in self.decoder_source_state_dict.items()}
            self.teacher_dec.load_state_dict(modified_decoder_state_dict, strict=False)

        for n, p in self.teacher_backb.named_parameters():
            p.requires_grad = False

        for n, p in self.teacher_dec.named_parameters():
            p.requires_grad = False

        self.teacher_backb.eval()
        self.teacher_dec.eval()
        
        if self.local_iter > 0:
            self._update_teacher(self.local_iter)

        # get weakly augmented image
        transform = A.Compose([
            A.HorizontalFlip(p=1.0),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_weak_images = []
        transformed_weak_metas = []

        for i in range(inputs.shape[0]):
            image_data = transform(image=inputs[i].permute(1, 2, 0).detach().cpu().numpy())
            image_weak_flipped = image_data["image"].unsqueeze(0).cuda()
            transformed_weak_images.append(image_weak_flipped)
            # create meta
            image_meta_weak_flipped = deepcopy(data_samples[i].metainfo)
            image_meta_weak_flipped['flip'] = True
            image_meta_weak_flipped['flip_direction'] = 'horizontal'
            transformed_weak_metas.append(image_meta_weak_flipped)

        weak_images_batch = torch.cat(transformed_weak_images, dim=0)

        with torch.no_grad():
            # whole inference
            x = self.teacher_backb(weak_images_batch)
            logits = self.teacher_dec.predict(x ,transformed_weak_metas,self.test_cfg)
            ema_softmax = torch.softmax(logits.detach(), dim=1)

            # flip back
            flip = transformed_weak_metas[0]['flip']
            if flip:
                flip_direction = transformed_weak_metas[0]['flip_direction']
                assert flip_direction in ['horizontal', 'vertical']
                if flip_direction == 'horizontal':
                    ema_softmax = ema_softmax.flip(dims=(3,))
                elif flip_direction == 'vertical':
                    ema_softmax = ema_softmax.flip(dims=(2,))

            # compute pseudo weight
            teacher_prob, teacher_pred = torch.max(ema_softmax, dim=1)
            #import pdb; pdb.set_trace()

            ps_large_p = teacher_prob.ge(self.threshold).long() == 1
            ps_size = torch.sum(ps_large_p).item()  # Compute the total number of elements in ps_large_p
            pseudo_weight = ps_size / (inputs.shape[0] * inputs.shape[2] * inputs.shape[3])  # Compute pseudo_weight

            # select most confident predictions
            mask = teacher_prob >= self.threshold
            teacher_pred = torch.where(mask, teacher_pred, 255)
            result = teacher_pred.unsqueeze(1)
            
        del transformed_weak_images
        del weak_images_batch
        
        # get strong augmentation
        strong_transform = A.Compose([
            A.Rotate(limit=30, p=1.0),
            A.Resize(960, 540),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_strong_images = []
        results_strong = []

        # To apply Color transf we need to norm and denorm images
        img_to_transform = deepcopy(inputs)
        # denormalize
        means, stds = get_mean_std(inputs)
        img_to_transform = denorm(img_to_transform, means, stds)

        for i in range(inputs.shape[0]):

            image_data = strong_transform(image=img_to_transform[i].permute(1, 2, 0).detach().cpu().numpy(),
                                          mask=result[i].permute(1, 2, 0).detach().cpu().numpy())
    
            
            image_strong_aug = image_data["image"].unsqueeze(0).cuda()
            transformed_strong_images.append(image_strong_aug)

            masked_result = image_data["mask"].permute(2, 0, 1).unsqueeze(0).type(torch.long).cuda()
            results_strong.append(masked_result)

        
        # Detach result and move to CPU
        del result, img_to_transform

        strong_images_batch = torch.cat(transformed_strong_images, dim=0)
        pseudo_labels = torch.cat(results_strong, dim=0)
        # renormalize
        strong_images_batch = renorm(strong_images_batch, means, stds)

        for sample, pseudo_label in zip(data_samples, pseudo_labels):
            sample.gt_sem_seg.data = pseudo_label
        
        #student_losses = self.loss(strong_images_batch,data_samples)
        x = self.extract_feat(strong_images_batch)

        student_losses = dict()     

        loss_decode = self._decode_head_forward_train(x, data_samples)
        
        student_losses.update(loss_decode) 

        student_losses['decode.loss_ce'] *= pseudo_weight
        
        losses.update(student_losses)

        self.local_iter += 1

        return losses

@MODELS.register_module()
class IDASS3(EncoderDecoder):

    def __init__(self, backbone, decode_head, init_cfg, threshold=0.96, ema_alpha=0.99, **kwargs):
        super().__init__(backbone=backbone, decode_head=decode_head, init_cfg=init_cfg, **kwargs)

        self.local_iter = 0
        self.threshold = threshold
        self.ema_alpha = ema_alpha

        self.teacher_backb = MODELS.build(backbone)
        self.teacher_dec = MODELS.build(decode_head)

    def _update_teacher(self, local_iter):
        alpha_teacher = min(1 - 1 / (local_iter + 1), self.ema_alpha)

        for param_name, param in self.backbone.named_parameters():
            if 'lora' in param_name:
                ema_param = self.teacher_backb.state_dict()[param_name]
                ema_param.data[:] = alpha_teacher * ema_param.data + (1 - alpha_teacher) * param.data


    def loss(self, inputs: Tensor, data_samples: SampleList) -> dict:
        """Calculate losses from a batch of inputs and data samples.

        Args:
            inputs (Tensor): Input images.
            data_samples (list[:obj:`SegDataSample`]): The seg data samples.
                It usually includes information such as `metainfo` and
                `gt_sem_seg`.

        Returns:
            dict[str, Tensor]: a dictionary of loss components
        """
        losses = dict()

        if self.local_iter == 0:
            self.backbone_source_state_dict = deepcopy(self.backbone.state_dict())
            self.decoder_source_state_dict = deepcopy(self.decode_head.state_dict())
            prefix = 'teacher_backb.'
            modified_backbone_state_dict = {prefix + k: v for k, v in self.backbone_source_state_dict.items()}
            self.teacher_backb.load_state_dict(modified_backbone_state_dict, strict=False)

            dec_prefix = 'teacher_dec.'
            modified_decoder_state_dict = {dec_prefix + k: v for k, v in self.decoder_source_state_dict.items()}
            self.teacher_dec.load_state_dict(modified_decoder_state_dict, strict=False)

            for n, p in self.teacher_backb.named_parameters():
                p.requires_grad = False

            for n, p in self.teacher_dec.named_parameters():
                p.requires_grad = False

        self.teacher_backb.eval()
        self.teacher_dec.eval()
        
        if self.local_iter > 0:
            self._update_teacher(self.local_iter)

        img_paths = [sample.img_path for sample in data_samples] #for full images   
        new_size = (inputs.shape[3], inputs.shape[2]) 
        full_image = []
        for imgs in img_paths:
            # Read and convert the image
            img = cv2.imread(imgs)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            resized_image = cv2.resize(img, new_size)
            resized_image = resized_image.astype(np.float32) 
            resized_image = torch.tensor(resized_image).permute(2, 0, 1)  
            mean = torch.tensor([123.675, 116.28, 103.53]).view(3, 1, 1)
            std = torch.tensor([58.395, 57.12, 57.375]).view(3, 1, 1)
            normalized_image = (resized_image - mean) / std
            full_image.append(normalized_image)

        # Stack all images into a single tensor and move to GPU if available
        full_image = torch.stack(full_image, dim=0).cuda()

        # get weakly augmented image
        transform = A.Compose([
            A.HorizontalFlip(p=1.0),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_weak_images = []
        transformed_weak_metas = []

        for i in range(inputs.shape[0]):
            image_data = transform(image=full_image[i].permute(1, 2, 0).detach().cpu().numpy())
            image_weak_flipped = image_data["image"].unsqueeze(0).cuda()
            transformed_weak_images.append(image_weak_flipped)
            # create meta
            image_meta_weak_flipped = deepcopy(data_samples[i].metainfo)
            image_meta_weak_flipped['flip'] = True
            image_meta_weak_flipped['flip_direction'] = 'horizontal'
            transformed_weak_metas.append(image_meta_weak_flipped)

        weak_images_batch = torch.cat(transformed_weak_images, dim=0)

        with torch.no_grad():
            # whole inference
            x = self.teacher_backb(weak_images_batch)
            logits = self.teacher_dec.predict(x ,transformed_weak_metas,self.test_cfg)
            ema_softmax = torch.softmax(logits.detach(), dim=1)
            # flip back
            flip = transformed_weak_metas[0]['flip']
            if flip:
                flip_direction = transformed_weak_metas[0]['flip_direction']
                assert flip_direction in ['horizontal', 'vertical']
                if flip_direction == 'horizontal':
                    ema_softmax = ema_softmax.flip(dims=(3,))
                elif flip_direction == 'vertical':
                    ema_softmax = ema_softmax.flip(dims=(2,))

            # compute pseudo weight
            teacher_prob, teacher_pred = torch.max(ema_softmax, dim=1)
            

            ps_large_p = teacher_prob.ge(self.threshold).long() == 1
            ps_size = torch.sum(ps_large_p).item()  # Compute the total number of elements in ps_large_p
            pseudo_weight = ps_size / (inputs.shape[0] * inputs.shape[2] * inputs.shape[3])  # Compute pseudo_weight

            # select most confident predictions
            mask = teacher_prob >= self.threshold
            teacher_pred = torch.where(mask, teacher_pred, 255)
            result = teacher_pred.unsqueeze(1)
            
        del transformed_weak_images
        del weak_images_batch
        
        # get strong augmentation
        strong_transform = A.Compose([
            A.Rotate(limit=30, p=1.0),
            A.Resize(960, 540),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_strong_images = []
        results_strong = []

        for i in range(inputs.shape[0]):

            image_data_s = strong_transform(image=inputs[i].permute(1, 2, 0).detach().cpu().numpy(),mask=result[i].permute(1, 2, 0).detach().cpu().numpy())
      
            image_strong_aug = image_data_s["image"].unsqueeze(0).cuda()
            transformed_strong_images.append(image_strong_aug)

            masked_result = image_data_s["mask"].permute(2, 0, 1).unsqueeze(0).type(torch.long).cuda()
            results_strong.append(masked_result)

        
        # Detach result and move to CPU
        del result

        strong_images_batch = torch.cat(transformed_strong_images, dim=0)
        pseudo_labels = torch.cat(results_strong, dim=0)

        for sample, pseudo_label in zip(data_samples, pseudo_labels):
            sample.gt_sem_seg.data = pseudo_label
        
        #student_losses = self.loss(strong_images_batch,data_samples)
        x = self.extract_feat(strong_images_batch)

        student_losses = dict()     

        loss_decode = self._decode_head_forward_train(x, data_samples)
        student_losses.update(loss_decode) 

        student_losses['decode.loss_ce'] *= pseudo_weight
        
        losses.update(student_losses)

        self.local_iter += 1

        return losses
    
@MODELS.register_module()
class XXX(EncoderDecoder):

    def __init__(self, backbone, decode_head, init_cfg, alpha=0.5, **kwargs):
        super().__init__(backbone=backbone, decode_head=decode_head, init_cfg=init_cfg, **kwargs)

        self.alpha = alpha
    def loss(self, inputs: Tensor, data_samples: SampleList) -> dict:
        """Calculate losses from a batch of inputs and data samples.

        Args:
            inputs (Tensor): Input images.
            data_samples (list[:obj:`SegDataSample`]): The seg data samples.
                It usually includes information such as `metainfo` and
                `gt_sem_seg`.

        Returns:
            dict[str, Tensor]: a dictionary of loss components
        """
        
        logits = self._forward(inputs) # without postprocessing
        logits = F.interpolate(logits , size = (inputs.shape[2], inputs.shape[3]) , mode='nearest')
        preds = torch.softmax(logits , dim =1)
        preds = torch.argmax(preds , dim = 1)
        preds = preds.unsqueeze(1)

        gts = []
        for gt in data_samples:
            x = gt._gt_sem_seg.data
            gts.append(x)
        
        gt = torch.stack(gts, dim = 0).cuda()

        unfold = torch.nn.Unfold(kernel_size=(240,135), stride=(240,135))

        predx = gt.float()
        x = unfold(predx) # B (c*p*p) no.of patches
        y = x.view(preds.shape[0] , -1 , preds.shape[1],240,135) # ( B no.of patches c p p )
        batch, num_patches, channels, patch_height, patch_width = y.shape
        num_classes = 19
        z = y.view(batch * num_patches, patch_height * patch_width)
        class_dist = torch.zeros(batch * num_patches, num_classes, device=z.device)
        

        for i in range(class_dist.shape[0]):
            for n in range(num_classes):
                class_dist[i,n] = (z[i] == n ).float().sum()
        class_dist /= class_dist.sum(dim=-1, keepdim=True)


        predx = preds.float()
        x = unfold(predx) # B (c*p*p) no.of patches
        y = x.view(preds.shape[0] , -1 , preds.shape[1],240,135) # ( B no.of patches c p p )
        z = y.view(batch * num_patches, patch_height * patch_width)
        preds_dist = torch.zeros(batch * num_patches, num_classes, device=z.device)
        

        for i in range(preds_dist.shape[0]):
            for n in range(num_classes):
                preds_dist[i,n] = (z[i] == n ).float().sum()
        preds_dist /= preds_dist.sum(dim=-1, keepdim=True)

        log_dist1 = torch.log(preds_dist + 1e-10)  # Adding small value to avoid log(0)

        # Compute KL divergence for each patch using PyTorch's built-in function
        kl_divs = nn.KLDivLoss(reduction="batchmean")(log_dist1, class_dist)

        losses = dict()
        img_feats = self.extract_feat(inputs)  
        
        
        loss_decode = self._decode_head_forward_train(img_feats, data_samples)
        losses.update(loss_decode) 

        losses['kl_loss'] = kl_divs

        losses['total_loss'] = losses['decode.loss_ce'] + losses['kl_loss']
        
        losses.update(losses)
            

        return losses
    

@MODELS.register_module()
class ORACLE(EncoderDecoder):

    def __init__(self, backbone, decode_head, init_cfg,patch_rows,patch_cols, threshold=0.96, ema_alpha=0.99, **kwargs):
        super().__init__(backbone=backbone, decode_head=decode_head, init_cfg=init_cfg, **kwargs)

        self.local_iter = 0
        self.threshold = threshold
        self.ema_alpha = ema_alpha

        self.patch_rows = patch_rows
        self.patch_cols = patch_cols

        self.teacher_backb = MODELS.build(backbone)
        self.teacher_dec = MODELS.build(decode_head)

    def _update_teacher(self, local_iter):
        alpha_teacher = min(1 - 1 / (local_iter + 1), self.ema_alpha)

        for param_name, param in self.backbone.named_parameters():
            if 'lora' in param_name:
                ema_param = self.teacher_backb.state_dict()[param_name]
                ema_param.data[:] = alpha_teacher * ema_param.data + (1 - alpha_teacher) * param.data


    def loss(self, inputs: Tensor, data_samples: SampleList) -> dict:
        """Calculate losses from a batch of inputs and data samples.

        Args:
            inputs (Tensor): Input images.
            data_samples (list[:obj:`SegDataSample`]): The seg data samples.
                It usually includes information such as `metainfo` and
                `gt_sem_seg`.

        Returns:
            dict[str, Tensor]: a dictionary of loss components
        """
        losses = dict()

        if self.local_iter == 0:
            self.backbone_source_state_dict = deepcopy(self.backbone.state_dict())
            self.decoder_source_state_dict = deepcopy(self.decode_head.state_dict())
            prefix = 'teacher_backb.'
            modified_backbone_state_dict = {prefix + k: v for k, v in self.backbone_source_state_dict.items()}
            self.teacher_backb.load_state_dict(modified_backbone_state_dict, strict=False)

            dec_prefix = 'teacher_dec.'
            modified_decoder_state_dict = {dec_prefix + k: v for k, v in self.decoder_source_state_dict.items()}
            self.teacher_dec.load_state_dict(modified_decoder_state_dict, strict=False)

            for n, p in self.teacher_backb.named_parameters():
                p.requires_grad = False

            for n, p in self.teacher_dec.named_parameters():
                p.requires_grad = False

        self.teacher_backb.eval()
        self.teacher_dec.eval()
        
        if self.local_iter > 0:
            self._update_teacher(self.local_iter)

        # get weakly augmented image
        transform = A.Compose([
            A.HorizontalFlip(p=1.0),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_weak_images = []
        transformed_weak_metas = []

        for i in range(inputs.shape[0]):
            image_data = transform(image=inputs[i].permute(1, 2, 0).detach().cpu().numpy())
            image_weak_flipped = image_data["image"].unsqueeze(0).cuda()
            transformed_weak_images.append(image_weak_flipped)
            # create meta
            image_meta_weak_flipped = deepcopy(data_samples[i].metainfo)
            image_meta_weak_flipped['flip'] = True
            image_meta_weak_flipped['flip_direction'] = 'horizontal'
            transformed_weak_metas.append(image_meta_weak_flipped)

        weak_images_batch = torch.cat(transformed_weak_images, dim=0)

        with torch.no_grad():
            # whole inference
            x = self.teacher_backb(weak_images_batch)
            logits = self.teacher_dec.predict(x ,transformed_weak_metas,self.test_cfg)
            ema_softmax = torch.softmax(logits.detach(), dim=1)

            # flip back
            flip = transformed_weak_metas[0]['flip']
            if flip:
                flip_direction = transformed_weak_metas[0]['flip_direction']
                assert flip_direction in ['horizontal', 'vertical']
                if flip_direction == 'horizontal':
                    ema_softmax = ema_softmax.flip(dims=(3,))
                elif flip_direction == 'vertical':
                    ema_softmax = ema_softmax.flip(dims=(2,))

            # compute pseudo weight
            teacher_prob, teacher_pred = torch.max(ema_softmax, dim=1)
            #import pdb; pdb.set_trace()

            ps_large_p = teacher_prob.ge(self.threshold).long() == 1
            ps_size = torch.sum(ps_large_p).item()  # Compute the total number of elements in ps_large_p
            pseudo_weight = ps_size / (inputs.shape[0] * inputs.shape[2] * inputs.shape[3])  # Compute pseudo_weight

            # select most confident predictions
            mask = teacher_prob >= self.threshold
            teacher_pred = torch.where(mask, teacher_pred, 255)
            result = teacher_pred.unsqueeze(1)
            
        del transformed_weak_images
        del weak_images_batch
        
        # get strong augmentation
        strong_transform = A.Compose([
            A.Rotate(limit=30, p=1.0),
            A.Resize(960, 540),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_strong_images = []
        results_strong = []

        # To apply Color transf we need to norm and denorm images
        img_to_transform = deepcopy(inputs)
        # denormalize
        means, stds = get_mean_std(inputs)
        img_to_transform = denorm(img_to_transform, means, stds)

        for i in range(inputs.shape[0]):

            image_data = strong_transform(image=img_to_transform[i].permute(1, 2, 0).detach().cpu().numpy(),
                                          mask=result[i].permute(1, 2, 0).detach().cpu().numpy())
    
            image_strong_aug = image_data["image"].unsqueeze(0).cuda()
            transformed_strong_images.append(image_strong_aug)

            masked_result = image_data["mask"].permute(2, 0, 1).unsqueeze(0).type(torch.long).cuda()
            results_strong.append(masked_result)

        # Detach result and move to CPU
        del result, img_to_transform

        strong_images_batch = torch.cat(transformed_strong_images, dim=0)
        pseudo_labels = torch.cat(results_strong, dim=0)
        # renormalize
        strong_images_batch = renorm(strong_images_batch, means, stds)

        for sample, pseudo_label in zip(data_samples, pseudo_labels):
            sample.gt_sem_seg.data = pseudo_label
        
        #student_losses = self.loss(strong_images_batch,data_samples)
        x = self.extract_feat(strong_images_batch)
        loss_decode = self._decode_head_forward_train(x, data_samples)
        losses.update(loss_decode) 
        losses['decode.loss_ce'] *= pseudo_weight

        #### KL PART ####
        
        num_classes = 19

        img_paths = [sample.img_path for sample in data_samples] #for full images   
        new_size = (inputs.shape[2], inputs.shape[3]) # for opencv its reversed

        full_image = []
        for imgs in img_paths:
            # Read and convert the image
            img = cv2.imread(imgs)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            resized_image = cv2.resize(img, new_size)
            resized_image = resized_image.astype(np.float32) 
            resized_image = torch.tensor(resized_image).permute(2, 0, 1)  
            mean = torch.tensor([123.675, 116.28, 103.53]).view(3, 1, 1)
            std = torch.tensor([58.395, 57.12, 57.375]).view(3, 1, 1)
            normalized_image = (resized_image - mean) / std
            full_image.append(normalized_image)

        # Stack all images into a single tensor and move to GPU if available
        full_image = torch.stack(full_image, dim=0).cuda()

        logits = self._forward(full_image) # without postprocessing from student
        logits = F.interpolate(logits , size = (inputs.shape[3], inputs.shape[2]) , mode='nearest')
        preds = torch.softmax(logits , dim =1)
        preds = torch.argmax(preds , dim = 1)
        preds = preds.unsqueeze(1)

        seg_paths = [sample.seg_map_path for sample in data_samples] #for full images
        gts = []
        for imgs in seg_paths:
            # Read and convert the image
            img = cv2.imread(imgs)
            img = img[: ,:,0] # only taking 1 channel
            resized_image = cv2.resize(img, new_size) 
            resized_image = torch.tensor(resized_image).unsqueeze(0)
            gts.append(resized_image)

        # Stack all images into a single tensor and move to GPU if available
        gt = torch.stack(gts, dim=0).cuda()

        
        
        unfold = torch.nn.Unfold(kernel_size=(inputs.shape[3] // self.patch_rows, inputs.shape[2] // self.patch_cols), stride=(inputs.shape[3] // self.patch_rows, inputs.shape[2] // self.patch_cols))
        
        predx = gt.float()
        x = unfold(predx)  # B (c*p*p) no.of patches
        bs, c, h, w = predx.shape
        patch_h, patch_w = inputs.shape[3] // self.patch_rows, inputs.shape[2] // self.patch_cols
        num_patches = (h // patch_h) * (w // patch_w)
        a = x.reshape(bs, c, patch_h, patch_w, num_patches).permute(0, 4, 1, 2, 3)

        '''aa = a[0].cpu()  # Shape: [4, 1, 270, 480]
        fig = plt.figure(figsize=(8, 8))
        grid = ImageGrid(fig, 111, nrows_ncols=(8, 8), axes_pad=0.1)

        # Assuming y is a segmentation map with shape [4, 1, 270, 480]
        for i, ax in enumerate(grid):
            patch = aa[i, 0].numpy()  # Shape: [270, 480]
            ax.imshow(patch, cmap='jet')  # Use 'jet' colormap or any other preferred colormap
            ax.axis('off')

        plt.savefig('ex.png')'''

        # a -> ( B no.of patches c p p )
        batch, num_patches, channels, patch_height, patch_width = a.shape
        z = a.reshape(batch * num_patches, patch_height * patch_width)

        class_dist = torch.zeros(batch * num_patches, num_classes, device=z.device)
        
        for i in range(class_dist.shape[0]):
            for n in range(num_classes):
                class_dist[i,n] = (z[i] == n ).float().sum()
        class_dist /= class_dist.sum(dim=-1, keepdim=True)

        predx = preds.float()
        x = unfold(predx) # B (c*p*p) no.of patches
        bs, c, h, w = predx.shape
        patch_h, patch_w = inputs.shape[3] // self.patch_rows, inputs.shape[2] // self.patch_cols
        num_patches = (h // patch_h) * (w // patch_w)
        a = x.reshape(bs, c, patch_h, patch_w, num_patches).permute(0, 4, 1, 2, 3)
        # a -> ( B no.of patches c p p )
        batch, num_patches, channels, patch_height, patch_width = a.shape
        z = a.reshape(batch * num_patches, patch_height * patch_width)

        preds_dist = torch.zeros(batch * num_patches, num_classes, device=z.device)
        for i in range(preds_dist.shape[0]):
            for n in range(num_classes):
                preds_dist[i,n] = (z[i] == n ).float().sum()
        preds_dist /= preds_dist.sum(dim=-1, keepdim=True)

        log_dist1 = torch.log(preds_dist + 1e-12)  # Adding small value to avoid log(0)

        # Compute KL divergence 
        kl_divs = nn.KLDivLoss(reduction="none")(log_dist1, class_dist) ####### no. of patches x no. of classes
        #import pdb;pdb.set_trace()
        patch_kl_divs = kl_divs.sum(dim=1) # no. of patches x 1
        #weighted_kl = patch_kl_divs * fog_ent # entropy weighting
        mean_kl_div = patch_kl_divs.mean()

        losses['kl_loss'] = mean_kl_div

        losses['total_loss'] = losses['decode.loss_ce'] + 0.5*losses['kl_loss']

        #'import pdb;pdb.set_trace()
        
        self.local_iter += 1

        return losses
    
@MODELS.register_module()
class ORACLE2(EncoderDecoder):

    def __init__(self, backbone, decode_head, init_cfg,patch_rows,patch_cols, threshold=0.96, ema_alpha=0.99, **kwargs):
        super().__init__(backbone=backbone, decode_head=decode_head, init_cfg=init_cfg, **kwargs)

        self.local_iter = 0
        self.threshold = threshold
        self.ema_alpha = ema_alpha

        self.patch_rows = patch_rows
        self.patch_cols = patch_cols

        self.teacher_backb = MODELS.build(backbone)
        self.teacher_dec = MODELS.build(decode_head)

    def _update_teacher(self, local_iter):
        alpha_teacher = min(1 - 1 / (local_iter + 1), self.ema_alpha)

        for param_name, param in self.backbone.named_parameters():
            if 'lora' in param_name:
                ema_param = self.teacher_backb.state_dict()[param_name]
                ema_param.data[:] = alpha_teacher * ema_param.data + (1 - alpha_teacher) * param.data

    def loss(self, inputs: Tensor, data_samples: SampleList) -> dict:
        """Calculate losses from a batch of inputs and data samples.

        Args:
            inputs (Tensor): Input images.
            data_samples (list[:obj:`SegDataSample`]): The seg data samples.
                It usually includes information such as `metainfo` and
                `gt_sem_seg`.

        Returns:
            dict[str, Tensor]: a dictionary of loss components
        """
        losses = dict()

        if self.local_iter == 0:
            self.backbone_source_state_dict = deepcopy(self.backbone.state_dict())
            self.decoder_source_state_dict = deepcopy(self.decode_head.state_dict())
            prefix = 'teacher_backb.'
            modified_backbone_state_dict = {prefix + k: v for k, v in self.backbone_source_state_dict.items()}
            self.teacher_backb.load_state_dict(modified_backbone_state_dict, strict=False)

            dec_prefix = 'teacher_dec.'
            modified_decoder_state_dict = {dec_prefix + k: v for k, v in self.decoder_source_state_dict.items()}
            self.teacher_dec.load_state_dict(modified_decoder_state_dict, strict=False)

            for n, p in self.teacher_backb.named_parameters():
                p.requires_grad = False

            for n, p in self.teacher_dec.named_parameters():
                p.requires_grad = False

        self.teacher_backb.eval()
        self.teacher_dec.eval()
        
        if self.local_iter > 0:
            self._update_teacher(self.local_iter)

        # get weakly augmented image
        transform = A.Compose([
            A.HorizontalFlip(p=1.0),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_weak_images = []
        transformed_weak_metas = []

        for i in range(inputs.shape[0]):
            image_data = transform(image=inputs[i].permute(1, 2, 0).detach().cpu().numpy())
            image_weak_flipped = image_data["image"].unsqueeze(0).cuda()
            transformed_weak_images.append(image_weak_flipped)
            # create meta
            image_meta_weak_flipped = deepcopy(data_samples[i].metainfo)
            image_meta_weak_flipped['flip'] = True
            image_meta_weak_flipped['flip_direction'] = 'horizontal'
            transformed_weak_metas.append(image_meta_weak_flipped)

        weak_images_batch = torch.cat(transformed_weak_images, dim=0)

        with torch.no_grad():
            # whole inference
            x = self.teacher_backb(weak_images_batch)
            logits = self.teacher_dec.predict(x ,transformed_weak_metas,self.test_cfg)
            ema_softmax = torch.softmax(logits.detach(), dim=1)

            # flip back
            flip = transformed_weak_metas[0]['flip']
            if flip:
                flip_direction = transformed_weak_metas[0]['flip_direction']
                assert flip_direction in ['horizontal', 'vertical']
                if flip_direction == 'horizontal':
                    ema_softmax = ema_softmax.flip(dims=(3,))
                elif flip_direction == 'vertical':
                    ema_softmax = ema_softmax.flip(dims=(2,))

            # compute pseudo weight
            teacher_prob, teacher_pred = torch.max(ema_softmax, dim=1)
            #import pdb; pdb.set_trace()

            ps_large_p = teacher_prob.ge(self.threshold).long() == 1
            ps_size = torch.sum(ps_large_p).item()  # Compute the total number of elements in ps_large_p
            pseudo_weight = ps_size / (inputs.shape[0] * inputs.shape[2] * inputs.shape[3])  # Compute pseudo_weight

            # select most confident predictions
            mask = teacher_prob >= self.threshold
            teacher_pred = torch.where(mask, teacher_pred, 255)
            result = teacher_pred.unsqueeze(1)
            
        del transformed_weak_images
        del weak_images_batch
        
        # get strong augmentation
        strong_transform = A.Compose([
            A.Rotate(limit=30, p=1.0),
            A.Resize(960, 540),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_strong_images = []
        results_strong = []

        # To apply Color transf we need to norm and denorm images
        img_to_transform = deepcopy(inputs)
        # denormalize
        means, stds = get_mean_std(inputs)
        img_to_transform = denorm(img_to_transform, means, stds)

        for i in range(inputs.shape[0]):

            image_data = strong_transform(image=img_to_transform[i].permute(1, 2, 0).detach().cpu().numpy(),
                                          mask=result[i].permute(1, 2, 0).detach().cpu().numpy())
            
            image_strong_aug = image_data["image"].unsqueeze(0).cuda()
            transformed_strong_images.append(image_strong_aug)

            masked_result = image_data["mask"].permute(2, 0, 1).unsqueeze(0).type(torch.long).cuda()
            results_strong.append(masked_result)

        # Detach result and move to CPU
        del result, img_to_transform

        strong_images_batch = torch.cat(transformed_strong_images, dim=0)
        pseudo_labels = torch.cat(results_strong, dim=0)
        # renormalize
        strong_images_batch = renorm(strong_images_batch, means, stds)

        for sample, pseudo_label in zip(data_samples, pseudo_labels):
            sample.gt_sem_seg.data = pseudo_label
        
        #student_losses = self.loss(strong_images_batch,data_samples)
        x = self.extract_feat(strong_images_batch)
        loss_decode = self._decode_head_forward_train(x, data_samples)
        losses.update(loss_decode) 
        losses['decode.loss_ce'] *= pseudo_weight

        #### KL PART ####
        
        num_classes = 19
        unfold = torch.nn.Unfold(kernel_size=(inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols), stride=(inputs.shape[2] // self.patch_rows, inputs.shape[3]// self.patch_cols))
        
        img_paths = [sample.img_path for sample in data_samples] #for full images
        
        new_size = (inputs.shape[3], inputs.shape[2]) # for opencv its reversed
        full_image = []
        for imgs in img_paths:
            # Read and convert the image
            img = cv2.imread(imgs)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            
            # Resize the image
            resized_image = cv2.resize(img, new_size)
            
            # Convert to float32 and normalize the image
            resized_image = resized_image.astype(np.float32)  # Convert to float32 numpy array
            resized_image = torch.tensor(resized_image).permute(2, 0, 1)  # Convert to PyTorch tensor and rearrange dimensions
            
            # Define mean and std for normalization
            mean = torch.tensor([123.675, 116.28, 103.53]).view(3, 1, 1)
            std = torch.tensor([58.395, 57.12, 57.375]).view(3, 1, 1)
            
            # Normalize the image
            normalized_image = (resized_image - mean) / std
    
            # Add to list
            full_image.append(normalized_image)

        # Stack all images into a single tensor and move to GPU if available
        full_image = torch.stack(full_image, dim=0).cuda()

        #Teacher predictions of weakaugs as pseudo label
        teacher_preds = teacher_pred.unsqueeze(1)
        teacher_preds = teacher_preds.float()
        teacher_preds = F.interpolate(teacher_preds , size = (inputs.shape[2], inputs.shape[3]) , mode='nearest')
        predx = teacher_preds
        x = unfold(predx)  # B (c*p*p) no.of patches
        bs, c, h, w = predx.shape
        patch_h, patch_w = inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols
        num_patches = (h // patch_h) * (w // patch_w)
        a = x.reshape(bs, c, patch_h, patch_w, num_patches).permute(0, 4, 1, 2, 3)
        # a -> ( B no.of patches c p p )
        batch, num_patches, channels, patch_height, patch_width = a.shape
        z = a.reshape(batch * num_patches, patch_height * patch_width)

        class_dist = torch.zeros(batch * num_patches, num_classes, device=z.device)
        
        for i in range(class_dist.shape[0]):
            for n in range(num_classes):
                class_dist[i,n] = (z[i] == n ).float().sum()
        class_dist /= class_dist.sum(dim=-1, keepdim=True)

        # Sudent predictions
        logits = self._forward(inputs) # without postprocessing from student
        logits = F.interpolate(logits , size = (inputs.shape[2], inputs.shape[3]) , mode='nearest')
        preds = torch.softmax(logits , dim = 1)
        preds = torch.argmax(preds , dim = 1)
        preds = preds.unsqueeze(1)
        predx = preds.float()
        x = unfold(predx) # B (c*p*p) no.of patches
        bs, c, h, w = predx.shape
        patch_h, patch_w = inputs.shape[2]// self.patch_rows, inputs.shape[3] // self.patch_cols
        num_patches = (h // patch_h) * (w // patch_w)
        a = x.reshape(bs, c, patch_h, patch_w, num_patches).permute(0, 4, 1, 2, 3)
        # a -> ( B no.of patches c p p )
        batch, num_patches, channels, patch_height, patch_width = a.shape
        z = a.reshape(batch * num_patches, patch_height * patch_width)

        preds_dist = torch.zeros(batch * num_patches, num_classes, device=z.device)
        
        for i in range(preds_dist.shape[0]):
            for n in range(num_classes):
                preds_dist[i,n] = (z[i] == n ).float().sum()
        preds_dist /= preds_dist.sum(dim=-1, keepdim=True)

        log_dist1 = torch.log(preds_dist + 1e-12)  # Adding small value to avoid log(0)

        # Compute KL divergence for each patch using PyTorch's built-in function
        kl_divs = nn.KLDivLoss(reduction="none")(log_dist1, class_dist) #######

        losses['kl_loss'] = kl_divs

        losses['total_loss'] = losses['decode.loss_ce'] + 0.5*losses['kl_loss']

        #'import pdb;pdb.set_trace()
        
        self.local_iter += 1

        return losses
    
@MODELS.register_module()
class ORACLE3(EncoderDecoder):

    def __init__(self, backbone, decode_head, init_cfg,patch_rows,patch_cols, threshold=0.96, ema_alpha=0.99, **kwargs):
        super().__init__(backbone=backbone, decode_head=decode_head, init_cfg=init_cfg, **kwargs)

        self.local_iter = 0
        self.threshold = threshold
        self.ema_alpha = ema_alpha

        self.patch_rows = patch_rows
        self.patch_cols = patch_cols

        self.teacher_backb = MODELS.build(backbone)
        self.teacher_dec = MODELS.build(decode_head)

    def _update_teacher(self, local_iter):
        alpha_teacher = min(1 - 1 / (local_iter + 1), self.ema_alpha)

        for param_name, param in self.backbone.named_parameters():
            if 'lora' in param_name:
                ema_param = self.teacher_backb.state_dict()[param_name]
                ema_param.data[:] = alpha_teacher * ema_param.data + (1 - alpha_teacher) * param.data

    def loss(self, inputs: Tensor, data_samples: SampleList) -> dict:
        """Calculate losses from a batch of inputs and data samples.

        Args:
            inputs (Tensor): Input images.
            data_samples (list[:obj:`SegDataSample`]): The seg data samples.
                It usually includes information such as `metainfo` and
                `gt_sem_seg`.

        Returns:
            dict[str, Tensor]: a dictionary of loss components
        """
        losses = dict()

        if self.local_iter == 0:
            self.backbone_source_state_dict = deepcopy(self.backbone.state_dict())
            self.decoder_source_state_dict = deepcopy(self.decode_head.state_dict())
            prefix = 'teacher_backb.'
            modified_backbone_state_dict = {prefix + k: v for k, v in self.backbone_source_state_dict.items()}
            self.teacher_backb.load_state_dict(modified_backbone_state_dict, strict=False)

            dec_prefix = 'teacher_dec.'
            modified_decoder_state_dict = {dec_prefix + k: v for k, v in self.decoder_source_state_dict.items()}
            self.teacher_dec.load_state_dict(modified_decoder_state_dict, strict=False)

            for n, p in self.teacher_backb.named_parameters():
                p.requires_grad = False

            for n, p in self.teacher_dec.named_parameters():
                p.requires_grad = False

        self.teacher_backb.eval()
        self.teacher_dec.eval()
        
        if self.local_iter > 0:
            self._update_teacher(self.local_iter)

        # get weakly augmented image
        transform = A.Compose([
            A.HorizontalFlip(p=1.0),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_weak_images = []
        transformed_weak_metas = []

        for i in range(inputs.shape[0]):
            image_data = transform(image=inputs[i].permute(1, 2, 0).detach().cpu().numpy())
            image_weak_flipped = image_data["image"].unsqueeze(0).cuda()
            transformed_weak_images.append(image_weak_flipped)
            # create meta
            image_meta_weak_flipped = deepcopy(data_samples[i].metainfo)
            image_meta_weak_flipped['flip'] = True
            image_meta_weak_flipped['flip_direction'] = 'horizontal'
            transformed_weak_metas.append(image_meta_weak_flipped)

        weak_images_batch = torch.cat(transformed_weak_images, dim=0)

        with torch.no_grad():
            # whole inference
            x = self.teacher_backb(weak_images_batch)
            logits = self.teacher_dec.predict(x ,transformed_weak_metas,self.test_cfg)
            ema_softmax = torch.softmax(logits.detach(), dim=1)

            # flip back
            flip = transformed_weak_metas[0]['flip']
            if flip:
                flip_direction = transformed_weak_metas[0]['flip_direction']
                assert flip_direction in ['horizontal', 'vertical']
                if flip_direction == 'horizontal':
                    ema_softmax = ema_softmax.flip(dims=(3,))
                elif flip_direction == 'vertical':
                    ema_softmax = ema_softmax.flip(dims=(2,))

            # compute pseudo weight
            teacher_prob, teacher_pred = torch.max(ema_softmax, dim=1)
            #import pdb; pdb.set_trace()

            ps_large_p = teacher_prob.ge(self.threshold).long() == 1
            ps_size = torch.sum(ps_large_p).item()  # Compute the total number of elements in ps_large_p
            pseudo_weight = ps_size / (inputs.shape[0] * inputs.shape[2] * inputs.shape[3])  # Compute pseudo_weight

            # select most confident predictions
            mask = teacher_prob >= self.threshold
            teacher_pred = torch.where(mask, teacher_pred, 255)
            result = teacher_pred.unsqueeze(1)
            
        del transformed_weak_images
        del weak_images_batch
        
        # get strong augmentation
        strong_transform = A.Compose([
            A.Rotate(limit=30, p=1.0),
            A.Resize(960, 540),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_strong_images = []
        results_strong = []

        # To apply Color transf we need to norm and denorm images
        img_to_transform = deepcopy(inputs)
        # denormalize
        means, stds = get_mean_std(inputs)
        img_to_transform = denorm(img_to_transform, means, stds)

        for i in range(inputs.shape[0]):

            image_data = strong_transform(image=img_to_transform[i].permute(1, 2, 0).detach().cpu().numpy(),
                                          mask=result[i].permute(1, 2, 0).detach().cpu().numpy())
    
            image_strong_aug = image_data["image"].unsqueeze(0).cuda()
            transformed_strong_images.append(image_strong_aug)

            masked_result = image_data["mask"].permute(2, 0, 1).unsqueeze(0).type(torch.long).cuda()
            results_strong.append(masked_result)

        # Detach result and move to CPU
        del result, img_to_transform

        strong_images_batch = torch.cat(transformed_strong_images, dim=0)
        pseudo_labels = torch.cat(results_strong, dim=0)
        # renormalize
        strong_images_batch = renorm(strong_images_batch, means, stds)

        for sample, pseudo_label in zip(data_samples, pseudo_labels):
            sample.gt_sem_seg.data = pseudo_label
        
        #student_losses = self.loss(strong_images_batch,data_samples)
        x = self.extract_feat(strong_images_batch)
        loss_decode = self._decode_head_forward_train(x, data_samples)
        losses.update(loss_decode) 
        losses['decode.loss_ce'] *= pseudo_weight

        #### KL PART ####
        
        num_classes = 19

        unfold = torch.nn.Unfold(kernel_size=(inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols), stride=(inputs.shape[2] // self.patch_rows, inputs.shape[3]// self.patch_cols))
        
        # Teacher labels of the noaug images
        with torch.no_grad():
            logits = self.teacher_backb(inputs) # without postprocessing from student
            logits = self.teacher_dec.forward(logits)
            logits = F.interpolate(logits , size = (inputs.shape[2], inputs.shape[3]) , mode='nearest')
            preds = torch.softmax(logits , dim = 1)
            preds = torch.argmax(preds , dim = 1)
            preds = preds.unsqueeze(1)
            predx = preds.float()
            x = unfold(predx)

        bs, c, h, w = predx.shape
        patch_h, patch_w = inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols
        num_patches = (h // patch_h) * (w // patch_w)
        a = x.reshape(bs, c, patch_h, patch_w, num_patches).permute(0, 4, 1, 2, 3)
        # a -> ( B no.of patches c p p )
        batch, num_patches, channels, patch_height, patch_width = a.shape
        z = a.reshape(batch * num_patches, patch_height * patch_width)
        class_dist = torch.zeros(batch * num_patches, num_classes, device=z.device)
        
        for i in range(class_dist.shape[0]):
            for n in range(num_classes):
                class_dist[i,n] = (z[i] == n ).float().sum()
        class_dist /= class_dist.sum(dim=-1, keepdim=True)

        #####Student predictions
        logits = self._forward(inputs) # without postprocessing from student
        logits = F.interpolate(logits , size = (inputs.shape[2], inputs.shape[3]) , mode='nearest')
        preds = torch.softmax(logits , dim = 1)
        preds = torch.argmax(preds , dim = 1)
        preds = preds.unsqueeze(1)
        predx = preds.float()
        x = unfold(predx) # B (c*p*p) no.of patches
        bs, c, h, w = predx.shape
        patch_h, patch_w = inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols
        num_patches = (h // patch_h) * (w // patch_w)
        a = x.reshape(bs, c, patch_h, patch_w, num_patches).permute(0, 4, 1, 2, 3)
        # a -> ( B no.of patches c p p )
        batch, num_patches, channels, patch_height, patch_width = a.shape
        z = a.reshape(batch * num_patches, patch_height * patch_width)

        preds_dist = torch.zeros(batch * num_patches, num_classes, device=z.device)
        
        for i in range(preds_dist.shape[0]):
            for n in range(num_classes):
                preds_dist[i,n] = (z[i] == n ).float().sum()
        preds_dist /= preds_dist.sum(dim=-1, keepdim=True)

        log_dist1 = torch.log(preds_dist + 1e-12)  # Adding small value to avoid log(0)

        # Compute KL divergence for each patch using PyTorch's built-in function
        kl_divs = nn.KLDivLoss(reduction="none")(log_dist1, class_dist) #######
        losses['kl_loss'] = kl_divs

        losses['total_loss'] = losses['decode.loss_ce'] + 0.5*losses['kl_loss']

        #'import pdb;pdb.set_trace()
        
        self.local_iter += 1

        return losses
    
@MODELS.register_module()
class ORACLE4(EncoderDecoder):

    def __init__(self, backbone, decode_head, init_cfg,patch_rows,patch_cols, threshold=0.96, ema_alpha=0.99, **kwargs):
        super().__init__(backbone=backbone, decode_head=decode_head, init_cfg=init_cfg, **kwargs)

        self.local_iter = 0
        self.threshold = threshold
        self.ema_alpha = ema_alpha

        self.patch_rows = patch_rows
        self.patch_cols = patch_cols

        self.teacher_backb = MODELS.build(backbone)
        self.teacher_dec = MODELS.build(decode_head)

    def _update_teacher(self, local_iter):
        alpha_teacher = min(1 - 1 / (local_iter + 1), self.ema_alpha)

        for param_name, param in self.backbone.named_parameters():
            if 'lora' in param_name:
                ema_param = self.teacher_backb.state_dict()[param_name]
                ema_param.data[:] = alpha_teacher * ema_param.data + (1 - alpha_teacher) * param.data

    def loss(self, inputs: Tensor, data_samples: SampleList) -> dict:
        """Calculate losses from a batch of inputs and data samples.

        Args:
            inputs (Tensor): Input images.
            data_samples (list[:obj:`SegDataSample`]): The seg data samples.
                It usually includes information such as `metainfo` and
                `gt_sem_seg`.

        Returns:
            dict[str, Tensor]: a dictionary of loss components
        """
        losses = dict()

        if self.local_iter == 0:
            self.backbone_source_state_dict = deepcopy(self.backbone.state_dict())
            self.decoder_source_state_dict = deepcopy(self.decode_head.state_dict())
            prefix = 'teacher_backb.'
            modified_backbone_state_dict = {prefix + k: v for k, v in self.backbone_source_state_dict.items()}
            self.teacher_backb.load_state_dict(modified_backbone_state_dict, strict=False)

            dec_prefix = 'teacher_dec.'
            modified_decoder_state_dict = {dec_prefix + k: v for k, v in self.decoder_source_state_dict.items()}
            self.teacher_dec.load_state_dict(modified_decoder_state_dict, strict=False)

            for n, p in self.teacher_backb.named_parameters():
                p.requires_grad = False

            for n, p in self.teacher_dec.named_parameters():
                p.requires_grad = False

        self.teacher_backb.eval()
        self.teacher_dec.eval()
        
        if self.local_iter > 0:
            self._update_teacher(self.local_iter)

        # get weakly augmented image
        transform = A.Compose([
            A.HorizontalFlip(p=1.0),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_weak_images = []
        transformed_weak_metas = []

        for i in range(inputs.shape[0]):
            image_data = transform(image=inputs[i].permute(1, 2, 0).detach().cpu().numpy())
            image_weak_flipped = image_data["image"].unsqueeze(0).cuda()
            transformed_weak_images.append(image_weak_flipped)
            # create meta
            image_meta_weak_flipped = deepcopy(data_samples[i].metainfo)
            image_meta_weak_flipped['flip'] = True
            image_meta_weak_flipped['flip_direction'] = 'horizontal'
            transformed_weak_metas.append(image_meta_weak_flipped)

        weak_images_batch = torch.cat(transformed_weak_images, dim=0)

        with torch.no_grad():
            # whole inference
            x = self.teacher_backb(weak_images_batch)
            logits = self.teacher_dec.predict(x ,transformed_weak_metas,self.test_cfg)
            ema_softmax = torch.softmax(logits.detach(), dim=1)

            # flip back
            flip = transformed_weak_metas[0]['flip']
            if flip:
                flip_direction = transformed_weak_metas[0]['flip_direction']
                assert flip_direction in ['horizontal', 'vertical']
                if flip_direction == 'horizontal':
                    ema_softmax = ema_softmax.flip(dims=(3,))
                elif flip_direction == 'vertical':
                    ema_softmax = ema_softmax.flip(dims=(2,))

            # compute pseudo weight
            teacher_prob, teacher_pred = torch.max(ema_softmax, dim=1)
            #import pdb; pdb.set_trace()

            ps_large_p = teacher_prob.ge(self.threshold).long() == 1
            ps_size = torch.sum(ps_large_p).item()  # Compute the total number of elements in ps_large_p
            pseudo_weight = ps_size / (inputs.shape[0] * inputs.shape[2] * inputs.shape[3])  # Compute pseudo_weight

            # select most confident predictions
            mask = teacher_prob >= self.threshold
            teacher_pred = torch.where(mask, teacher_pred, 255)
            result = teacher_pred.unsqueeze(1)
            
        del transformed_weak_images
        del weak_images_batch
        
        # get strong augmentation
        strong_transform = A.Compose([
            A.Rotate(limit=30, p=1.0),
            A.Resize(960, 540),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_strong_images = []
        results_strong = []

        # To apply Color transf we need to norm and denorm images
        img_to_transform = deepcopy(inputs)
        # denormalize
        means, stds = get_mean_std(inputs)
        img_to_transform = denorm(img_to_transform, means, stds)

        for i in range(inputs.shape[0]):

            image_data = strong_transform(image=img_to_transform[i].permute(1, 2, 0).detach().cpu().numpy(),
                                          mask=result[i].permute(1, 2, 0).detach().cpu().numpy())
    
            image_strong_aug = image_data["image"].unsqueeze(0).cuda()
            transformed_strong_images.append(image_strong_aug)

            masked_result = image_data["mask"].permute(2, 0, 1).unsqueeze(0).type(torch.long).cuda()
            results_strong.append(masked_result)

        # Detach result and move to CPU
        del result, img_to_transform

        strong_images_batch = torch.cat(transformed_strong_images, dim=0)
        pseudo_labels = torch.cat(results_strong, dim=0)
        # renormalize
        strong_images_batch = renorm(strong_images_batch, means, stds)

        for sample, pseudo_label in zip(data_samples, pseudo_labels):
            sample.gt_sem_seg.data = pseudo_label
        
        #student_losses = self.loss(strong_images_batch,data_samples)
        x = self.extract_feat(strong_images_batch)
        loss_decode = self._decode_head_forward_train(x, data_samples)
        losses.update(loss_decode) 
        losses['decode.loss_ce'] *= pseudo_weight

        #### KL PART ####
        
        num_classes = 19

        unfold = torch.nn.Unfold(kernel_size=(inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols), stride=(inputs.shape[2] // self.patch_rows, inputs.shape[3]// self.patch_cols))
        
        # Teacher labels as GT for noaug image
        with torch.no_grad():
            logits = self.teacher_backb(inputs) # without postprocessing from student
            logits = self.teacher_dec.forward(logits)
            logits = F.interpolate(logits , size = (inputs.shape[2], inputs.shape[3]) , mode='nearest')
            preds = torch.softmax(logits , dim = 1)
            preds = torch.argmax(preds , dim = 1)
            preds = preds.unsqueeze(1)
            predx = preds.float()
            x = unfold(predx)

        bs, c, h, w = predx.shape
        patch_h, patch_w = inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols
        num_patches = (h // patch_h) * (w // patch_w)
        a = x.reshape(bs, c, patch_h, patch_w, num_patches).permute(0, 4, 1, 2, 3)
        # a -> ( B no.of patches c p p )
        batch, num_patches, channels, patch_height, patch_width = a.shape
        z = a.reshape(batch * num_patches, patch_height * patch_width)
        class_dist = torch.zeros(batch * num_patches, num_classes, device=z.device)
        
        for i in range(class_dist.shape[0]):
            for n in range(num_classes):
                class_dist[i,n] = (z[i] == n ).float().sum()
        class_dist /= class_dist.sum(dim=-1, keepdim=True)

        #####Student predictions for strong augs
        logits = self.backbone(strong_images_batch) # without postprocessing from student
        logits = self.decode_head.forward(logits)
        logits = F.interpolate(logits , size = (inputs.shape[2], inputs.shape[3]) , mode='nearest')
        preds = torch.softmax(logits , dim = 1)
        preds = torch.argmax(preds , dim = 1)
        preds = preds.unsqueeze(1)
        predx = preds.float()
        x = unfold(predx) # B (c*p*p) no.of patches
        bs, c, h, w = predx.shape
        patch_h, patch_w = inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols
        num_patches = (h // patch_h) * (w // patch_w)
        a = x.reshape(bs, c, patch_h, patch_w, num_patches).permute(0, 4, 1, 2, 3)
        # a -> ( B no.of patches c p p )
        batch, num_patches, channels, patch_height, patch_width = a.shape
        z = a.reshape(batch * num_patches, patch_height * patch_width)

        preds_dist = torch.zeros(batch * num_patches, num_classes, device=z.device)
        
        for i in range(preds_dist.shape[0]):
            for n in range(num_classes):
                preds_dist[i,n] = (z[i] == n ).float().sum()
        preds_dist /= preds_dist.sum(dim=-1, keepdim=True)

        log_dist1 = torch.log(preds_dist + 1e-12)  # Adding small value to avoid log(0)

        # Compute KL divergence for each patch using PyTorch's built-in function
        kl_divs = nn.KLDivLoss(reduction="none")(log_dist1, class_dist) #######
        losses['kl_loss'] = kl_divs

        losses['total_loss'] = losses['decode.loss_ce'] + 0.5*losses['kl_loss']

        #'import pdb;pdb.set_trace()
        
        self.local_iter += 1

        return losses
    
@MODELS.register_module()
class ORACLE5(EncoderDecoder):

    def __init__(self, backbone, decode_head, init_cfg,patch_rows,patch_cols, threshold=0.96, ema_alpha=0.99, **kwargs):
        super().__init__(backbone=backbone, decode_head=decode_head, init_cfg=init_cfg, **kwargs)

        self.local_iter = 0
        self.threshold = threshold
        self.ema_alpha = ema_alpha

        self.patch_rows = patch_rows
        self.patch_cols = patch_cols

        self.teacher_backb = MODELS.build(backbone)
        self.teacher_dec = MODELS.build(decode_head)

    def _update_teacher(self, local_iter):
        alpha_teacher = min(1 - 1 / (local_iter + 1), self.ema_alpha)

        for param_name, param in self.backbone.named_parameters():
            if 'lora' in param_name:
                ema_param = self.teacher_backb.state_dict()[param_name]
                ema_param.data[:] = alpha_teacher * ema_param.data + (1 - alpha_teacher) * param.data

    def loss(self, inputs: Tensor, data_samples: SampleList) -> dict:
        """Calculate losses from a batch of inputs and data samples.

        Args:
            inputs (Tensor): Input images.
            data_samples (list[:obj:`SegDataSample`]): The seg data samples.
                It usually includes information such as `metainfo` and
                `gt_sem_seg`.

        Returns:
            dict[str, Tensor]: a dictionary of loss components
        """
        losses = dict()

        if self.local_iter == 0:
            self.backbone_source_state_dict = deepcopy(self.backbone.state_dict())
            self.decoder_source_state_dict = deepcopy(self.decode_head.state_dict())
            prefix = 'teacher_backb.'
            modified_backbone_state_dict = {prefix + k: v for k, v in self.backbone_source_state_dict.items()}
            self.teacher_backb.load_state_dict(modified_backbone_state_dict, strict=False)

            dec_prefix = 'teacher_dec.'
            modified_decoder_state_dict = {dec_prefix + k: v for k, v in self.decoder_source_state_dict.items()}
            self.teacher_dec.load_state_dict(modified_decoder_state_dict, strict=False)

            for n, p in self.teacher_backb.named_parameters():
                p.requires_grad = False

            for n, p in self.teacher_dec.named_parameters():
                p.requires_grad = False

        self.teacher_backb.eval()
        self.teacher_dec.eval()
        
        if self.local_iter > 0:
            self._update_teacher(self.local_iter)

        # get weakly augmented image
        transform = A.Compose([
            A.HorizontalFlip(p=1.0),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_weak_images = []
        transformed_weak_metas = []

        for i in range(inputs.shape[0]):
            image_data = transform(image=inputs[i].permute(1, 2, 0).detach().cpu().numpy())
            image_weak_flipped = image_data["image"].unsqueeze(0).cuda()
            transformed_weak_images.append(image_weak_flipped)
            # create meta
            image_meta_weak_flipped = deepcopy(data_samples[i].metainfo)
            image_meta_weak_flipped['flip'] = True
            image_meta_weak_flipped['flip_direction'] = 'horizontal'
            transformed_weak_metas.append(image_meta_weak_flipped)

        weak_images_batch = torch.cat(transformed_weak_images, dim=0)

        with torch.no_grad():
            # whole inference
            x = self.teacher_backb(weak_images_batch)
            logits = self.teacher_dec.predict(x ,transformed_weak_metas,self.test_cfg)
            ema_softmax = torch.softmax(logits.detach(), dim=1)

            # flip back
            flip = transformed_weak_metas[0]['flip']
            if flip:
                flip_direction = transformed_weak_metas[0]['flip_direction']
                assert flip_direction in ['horizontal', 'vertical']
                if flip_direction == 'horizontal':
                    ema_softmax = ema_softmax.flip(dims=(3,))
                elif flip_direction == 'vertical':
                    ema_softmax = ema_softmax.flip(dims=(2,))

            # compute pseudo weight
            teacher_prob, teacher_pred = torch.max(ema_softmax, dim=1)
            #import pdb; pdb.set_trace()

            ps_large_p = teacher_prob.ge(self.threshold).long() == 1
            ps_size = torch.sum(ps_large_p).item()  # Compute the total number of elements in ps_large_p
            pseudo_weight = ps_size / (inputs.shape[0] * inputs.shape[2] * inputs.shape[3])  # Compute pseudo_weight

            # select most confident predictions
            mask = teacher_prob >= self.threshold
            teacher_pred = torch.where(mask, teacher_pred, 255)
            result = teacher_pred.unsqueeze(1)
            
        del transformed_weak_images
        del weak_images_batch
        
        # get strong augmentation
        strong_transform = A.Compose([
            A.Rotate(limit=30, p=1.0),
            A.Resize(960, 540),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_strong_images = []
        results_strong = []

        # To apply Color transf we need to norm and denorm images
        img_to_transform = deepcopy(inputs)
        # denormalize
        means, stds = get_mean_std(inputs)
        img_to_transform = denorm(img_to_transform, means, stds)

        for i in range(inputs.shape[0]):

            image_data = strong_transform(image=img_to_transform[i].permute(1, 2, 0).detach().cpu().numpy(),
                                          mask=result[i].permute(1, 2, 0).detach().cpu().numpy())
    
            image_strong_aug = image_data["image"].unsqueeze(0).cuda()
            transformed_strong_images.append(image_strong_aug)

            masked_result = image_data["mask"].permute(2, 0, 1).unsqueeze(0).type(torch.long).cuda()
            results_strong.append(masked_result)

        # Detach result and move to CPU
        del result, img_to_transform

        strong_images_batch = torch.cat(transformed_strong_images, dim=0)
        pseudo_labels = torch.cat(results_strong, dim=0)
        # renormalize
        strong_images_batch = renorm(strong_images_batch, means, stds)

        for sample, pseudo_label in zip(data_samples, pseudo_labels):
            sample.gt_sem_seg.data = pseudo_label
        
        #student_losses = self.loss(strong_images_batch,data_samples)
        x = self.extract_feat(strong_images_batch)
        loss_decode = self._decode_head_forward_train(x, data_samples)
        losses.update(loss_decode) 
        losses['decode.loss_ce'] *= pseudo_weight

        #### KL PART ####
        num_classes = 19

        unfold = torch.nn.Unfold(kernel_size=(inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols), stride=(inputs.shape[2] // self.patch_rows, inputs.shape[3]// self.patch_cols))
        
        #Teacher predictions of weakaugs as pseudo label
        teacher_preds = teacher_pred.unsqueeze(1)
        teacher_preds = teacher_preds.float()
        teacher_preds = F.interpolate(teacher_preds , size = (inputs.shape[2], inputs.shape[3]) , mode='nearest')
        predx = teacher_preds
        x = unfold(predx)

        bs, c, h, w = predx.shape
        patch_h, patch_w = inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols
        num_patches = (h // patch_h) * (w // patch_w)
        a = x.reshape(bs, c, patch_h, patch_w, num_patches).permute(0, 4, 1, 2, 3)
        # a -> ( B no.of patches c p p )
        batch, num_patches, channels, patch_height, patch_width = a.shape
        z = a.reshape(batch * num_patches, patch_height * patch_width)
        class_dist = torch.zeros(batch * num_patches, num_classes, device=z.device)
        
        for i in range(class_dist.shape[0]):
            for n in range(num_classes):
                class_dist[i,n] = (z[i] == n ).float().sum()
        class_dist /= class_dist.sum(dim=-1, keepdim=True)

        #####Student predictions for strong augs
        logits = self.backbone(strong_images_batch) # without postprocessing from student
        logits = self.decode_head.forward(logits)
        logits = F.interpolate(logits , size = (inputs.shape[2], inputs.shape[3]) , mode='nearest')
        preds = torch.softmax(logits , dim = 1)
        preds = torch.argmax(preds , dim = 1)
        preds = preds.unsqueeze(1)
        predx = preds.float()
        x = unfold(predx) # B (c*p*p) no.of patches
        bs, c, h, w = predx.shape
        patch_h, patch_w = inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols
        num_patches = (h // patch_h) * (w // patch_w)
        a = x.reshape(bs, c, patch_h, patch_w, num_patches).permute(0, 4, 1, 2, 3)
        # a -> ( B no.of patches c p p )
        batch, num_patches, channels, patch_height, patch_width = a.shape
        z = a.reshape(batch * num_patches, patch_height * patch_width)

        preds_dist = torch.zeros(batch * num_patches, num_classes, device=z.device)
        
        for i in range(preds_dist.shape[0]):
            for n in range(num_classes):
                preds_dist[i,n] = (z[i] == n ).float().sum()
        preds_dist /= preds_dist.sum(dim=-1, keepdim=True)

        log_dist1 = torch.log(preds_dist + 1e-12)  # Adding small value to avoid log(0)

        # Compute KL divergence for each patch using PyTorch's built-in function
        kl_divs = nn.KLDivLoss(reduction="none")(log_dist1, class_dist) #######
        losses['kl_loss'] = kl_divs

        losses['total_loss'] = losses['decode.loss_ce'] + 0.5*losses['kl_loss']
        
        self.local_iter += 1

        return losses
    
@MODELS.register_module()
class ORACLE6(EncoderDecoder):

    def __init__(self, backbone, decode_head, init_cfg,patch_rows,patch_cols, threshold=0.96, ema_alpha=0.99, class_dist_ema=0.99,**kwargs):
        super().__init__(backbone=backbone, decode_head=decode_head, init_cfg=init_cfg, **kwargs)

        self.local_iter = 0
        self.threshold = threshold
        self.ema_alpha = ema_alpha
        self.class_dist_ema = class_dist_ema

        self.patch_rows = patch_rows
        self.patch_cols = patch_cols

        self.teacher_class_dist = None

        self.teacher_backb = MODELS.build(backbone)
        self.teacher_dec = MODELS.build(decode_head)


    def _update_teacher(self, local_iter):
        alpha_teacher = min(1 - 1 / (local_iter + 1), self.ema_alpha)

        for param_name, param in self.backbone.named_parameters():
            if 'lora' in param_name:
                ema_param = self.teacher_backb.state_dict()[param_name]
                ema_param.data[:] = alpha_teacher * ema_param.data + (1 - alpha_teacher) * param.data

    def update_teacher_class_dist(self, new_dist):
        if self.teacher_class_dist is None:
            self.teacher_class_dist = new_dist
        else:
            old_dist_mean = self.teacher_class_dist.mean(dim=0)
            self.teacher_class_dist = self.class_dist_ema * old_dist_mean + (1 - self.class_dist_ema) * new_dist
            #print((new_dist -self.teacher_class_dist ).mean())

    def loss(self, inputs: Tensor, data_samples: SampleList) -> dict:
        """Calculate losses from a batch of inputs and data samples.

        Args:
            inputs (Tensor): Input images.
            data_samples (list[:obj:`SegDataSample`]): The seg data samples.
                It usually includes information such as `metainfo` and
                `gt_sem_seg`.

        Returns:
            dict[str, Tensor]: a dictionary of loss components
        """
        losses = dict()

        if self.local_iter == 0:
            self.backbone_source_state_dict = deepcopy(self.backbone.state_dict())
            self.decoder_source_state_dict = deepcopy(self.decode_head.state_dict())
            prefix = 'teacher_backb.'
            modified_backbone_state_dict = {prefix + k: v for k, v in self.backbone_source_state_dict.items()}
            self.teacher_backb.load_state_dict(modified_backbone_state_dict, strict=False)

            dec_prefix = 'teacher_dec.'
            modified_decoder_state_dict = {dec_prefix + k: v for k, v in self.decoder_source_state_dict.items()}
            self.teacher_dec.load_state_dict(modified_decoder_state_dict, strict=False)

            for n, p in self.teacher_backb.named_parameters():
                p.requires_grad = False

            for n, p in self.teacher_dec.named_parameters():
                p.requires_grad = False

        self.teacher_backb.eval()
        self.teacher_dec.eval()
        
        if self.local_iter > 0:
            self._update_teacher(self.local_iter)

        # get weakly augmented image
        transform = A.Compose([
            A.HorizontalFlip(p=1.0),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_weak_images = []
        transformed_weak_metas = []

        for i in range(inputs.shape[0]):
            image_data = transform(image=inputs[i].permute(1, 2, 0).detach().cpu().numpy())
            image_weak_flipped = image_data["image"].unsqueeze(0).cuda()
            transformed_weak_images.append(image_weak_flipped)
            # create meta
            image_meta_weak_flipped = deepcopy(data_samples[i].metainfo)
            image_meta_weak_flipped['flip'] = True
            image_meta_weak_flipped['flip_direction'] = 'horizontal'
            transformed_weak_metas.append(image_meta_weak_flipped)

        weak_images_batch = torch.cat(transformed_weak_images, dim=0)

        with torch.no_grad():
            # whole inference
            x = self.teacher_backb(weak_images_batch)
            logits = self.teacher_dec.predict(x ,transformed_weak_metas,self.test_cfg)
            ema_softmax = torch.softmax(logits.detach(), dim=1)

            # flip back
            flip = transformed_weak_metas[0]['flip']
            if flip:
                flip_direction = transformed_weak_metas[0]['flip_direction']
                assert flip_direction in ['horizontal', 'vertical']
                if flip_direction == 'horizontal':
                    ema_softmax = ema_softmax.flip(dims=(3,))
                elif flip_direction == 'vertical':
                    ema_softmax = ema_softmax.flip(dims=(2,))

            # compute pseudo weight
            teacher_prob, teacher_pred = torch.max(ema_softmax, dim=1)
            #import pdb; pdb.set_trace()

            ps_large_p = teacher_prob.ge(self.threshold).long() == 1
            ps_size = torch.sum(ps_large_p).item()  # Compute the total number of elements in ps_large_p
            pseudo_weight = ps_size / (inputs.shape[0] * inputs.shape[2] * inputs.shape[3])  # Compute pseudo_weight

            # select most confident predictions
            mask = teacher_prob >= self.threshold
            teacher_pred = torch.where(mask, teacher_pred, 255)
            result = teacher_pred.unsqueeze(1)
            
        del transformed_weak_images
        del weak_images_batch
        
        # get strong augmentation
        strong_transform = A.Compose([
            A.Rotate(limit=30, p=1.0),
            A.Resize(960, 540),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_strong_images = []
        results_strong = []

        # To apply Color transf we need to norm and denorm images
        img_to_transform = deepcopy(inputs)
        # denormalize
        means, stds = get_mean_std(inputs)
        img_to_transform = denorm(img_to_transform, means, stds)

        for i in range(inputs.shape[0]):

            image_data = strong_transform(image=img_to_transform[i].permute(1, 2, 0).detach().cpu().numpy(),
                                          mask=result[i].permute(1, 2, 0).detach().cpu().numpy())
    
            
            image_strong_aug = image_data["image"].unsqueeze(0).cuda()
            transformed_strong_images.append(image_strong_aug)

            masked_result = image_data["mask"].permute(2, 0, 1).unsqueeze(0).type(torch.long).cuda()
            results_strong.append(masked_result)

        # Detach result and move to CPU
        del result, img_to_transform

        strong_images_batch = torch.cat(transformed_strong_images, dim=0)
        pseudo_labels = torch.cat(results_strong, dim=0)
        # renormalize
        strong_images_batch = renorm(strong_images_batch, means, stds)

        for sample, pseudo_label in zip(data_samples, pseudo_labels):
            sample.gt_sem_seg.data = pseudo_label
        
        #student_losses = self.loss(strong_images_batch,data_samples)
        x = self.extract_feat(strong_images_batch)
        loss_decode = self._decode_head_forward_train(x, data_samples)
        losses.update(loss_decode) 
        losses['decode.loss_ce'] *= pseudo_weight

        #### KL PART ####
        
        num_classes = 19
        unfold = torch.nn.Unfold(kernel_size=(inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols), stride=(inputs.shape[2] // self.patch_rows, inputs.shape[3]// self.patch_cols))
        
        img_paths = [sample.img_path for sample in data_samples] #for full images   
        new_size = (inputs.shape[3], inputs.shape[2]) # for opencv its reversed

        full_image = []
        for imgs in img_paths:
            # Read and convert the image
            img = cv2.imread(imgs)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            resized_image = cv2.resize(img, new_size)
            resized_image = resized_image.astype(np.float32) 
            resized_image = torch.tensor(resized_image).permute(2, 0, 1)  
            mean = torch.tensor([123.675, 116.28, 103.53]).view(3, 1, 1)
            std = torch.tensor([58.395, 57.12, 57.375]).view(3, 1, 1)
            normalized_image = (resized_image - mean) / std
            full_image.append(normalized_image)

        # Stack all images into a single tensor and move to GPU if available
        full_image = torch.stack(full_image, dim=0).cuda()

        #Teacher predictions with full images
        with torch.no_grad():
            logits = self.teacher_backb(full_image) # without postprocessing from student
            logits = self.teacher_dec.forward(logits)
            logits = F.interpolate(logits , size = (inputs.shape[2], inputs.shape[3]) , mode='nearest')
            preds = torch.softmax(logits , dim = 1)
            preds = torch.argmax(preds , dim = 1)
            preds = preds.unsqueeze(1)
            predx = preds.float()
            x = unfold(predx)# B (c*p*p) no.of patches

        bs, c, h, w = predx.shape
        patch_h, patch_w = inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols
        num_patches = (h // patch_h) * (w // patch_w)
        a = x.reshape(bs, c, patch_h, patch_w, num_patches).permute(0, 4, 1, 2, 3)
        # a -> ( B no.of patches c p p )
        batch, num_patches, channels, patch_height, patch_width = a.shape
        z = a.reshape(batch * num_patches, patch_height * patch_width)

        class_dist = torch.zeros(batch * num_patches, num_classes, device=z.device)
        
        for i in range(class_dist.shape[0]):
            for n in range(num_classes):
                class_dist[i,n] = (z[i] == n ).float().sum()
        class_dist /= class_dist.sum(dim=-1, keepdim=True)

        # Update teacher's class distribution
        self.update_teacher_class_dist(class_dist)

        # Student predictions with full images
        logits = self._forward(full_image) 
        logits = F.interpolate(logits , size = (inputs.shape[2], inputs.shape[3]) , mode='nearest')
        preds = torch.softmax(logits , dim = 1)
        preds = torch.argmax(preds , dim = 1)
        preds = preds.unsqueeze(1)
        predx = preds.float()
        x = unfold(predx) # B (c*p*p) no.of patches
        bs, c, h, w = predx.shape
        patch_h, patch_w = inputs.shape[2]// self.patch_rows, inputs.shape[3] // self.patch_cols
        num_patches = (h // patch_h) * (w // patch_w)
        a = x.reshape(bs, c, patch_h, patch_w, num_patches).permute(0, 4, 1, 2, 3)
        # a -> ( B no.of patches c p p )
        batch, num_patches, channels, patch_height, patch_width = a.shape
        z = a.reshape(batch * num_patches, patch_height * patch_width)

        preds_dist = torch.zeros(batch * num_patches, num_classes, device=z.device)
        
        for i in range(preds_dist.shape[0]):
            for n in range(num_classes):
                preds_dist[i,n] = (z[i] == n ).float().sum()
        preds_dist /= preds_dist.sum(dim=-1, keepdim=True)

        log_dist1 = torch.log(preds_dist + 1e-12)  # Adding small value to avoid log(0)
        #import pdb;pdb.set_trace()

        # Compute KL divergence for each patch using PyTorch's built-in function
        kl_divs = nn.KLDivLoss(reduction="none")(log_dist1,class_dist)

        losses['kl_loss'] = kl_divs

        losses['total_loss'] = losses['decode.loss_ce'] + x*losses['kl_loss']

      #import pdb;pdb.set_trace()
        
        self.local_iter += 1

        return losses

@MODELS.register_module()
class ORACLE7(EncoderDecoder):
    ## dynamic KL ###

    def __init__(self, backbone, decode_head, init_cfg,patch_rows,patch_cols, threshold=0.96, ema_alpha=0.99, class_dist_ema=0.99,**kwargs):
        super().__init__(backbone=backbone, decode_head=decode_head, init_cfg=init_cfg, **kwargs)

        self.local_iter = 0
        self.threshold = threshold
        self.ema_alpha = ema_alpha
        self.class_dist_ema = class_dist_ema

        self.patch_rows = patch_rows
        self.patch_cols = patch_cols

        self.teacher_class_dist = None

        self.teacher_backb = MODELS.build(backbone)
        self.teacher_dec = MODELS.build(decode_head)

        self.x_start = 0.01
        self.x_end = 0.5
        self.total_iterations = 20000

    def _update_teacher(self, local_iter):
        alpha_teacher = min(1 - 1 / (local_iter + 1), self.ema_alpha)

        for param_name, param in self.backbone.named_parameters():
            if 'lora' in param_name:
                ema_param = self.teacher_backb.state_dict()[param_name]
                ema_param.data[:] = alpha_teacher * ema_param.data + (1 - alpha_teacher) * param.data

    def update_teacher_class_dist(self, new_dist):
        if self.teacher_class_dist is None:
            self.teacher_class_dist = new_dist
        else:
            old_dist_mean = self.teacher_class_dist.mean(dim=0)
            self.teacher_class_dist = self.class_dist_ema * old_dist_mean + (1 - self.class_dist_ema) * new_dist
            #print((new_dist -self.teacher_class_dist ).mean())

    def loss(self, inputs: Tensor, data_samples: SampleList) -> dict:
        """Calculate losses from a batch of inputs and data samples.

        Args:
            inputs (Tensor): Input images.
            data_samples (list[:obj:`SegDataSample`]): The seg data samples.
                It usually includes information such as `metainfo` and
                `gt_sem_seg`.

        Returns:
            dict[str, Tensor]: a dictionary of loss components
        """
        losses = dict()

        if self.local_iter == 0:
            self.backbone_source_state_dict = deepcopy(self.backbone.state_dict())
            self.decoder_source_state_dict = deepcopy(self.decode_head.state_dict())
            prefix = 'teacher_backb.'
            modified_backbone_state_dict = {prefix + k: v for k, v in self.backbone_source_state_dict.items()}
            self.teacher_backb.load_state_dict(modified_backbone_state_dict, strict=False)

            dec_prefix = 'teacher_dec.'
            modified_decoder_state_dict = {dec_prefix + k: v for k, v in self.decoder_source_state_dict.items()}
            self.teacher_dec.load_state_dict(modified_decoder_state_dict, strict=False)

            for n, p in self.teacher_backb.named_parameters():
                p.requires_grad = False

            for n, p in self.teacher_dec.named_parameters():
                p.requires_grad = False

        self.teacher_backb.eval()
        self.teacher_dec.eval()
        
        if self.local_iter > 0:
            self._update_teacher(self.local_iter)

        # get weakly augmented image
        transform = A.Compose([
            A.HorizontalFlip(p=1.0),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_weak_images = []
        transformed_weak_metas = []

        for i in range(inputs.shape[0]):
            image_data = transform(image=inputs[i].permute(1, 2, 0).detach().cpu().numpy())
            image_weak_flipped = image_data["image"].unsqueeze(0).cuda()
            transformed_weak_images.append(image_weak_flipped)
            # create meta
            image_meta_weak_flipped = deepcopy(data_samples[i].metainfo)
            image_meta_weak_flipped['flip'] = True
            image_meta_weak_flipped['flip_direction'] = 'horizontal'
            transformed_weak_metas.append(image_meta_weak_flipped)

        weak_images_batch = torch.cat(transformed_weak_images, dim=0)

        with torch.no_grad():
            # whole inference
            x = self.teacher_backb(weak_images_batch)
            logits = self.teacher_dec.predict(x ,transformed_weak_metas,self.test_cfg)
            ema_softmax = torch.softmax(logits.detach(), dim=1)

            # flip back
            flip = transformed_weak_metas[0]['flip']
            if flip:
                flip_direction = transformed_weak_metas[0]['flip_direction']
                assert flip_direction in ['horizontal', 'vertical']
                if flip_direction == 'horizontal':
                    ema_softmax = ema_softmax.flip(dims=(3,))
                elif flip_direction == 'vertical':
                    ema_softmax = ema_softmax.flip(dims=(2,))

            # compute pseudo weight
            teacher_prob, teacher_pred = torch.max(ema_softmax, dim=1)
            #import pdb; pdb.set_trace()

            ps_large_p = teacher_prob.ge(self.threshold).long() == 1
            ps_size = torch.sum(ps_large_p).item()  # Compute the total number of elements in ps_large_p
            pseudo_weight = ps_size / (inputs.shape[0] * inputs.shape[2] * inputs.shape[3])  # Compute pseudo_weight

            # select most confident predictions
            mask = teacher_prob >= self.threshold
            teacher_pred = torch.where(mask, teacher_pred, 255)
            result = teacher_pred.unsqueeze(1)
            
        del transformed_weak_images
        del weak_images_batch
        
        # get strong augmentation
        strong_transform = A.Compose([
            A.Rotate(limit=30, p=1.0),
            A.Resize(960, 540),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_strong_images = []
        results_strong = []

        # To apply Color transf we need to norm and denorm images
        img_to_transform = deepcopy(inputs)
        # denormalize
        means, stds = get_mean_std(inputs)
        img_to_transform = denorm(img_to_transform, means, stds)

        for i in range(inputs.shape[0]):

            image_data = strong_transform(image=img_to_transform[i].permute(1, 2, 0).detach().cpu().numpy(),
                                          mask=result[i].permute(1, 2, 0).detach().cpu().numpy())
    
            image_strong_aug = image_data["image"].unsqueeze(0).cuda()
            transformed_strong_images.append(image_strong_aug)

            masked_result = image_data["mask"].permute(2, 0, 1).unsqueeze(0).type(torch.long).cuda()
            results_strong.append(masked_result)

        # Detach result and move to CPU
        del result, img_to_transform

        strong_images_batch = torch.cat(transformed_strong_images, dim=0)
        pseudo_labels = torch.cat(results_strong, dim=0)
        # renormalize
        strong_images_batch = renorm(strong_images_batch, means, stds)

        for sample, pseudo_label in zip(data_samples, pseudo_labels):
            sample.gt_sem_seg.data = pseudo_label
        
        #student_losses = self.loss(strong_images_batch,data_samples)
        x = self.extract_feat(strong_images_batch)
        loss_decode = self._decode_head_forward_train(x, data_samples)
        losses.update(loss_decode) 
        losses['decode.loss_ce'] *= pseudo_weight

        #### KL PART ####
        
        num_classes = 19
        unfold = torch.nn.Unfold(kernel_size=(inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols), stride=(inputs.shape[2] // self.patch_rows, inputs.shape[3]// self.patch_cols))
        
        img_paths = [sample.img_path for sample in data_samples] #for full images   
        new_size = (inputs.shape[3], inputs.shape[2]) # for opencv its reversed

        full_image = []
        for imgs in img_paths:
            # Read and convert the image
            img = cv2.imread(imgs)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            resized_image = cv2.resize(img, new_size)
            resized_image = resized_image.astype(np.float32) 
            resized_image = torch.tensor(resized_image).permute(2, 0, 1)  
            mean = torch.tensor([123.675, 116.28, 103.53]).view(3, 1, 1)
            std = torch.tensor([58.395, 57.12, 57.375]).view(3, 1, 1)
            normalized_image = (resized_image - mean) / std
            full_image.append(normalized_image)

        # Stack all images into a single tensor and move to GPU if available
        full_image = torch.stack(full_image, dim=0).cuda()

        #Teacher predictions with full images
        with torch.no_grad():
            logits = self.teacher_backb(full_image) # without postprocessing from student
            logits = self.teacher_dec.forward(logits)
            logits = F.interpolate(logits , size = (inputs.shape[2], inputs.shape[3]) , mode='nearest')
            preds = torch.softmax(logits , dim = 1)
            preds = torch.argmax(preds , dim = 1)
            preds = preds.unsqueeze(1)
            predx = preds.float()
            x = unfold(predx)# B (c*p*p) no.of patches

        bs, c, h, w = predx.shape
        patch_h, patch_w = inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols
        num_patches = (h // patch_h) * (w // patch_w)
        a = x.reshape(bs, c, patch_h, patch_w, num_patches).permute(0, 4, 1, 2, 3)
        # a -> ( B no.of patches c p p )
        batch, num_patches, channels, patch_height, patch_width = a.shape
        z = a.reshape(batch * num_patches, patch_height * patch_width)

        class_dist = torch.zeros(batch * num_patches, num_classes, device=z.device)
        
        for i in range(class_dist.shape[0]):
            for n in range(num_classes):
                class_dist[i,n] = (z[i] == n ).float().sum()
        class_dist /= class_dist.sum(dim=-1, keepdim=True)

        # Update teacher's class distribution
        self.update_teacher_class_dist(class_dist)

        # Student predictions with full images
        logits = self._forward(full_image) 
        logits = F.interpolate(logits , size = (inputs.shape[2], inputs.shape[3]) , mode='nearest')
        preds = torch.softmax(logits , dim = 1)
        preds = torch.argmax(preds , dim = 1)
        preds = preds.unsqueeze(1)
        predx = preds.float()
        x = unfold(predx) # B (c*p*p) no.of patches
        bs, c, h, w = predx.shape
        patch_h, patch_w = inputs.shape[2]// self.patch_rows, inputs.shape[3] // self.patch_cols
        num_patches = (h // patch_h) * (w // patch_w)
        a = x.reshape(bs, c, patch_h, patch_w, num_patches).permute(0, 4, 1, 2, 3)
        # a -> ( B no.of patches c p p )
        batch, num_patches, channels, patch_height, patch_width = a.shape
        z = a.reshape(batch * num_patches, patch_height * patch_width)

        preds_dist = torch.zeros(batch * num_patches, num_classes, device=z.device)
        
        for i in range(preds_dist.shape[0]):
            for n in range(num_classes):
                preds_dist[i,n] = (z[i] == n ).float().sum()
        preds_dist /= preds_dist.sum(dim=-1, keepdim=True)

        log_dist1 = torch.log(preds_dist + 1e-12)  # Adding small value to avoid log(0)
        #import pdb;pdb.set_trace()

        # Compute KL divergence for each patch using PyTorch's built-in function
        kl_divs = nn.KLDivLoss(reduction="none")(log_dist1,class_dist)

        losses['kl_loss'] = kl_divs

        dyn_kl = self.x_start + (self.x_end - self.x_start) * (self.local_iter / self.total_iterations)

        losses['total_loss'] = losses['decode.loss_ce'] + dyn_kl*losses['kl_loss']
       
      #import pdb;pdb.set_trace()
        
        self.local_iter += 1

        return losses
    
@MODELS.register_module()
class ORACLE8(EncoderDecoder):
    ## dynamic KL and global entropy info from gt ###

    def __init__(self, backbone, decode_head, init_cfg,patch_rows,patch_cols, threshold=0.96, ema_alpha=0.99, class_dist_ema=0.99,**kwargs):
        super().__init__(backbone=backbone, decode_head=decode_head, init_cfg=init_cfg, **kwargs)

        self.local_iter = 0
        self.threshold = threshold
        self.ema_alpha = ema_alpha
        self.class_dist_ema = class_dist_ema

        self.patch_rows = patch_rows
        self.patch_cols = patch_cols

        self.teacher_class_dist = None

        self.teacher_backb = MODELS.build(backbone)
        self.teacher_dec = MODELS.build(decode_head)

        self.x_start = 0.01
        self.x_end = 0.5
        self.total_iterations = 20000

    def _update_teacher(self, local_iter):
        alpha_teacher = min(1 - 1 / (local_iter + 1), self.ema_alpha)

        for param_name, param in self.backbone.named_parameters():
            if 'lora' in param_name:
                ema_param = self.teacher_backb.state_dict()[param_name]
                ema_param.data[:] = alpha_teacher * ema_param.data + (1 - alpha_teacher) * param.data

    def update_teacher_class_dist(self, new_dist):
        if self.teacher_class_dist is None:
            self.teacher_class_dist = new_dist
        else:
            old_dist_mean = self.teacher_class_dist.mean(dim=0)
            self.teacher_class_dist = self.class_dist_ema * old_dist_mean + (1 - self.class_dist_ema) * new_dist
            #print((new_dist -self.teacher_class_dist ).mean())

    def loss(self, inputs: Tensor, data_samples: SampleList) -> dict:
        """Calculate losses from a batch of inputs and data samples.

        Args:
            inputs (Tensor): Input images.
            data_samples (list[:obj:`SegDataSample`]): The seg data samples.
                It usually includes information such as `metainfo` and
                `gt_sem_seg`.

        Returns:
            dict[str, Tensor]: a dictionary of loss components
        """
        losses = dict()

        if self.local_iter == 0:
            self.backbone_source_state_dict = deepcopy(self.backbone.state_dict())
            self.decoder_source_state_dict = deepcopy(self.decode_head.state_dict())
            prefix = 'teacher_backb.'
            modified_backbone_state_dict = {prefix + k: v for k, v in self.backbone_source_state_dict.items()}
            self.teacher_backb.load_state_dict(modified_backbone_state_dict, strict=False)

            dec_prefix = 'teacher_dec.'
            modified_decoder_state_dict = {dec_prefix + k: v for k, v in self.decoder_source_state_dict.items()}
            self.teacher_dec.load_state_dict(modified_decoder_state_dict, strict=False)

            for n, p in self.teacher_backb.named_parameters():
                p.requires_grad = False

            for n, p in self.teacher_dec.named_parameters():
                p.requires_grad = False

        self.teacher_backb.eval()
        self.teacher_dec.eval()
        
        if self.local_iter > 0:
            self._update_teacher(self.local_iter)

        # get weakly augmented image
        transform = A.Compose([
            A.HorizontalFlip(p=1.0),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_weak_images = []
        transformed_weak_metas = []

        for i in range(inputs.shape[0]):
            image_data = transform(image=inputs[i].permute(1, 2, 0).detach().cpu().numpy())
            image_weak_flipped = image_data["image"].unsqueeze(0).cuda()
            transformed_weak_images.append(image_weak_flipped)
            # create meta
            image_meta_weak_flipped = deepcopy(data_samples[i].metainfo)
            image_meta_weak_flipped['flip'] = True
            image_meta_weak_flipped['flip_direction'] = 'horizontal'
            transformed_weak_metas.append(image_meta_weak_flipped)

        weak_images_batch = torch.cat(transformed_weak_images, dim=0)

        with torch.no_grad():
            # whole inference
            x = self.teacher_backb(weak_images_batch)
            logits = self.teacher_dec.predict(x ,transformed_weak_metas,self.test_cfg)
            ema_softmax = torch.softmax(logits.detach(), dim=1)

            # flip back
            flip = transformed_weak_metas[0]['flip']
            if flip:
                flip_direction = transformed_weak_metas[0]['flip_direction']
                assert flip_direction in ['horizontal', 'vertical']
                if flip_direction == 'horizontal':
                    ema_softmax = ema_softmax.flip(dims=(3,))
                elif flip_direction == 'vertical':
                    ema_softmax = ema_softmax.flip(dims=(2,))

            # compute pseudo weight
            teacher_prob, teacher_pred = torch.max(ema_softmax, dim=1)
            #import pdb; pdb.set_trace()

            ps_large_p = teacher_prob.ge(self.threshold).long() == 1
            ps_size = torch.sum(ps_large_p).item()  # Compute the total number of elements in ps_large_p
            pseudo_weight = ps_size / (inputs.shape[0] * inputs.shape[2] * inputs.shape[3])  # Compute pseudo_weight

            # select most confident predictions
            mask = teacher_prob >= self.threshold
            teacher_pred = torch.where(mask, teacher_pred, 255)
            result = teacher_pred.unsqueeze(1)
            
        del transformed_weak_images
        del weak_images_batch
        
        # get strong augmentation
        strong_transform = A.Compose([
            A.Rotate(limit=30, p=1.0),
            A.Resize(960, 540),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_strong_images = []
        results_strong = []

        # To apply Color transf we need to norm and denorm images
        img_to_transform = deepcopy(inputs)
        # denormalize
        means, stds = get_mean_std(inputs)
        img_to_transform = denorm(img_to_transform, means, stds)

        for i in range(inputs.shape[0]):

            image_data = strong_transform(image=img_to_transform[i].permute(1, 2, 0).detach().cpu().numpy(),
                                          mask=result[i].permute(1, 2, 0).detach().cpu().numpy())
    
            image_strong_aug = image_data["image"].unsqueeze(0).cuda()
            transformed_strong_images.append(image_strong_aug)

            masked_result = image_data["mask"].permute(2, 0, 1).unsqueeze(0).type(torch.long).cuda()
            results_strong.append(masked_result)

        # Detach result and move to CPU
        del result, img_to_transform

        strong_images_batch = torch.cat(transformed_strong_images, dim=0)
        pseudo_labels = torch.cat(results_strong, dim=0)
        # renormalize
        strong_images_batch = renorm(strong_images_batch, means, stds)

        for sample, pseudo_label in zip(data_samples, pseudo_labels):
            sample.gt_sem_seg.data = pseudo_label
        
        #student_losses = self.loss(strong_images_batch,data_samples)
        x = self.extract_feat(strong_images_batch)
        loss_decode = self._decode_head_forward_train(x, data_samples)
        losses.update(loss_decode) 
        losses['decode.loss_ce'] *= pseudo_weight

        #### KL PART ####
        num_classes = 19
        img_paths = [sample.img_path for sample in data_samples] #for full images   
        new_size = (inputs.shape[3], inputs.shape[2]) # for opencv its reversed
        unfold = torch.nn.Unfold(kernel_size=(inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols), stride=(inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols))

        full_image = []
        for imgs in img_paths:
            # Read and convert the image
            img = cv2.imread(imgs)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            resized_image = cv2.resize(img, new_size)
            resized_image = resized_image.astype(np.float32) 
            resized_image = torch.tensor(resized_image).permute(2, 0, 1)  
            mean = torch.tensor([123.675, 116.28, 103.53]).view(3, 1, 1)
            std = torch.tensor([58.395, 57.12, 57.375]).view(3, 1, 1)
            normalized_image = (resized_image - mean) / std
            full_image.append(normalized_image)

        # Stack all images into a single tensor and move to GPU if available
        full_image = torch.stack(full_image, dim=0).cuda()

        logits = self._forward(full_image) # without postprocessing from student
        logits = F.interpolate(logits , size = (inputs.shape[2], inputs.shape[3]) , mode='nearest')
        preds = torch.softmax(logits , dim =1)
        preds = torch.argmax(preds , dim = 1)
        preds = preds.unsqueeze(1)
        predx = preds.float()
        x = unfold(predx) # B (c*p*p) no.of patches
        bs, c, h, w = predx.shape
        patch_h, patch_w = inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols
        num_patches = (h // patch_h) * (w // patch_w)
        a = x.reshape(bs, c, patch_h, patch_w, num_patches).permute(0, 4, 1, 2, 3)
        # a -> ( B no.of patches c p p )
        batch, num_patches, channels, patch_height, patch_width = a.shape
        z = a.reshape(batch * num_patches, patch_height * patch_width)

        preds_dist = torch.zeros(batch * num_patches, num_classes, device=z.device)
        for i in range(preds_dist.shape[0]):
            for n in range(num_classes):
                preds_dist[i,n] = (z[i] == n ).float().sum()
        preds_dist /= preds_dist.sum(dim=-1, keepdim=True)

        fog_gt = torch.from_numpy(np.load('/BS/DApt/work/project/segformer_test/gt_entropies/fog.npy')).to(x.device) # no. of patches x no. of classes
        fog_gt = torch.stack([fog_gt,fog_gt])
        
        preds_dist = preds_dist.reshape(bs , -1 , num_classes)
        preds_dist = preds_dist.mean(dim = 0)

        log_dist1 = torch.log(preds_dist + 1e-12)  # Adding small value to avoid log(0)
        
        # Compute KL divergence 
        kl_divs = nn.KLDivLoss(reduction="none")(log_dist1, fog_gt) ####### bs x no. of patches x no. of classes
        kl_divs = kl_divs.mean(0) #no. of patches x no. of classes
        patch_kl_divs = kl_divs.sum(dim=1) # no. of patches x 1
        mean_kl_div = patch_kl_divs.mean()

        losses['kl_loss'] = mean_kl_div
        dyn_kl = self.x_start + (self.x_end - self.x_start) * (self.local_iter / self.total_iterations)
        losses['total_loss'] = losses['decode.loss_ce'] + 0.1*losses['kl_loss']

        #import pdb;pdb.set_trace()
        self.local_iter += 1

        return losses
    
@MODELS.register_module()
class ORACLE9(EncoderDecoder):
    ## dynamic KL ###

    def __init__(self, backbone, decode_head, init_cfg,patch_rows,patch_cols, threshold=0.96, ema_alpha=0.99, class_dist_ema=0.01,**kwargs):
        super().__init__(backbone=backbone, decode_head=decode_head, init_cfg=init_cfg, **kwargs)

        self.local_iter = 0
        self.threshold = threshold
        self.ema_alpha = ema_alpha
        self.class_dist_ema = class_dist_ema

        self.patch_rows = patch_rows
        self.patch_cols = patch_cols

        self.teacher_class_dist = None

        self.teacher_backb = MODELS.build(backbone)
        self.teacher_dec = MODELS.build(decode_head)

        self.x_start = 0.01
        self.x_end = 0.5
        self.total_iterations = 20000

    def _update_teacher(self, local_iter):
        alpha_teacher = min(1 - 1 / (local_iter + 1), self.ema_alpha)

        for param_name, param in self.backbone.named_parameters():
            if 'lora' in param_name:
                ema_param = self.teacher_backb.state_dict()[param_name]
                ema_param.data[:] = alpha_teacher * ema_param.data + (1 - alpha_teacher) * param.data

    def update_teacher_class_dist(self, dist):
        if self.teacher_class_dist is None:
            self.teacher_class_dist = dist # (batch x no. of patches) x no. of classes
        else:
            old_dist_mean = self.teacher_class_dist.mean(dim=0) # (no. of classes x 1)
            self.teacher_class_dist = self.class_dist_ema * old_dist_mean + (1 - self.class_dist_ema) * dist
        

    def loss(self, inputs: Tensor, data_samples: SampleList) -> dict:
        """Calculate losses from a batch of inputs and data samples.

        Args:
            inputs (Tensor): Input images.
            data_samples (list[:obj:`SegDataSample`]): The seg data samples.
                It usually includes information such as `metainfo` and
                `gt_sem_seg`.

        Returns:
            dict[str, Tensor]: a dictionary of loss components
        """
        losses = dict()

        if self.local_iter == 0:
            self.backbone_source_state_dict = deepcopy(self.backbone.state_dict())
            self.decoder_source_state_dict = deepcopy(self.decode_head.state_dict())
            prefix = 'teacher_backb.'
            modified_backbone_state_dict = {prefix + k: v for k, v in self.backbone_source_state_dict.items()}
            self.teacher_backb.load_state_dict(modified_backbone_state_dict, strict=False)

            dec_prefix = 'teacher_dec.'
            modified_decoder_state_dict = {dec_prefix + k: v for k, v in self.decoder_source_state_dict.items()}
            self.teacher_dec.load_state_dict(modified_decoder_state_dict, strict=False)

            for n, p in self.teacher_backb.named_parameters():
                p.requires_grad = False

            for n, p in self.teacher_dec.named_parameters():
                p.requires_grad = False

        self.teacher_backb.eval()
        self.teacher_dec.eval()
        
        if self.local_iter > 0:
            self._update_teacher(self.local_iter)

        # get weakly augmented image
        transform = A.Compose([
            A.HorizontalFlip(p=1.0),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_weak_images = []
        transformed_weak_metas = []

        for i in range(inputs.shape[0]):
            image_data = transform(image=inputs[i].permute(1, 2, 0).detach().cpu().numpy())
            image_weak_flipped = image_data["image"].unsqueeze(0).cuda()
            transformed_weak_images.append(image_weak_flipped)
            # create meta
            image_meta_weak_flipped = deepcopy(data_samples[i].metainfo)
            image_meta_weak_flipped['flip'] = True
            image_meta_weak_flipped['flip_direction'] = 'horizontal'
            transformed_weak_metas.append(image_meta_weak_flipped)

        weak_images_batch = torch.cat(transformed_weak_images, dim=0)

        with torch.no_grad():
            # whole inference
            x = self.teacher_backb(weak_images_batch)
            logits = self.teacher_dec.predict(x ,transformed_weak_metas,self.test_cfg)
            ema_softmax = torch.softmax(logits.detach(), dim=1)

            # flip back
            flip = transformed_weak_metas[0]['flip']
            if flip:
                flip_direction = transformed_weak_metas[0]['flip_direction']
                assert flip_direction in ['horizontal', 'vertical']
                if flip_direction == 'horizontal':
                    ema_softmax = ema_softmax.flip(dims=(3,))
                elif flip_direction == 'vertical':
                    ema_softmax = ema_softmax.flip(dims=(2,))

            # compute pseudo weight
            teacher_prob, teacher_pred = torch.max(ema_softmax, dim=1)
            #import pdb; pdb.set_trace()

            ps_large_p = teacher_prob.ge(self.threshold).long() == 1
            ps_size = torch.sum(ps_large_p).item()  # Compute the total number of elements in ps_large_p
            pseudo_weight = ps_size / (inputs.shape[0] * inputs.shape[2] * inputs.shape[3])  # Compute pseudo_weight

            # select most confident predictions
            mask = teacher_prob >= self.threshold
            teacher_pred = torch.where(mask, teacher_pred, 255)
            result = teacher_pred.unsqueeze(1)
            
        del transformed_weak_images
        del weak_images_batch
        
        # get strong augmentation
        strong_transform = A.Compose([
            A.Rotate(limit=30, p=1.0),
            A.Resize(960, 540),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_strong_images = []
        results_strong = []

        # To apply Color transf we need to norm and denorm images
        img_to_transform = deepcopy(inputs)
        # denormalize
        means, stds = get_mean_std(inputs)
        img_to_transform = denorm(img_to_transform, means, stds)

        for i in range(inputs.shape[0]):

            image_data = strong_transform(image=img_to_transform[i].permute(1, 2, 0).detach().cpu().numpy(),
                                          mask=result[i].permute(1, 2, 0).detach().cpu().numpy())
    
            image_strong_aug = image_data["image"].unsqueeze(0).cuda()
            transformed_strong_images.append(image_strong_aug)

            masked_result = image_data["mask"].permute(2, 0, 1).unsqueeze(0).type(torch.long).cuda()
            results_strong.append(masked_result)

        # Detach result and move to CPU
        del result, img_to_transform

        strong_images_batch = torch.cat(transformed_strong_images, dim=0)
        pseudo_labels = torch.cat(results_strong, dim=0)
        # renormalize
        strong_images_batch = renorm(strong_images_batch, means, stds)

        for sample, pseudo_label in zip(data_samples, pseudo_labels):
            sample.gt_sem_seg.data = pseudo_label
        
        #student_losses = self.loss(strong_images_batch,data_samples)
        x = self.extract_feat(strong_images_batch)
        loss_decode = self._decode_head_forward_train(x, data_samples)
        losses.update(loss_decode) 
        losses['decode.loss_ce'] *= pseudo_weight

        #### KL PART ####
        
        num_classes = 19
        unfold = torch.nn.Unfold(kernel_size=(inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols), stride=(inputs.shape[2] // self.patch_rows, inputs.shape[3]// self.patch_cols))
        
        img_paths = [sample.img_path for sample in data_samples] #for full images   
        new_size = (inputs.shape[3], inputs.shape[2]) # for opencv its reversed

        gts = []
        for gt in data_samples:
            x = gt._gt_sem_seg.data
            gts.append(x)
        gt = torch.stack(gts, dim = 0).cuda()
        unfold = torch.nn.Unfold(kernel_size=(inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols), stride=(inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols))
        predx = gt.float()
        x = unfold(predx)  # B (c*p*p) no.of patches
        bs, c, h, w = predx.shape
        patch_h, patch_w = inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols
        num_patches = (h // patch_h) * (w // patch_w)
        a = x.reshape(bs, c, patch_h, patch_w, num_patches).permute(0, 4, 1, 2, 3)
        # a -> ( B no.of patches c p p )
        batch, num_patches, channels, patch_height, patch_width = a.shape
        z = a.reshape(batch * num_patches, patch_height * patch_width)

        class_dist = torch.zeros(batch * num_patches, num_classes, device=z.device)
        
        for i in range(class_dist.shape[0]):
            for n in range(num_classes):
                class_dist[i,n] = (z[i] == n ).float().sum()
        class_dist /= (class_dist.sum(dim=-1, keepdim=True) + 1e-12)

        # Update teacher's class distribution
        self.update_teacher_class_dist(class_dist)

        # Student predictions with full images
        full_image = []
        for imgs in img_paths:
            # Read and convert the image
            img = cv2.imread(imgs)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            resized_image = cv2.resize(img, new_size)
            resized_image = resized_image.astype(np.float32) 
            resized_image = torch.tensor(resized_image).permute(2, 0, 1)  
            mean = torch.tensor([123.675, 116.28, 103.53]).view(3, 1, 1)
            std = torch.tensor([58.395, 57.12, 57.375]).view(3, 1, 1)
            normalized_image = (resized_image - mean) / std
            full_image.append(normalized_image)

        # Stack all images into a single tensor and move to GPU if available
        full_image = torch.stack(full_image, dim=0).cuda()
        logits = self._forward(full_image) 
        logits = F.interpolate(logits , size = (inputs.shape[2], inputs.shape[3]) , mode='nearest')
        preds = torch.softmax(logits , dim = 1)
        preds = torch.argmax(preds , dim = 1)
        preds = preds.unsqueeze(1)
        predx = preds.float()
        x = unfold(predx) # B (c*p*p) no.of patches
        bs, c, h, w = predx.shape
        patch_h, patch_w = inputs.shape[2]// self.patch_rows, inputs.shape[3] // self.patch_cols
        num_patches = (h // patch_h) * (w // patch_w)
        a = x.reshape(bs, c, patch_h, patch_w, num_patches).permute(0, 4, 1, 2, 3)
        # a -> ( B no.of patches c p p )
        batch, num_patches, channels, patch_height, patch_width = a.shape
        z = a.reshape(batch * num_patches, patch_height * patch_width)

        preds_dist = torch.zeros(batch * num_patches, num_classes, device=z.device)
        
        for i in range(preds_dist.shape[0]):
            for n in range(num_classes):
                preds_dist[i,n] = (z[i] == n ).float().sum()
        preds_dist /= (preds_dist.sum(dim=-1, keepdim=True) + 1e-12)

        log_dist1 = torch.log(preds_dist + 1e-12)  # Adding small value to avoid log(0)
        
        # Compute KL divergence for each patch using PyTorch's built-in function
        kl_divs = nn.KLDivLoss(reduction="none")(log_dist1, self.teacher_class_dist) ####### no. of patches x no. of classes
        patch_kl_divs = kl_divs.sum(dim=1) # no. of patches x 1
        mean_kl_div = patch_kl_divs.mean()

        losses['kl_loss'] = mean_kl_div
        losses['total_loss'] = losses['decode.loss_ce'] + 0.1*losses['kl_loss']
       
       
      #import pdb;pdb.set_trace()
        
        self.local_iter += 1

        return losses
    

@MODELS.register_module()
class ORACLE10(EncoderDecoder):
    ## global entropy KL losss weighting info from gt ###

    def __init__(self, backbone, decode_head, init_cfg,patch_rows,patch_cols,kl, threshold=0.96, ema_alpha=0.99, class_dist_ema=0.99,**kwargs):
        super().__init__(backbone=backbone, decode_head=decode_head, init_cfg=init_cfg, **kwargs)

        self.local_iter = 0
        self.threshold = threshold
        self.ema_alpha = ema_alpha
        self.class_dist_ema = class_dist_ema

        self.patch_rows = patch_rows
        self.patch_cols = patch_cols
        self.kl = kl

        self.teacher_class_dist = None

        self.teacher_backb = MODELS.build(backbone)
        self.teacher_dec = MODELS.build(decode_head)

        self.x_start = 0.01
        self.x_end = 0.5
        self.total_iterations = 20000

    def _update_teacher(self, local_iter):
        alpha_teacher = min(1 - 1 / (local_iter + 1), self.ema_alpha)

        for param_name, param in self.backbone.named_parameters():
            if 'lora' in param_name:
                ema_param = self.teacher_backb.state_dict()[param_name]
                ema_param.data[:] = alpha_teacher * ema_param.data + (1 - alpha_teacher) * param.data

    def update_teacher_class_dist(self, new_dist):
        if self.teacher_class_dist is None:
            self.teacher_class_dist = new_dist
        else:
            old_dist_mean = self.teacher_class_dist.mean(dim=0)
            self.teacher_class_dist = self.class_dist_ema * old_dist_mean + (1 - self.class_dist_ema) * new_dist
            #print((new_dist -self.teacher_class_dist ).mean())

    def loss(self, inputs: Tensor, data_samples: SampleList) -> dict:
        """Calculate losses from a batch of inputs and data samples.

        Args:
            inputs (Tensor): Input images.
            data_samples (list[:obj:`SegDataSample`]): The seg data samples.
                It usually includes information such as `metainfo` and
                `gt_sem_seg`.

        Returns:
            dict[str, Tensor]: a dictionary of loss components
        """
        losses = dict()

        if self.local_iter == 0:
            self.backbone_source_state_dict = deepcopy(self.backbone.state_dict())
            self.decoder_source_state_dict = deepcopy(self.decode_head.state_dict())
            prefix = 'teacher_backb.'
            modified_backbone_state_dict = {prefix + k: v for k, v in self.backbone_source_state_dict.items()}
            self.teacher_backb.load_state_dict(modified_backbone_state_dict, strict=False)

            dec_prefix = 'teacher_dec.'
            modified_decoder_state_dict = {dec_prefix + k: v for k, v in self.decoder_source_state_dict.items()}
            self.teacher_dec.load_state_dict(modified_decoder_state_dict, strict=False)

            for n, p in self.teacher_backb.named_parameters():
                p.requires_grad = False

            for n, p in self.teacher_dec.named_parameters():
                p.requires_grad = False

        self.teacher_backb.eval()
        self.teacher_dec.eval()
        
        if self.local_iter > 0:
            self._update_teacher(self.local_iter)

        # get weakly augmented image
        transform = A.Compose([
            A.HorizontalFlip(p=1.0),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_weak_images = []
        transformed_weak_metas = []

        for i in range(inputs.shape[0]):
            image_data = transform(image=inputs[i].permute(1, 2, 0).detach().cpu().numpy())
            image_weak_flipped = image_data["image"].unsqueeze(0).cuda()
            transformed_weak_images.append(image_weak_flipped)
            # create meta
            image_meta_weak_flipped = deepcopy(data_samples[i].metainfo)
            image_meta_weak_flipped['flip'] = True
            image_meta_weak_flipped['flip_direction'] = 'horizontal'
            transformed_weak_metas.append(image_meta_weak_flipped)

        weak_images_batch = torch.cat(transformed_weak_images, dim=0)

        with torch.no_grad():
            # whole inference
            x = self.teacher_backb(weak_images_batch)
            logits = self.teacher_dec.predict(x ,transformed_weak_metas,self.test_cfg)
            ema_softmax = torch.softmax(logits.detach(), dim=1)

            # flip back
            flip = transformed_weak_metas[0]['flip']
            if flip:
                flip_direction = transformed_weak_metas[0]['flip_direction']
                assert flip_direction in ['horizontal', 'vertical']
                if flip_direction == 'horizontal':
                    ema_softmax = ema_softmax.flip(dims=(3,))
                elif flip_direction == 'vertical':
                    ema_softmax = ema_softmax.flip(dims=(2,))

            # compute pseudo weight
            teacher_prob, teacher_pred = torch.max(ema_softmax, dim=1)
            #import pdb; pdb.set_trace()

            ps_large_p = teacher_prob.ge(self.threshold).long() == 1
            ps_size = torch.sum(ps_large_p).item()  # Compute the total number of elements in ps_large_p
            pseudo_weight = ps_size / (inputs.shape[0] * inputs.shape[2] * inputs.shape[3])  # Compute pseudo_weight

            # select most confident predictions
            mask = teacher_prob >= self.threshold
            teacher_pred = torch.where(mask, teacher_pred, 255)
            result = teacher_pred.unsqueeze(1)
            
        del transformed_weak_images
        del weak_images_batch
        
        # get strong augmentation
        strong_transform = A.Compose([
            A.Rotate(limit=30, p=1.0),
            A.Resize(960, 540),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_strong_images = []
        results_strong = []

        # To apply Color transf we need to norm and denorm images
        img_to_transform = deepcopy(inputs)
        # denormalize
        means, stds = get_mean_std(inputs)
        img_to_transform = denorm(img_to_transform, means, stds)

        for i in range(inputs.shape[0]):

            image_data = strong_transform(image=img_to_transform[i].permute(1, 2, 0).detach().cpu().numpy(),
                                          mask=result[i].permute(1, 2, 0).detach().cpu().numpy())
    
            image_strong_aug = image_data["image"].unsqueeze(0).cuda()
            transformed_strong_images.append(image_strong_aug)

            masked_result = image_data["mask"].permute(2, 0, 1).unsqueeze(0).type(torch.long).cuda()
            results_strong.append(masked_result)

        # Detach result and move to CPU
        del result, img_to_transform

        strong_images_batch = torch.cat(transformed_strong_images, dim=0)
        pseudo_labels = torch.cat(results_strong, dim=0)
        # renormalize
        strong_images_batch = renorm(strong_images_batch, means, stds)

        for sample, pseudo_label in zip(data_samples, pseudo_labels):
            sample.gt_sem_seg.data = pseudo_label
        
        #student_losses = self.loss(strong_images_batch,data_samples)
        x = self.extract_feat(strong_images_batch)
        loss_decode = self._decode_head_forward_train(x, data_samples)
        losses.update(loss_decode) 
        losses['decode.loss_ce'] *= pseudo_weight

        #### KL PART ####
        num_classes = 19
        img_paths = [sample.img_path for sample in data_samples] #for full images   
        new_size = (inputs.shape[3], inputs.shape[2]) # for opencv its reversed
        #unfold = torch.nn.Unfold(kernel_size=(inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols), stride=(inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols))

        full_image = []
        for imgs in img_paths:
            # Read and convert the image
            img = cv2.imread(imgs)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            resized_image = cv2.resize(img, new_size)
            resized_image = resized_image.astype(np.float32) 
            resized_image = torch.tensor(resized_image).permute(2, 0, 1)  
            mean = torch.tensor([123.675, 116.28, 103.53]).view(3, 1, 1)
            std = torch.tensor([58.395, 57.12, 57.375]).view(3, 1, 1)
            normalized_image = (resized_image - mean) / std
            full_image.append(normalized_image)

        # Stack all images into a single tensor and move to GPU if available
        full_image = torch.stack(full_image, dim=0).cuda()

        logitx = self._forward(full_image) # without postprocessing from student
        unfold = torch.nn.Unfold(kernel_size=(logitx.shape[2] // self.patch_rows, logitx.shape[3] // self.patch_cols), stride=(logitx.shape[2] // self.patch_rows, logitx.shape[3] // self.patch_cols))
        #logits = F.interpolate(logitx , size = (inputs.shape[2], inputs.shape[3]) , mode='nearest')
        preds = torch.softmax(logitx , dim =1)
        preds = torch.argmax(preds , dim = 1)
        preds = preds.unsqueeze(1)
        predx = preds.float()
        x = unfold(predx) # B (c*p*p) no.of patches
        bs, c, h, w = predx.shape
        patch_h, patch_w = logitx.shape[2] // self.patch_rows, logitx.shape[3] // self.patch_cols
        num_patches = (h // patch_h) * (w // patch_w)
        patch = x.reshape(bs, c, patch_h, patch_w, num_patches).permute(0, 4, 1, 2, 3)
        # a -> ( B no.of patches c p p )
        batch, num_patches, channels, patch_height, patch_width = patch.shape
        z = patch.reshape(batch * num_patches, patch_height * patch_width)
        
        preds_dist = torch.zeros(batch * num_patches, num_classes, device=z.device)
        
        for i in range(preds_dist.shape[0]):
            for n in range(num_classes):
                preds_dist[i,n] = (z[i] == n ).float().sum()
                
        preds_dist /= preds_dist.sum(dim=-1, keepdim=True)
        preds_dist = preds_dist.reshape(bs , -1 , num_classes)
        preds_dist = preds_dist.mean(dim = 0)

        log_dist1 = torch.log(preds_dist + 1e-12)  # Adding small value to avoid log(0)
        
        #import pdb;pdb.set_trace()

        night_gt = torch.from_numpy(np.load('/BS/DApt/work/project/segformer_test/gt_entropies/night_v2.npy')).to(x.device) # no. of patches x no. of classes
        night_ent = torch.from_numpy(np.load('/BS/DApt/work/project/segformer_test/gt_entropies/night_entropy_v2.npy')).to(x.device) # no. of patches x no. of classes

        # Compute KL divergence 
        kl_divs = nn.KLDivLoss(reduction="none")(log_dist1, night_gt) ####### no. of patches x no. of classes
        patch_kl_divs = kl_divs.sum(dim=1) # no. of patches x 1

        l_inverted = torch.log(1/(1e-12 + night_ent))
        l_normalized = (l_inverted - l_inverted.min()) / (l_inverted.max() - l_inverted.min())
        weighted_kl = patch_kl_divs * l_normalized # entropy weighting

        mean_kl_div = weighted_kl.mean()
        losses['kl_loss'] = mean_kl_div
        dyn_kl = self.x_start + (self.x_end - self.x_start) * (self.local_iter / self.total_iterations)
        losses['total_loss'] = losses['decode.loss_ce'] + self.kl*losses['kl_loss']
        #print(f'Inver weighted with KL weight {self.kl}')
        #import pdb;pdb.set_trace()
        self.local_iter += 1

        return losses

@MODELS.register_module()
class ORACLE11(EncoderDecoder):
    ## global entropy KL losss weighting info from gt ###

    def __init__(self, backbone, decode_head, init_cfg,patch_rows,patch_cols,kl, threshold=0.96, ema_alpha=0.99, class_dist_ema=0.99,ent_ema = 0.5,**kwargs):
        super().__init__(backbone=backbone, decode_head=decode_head, init_cfg=init_cfg, **kwargs)

        self.local_iter = 0
        self.threshold = threshold
        self.ema_alpha = ema_alpha
        self.class_dist_ema = class_dist_ema
        self.ent_ema = ent_ema

        self.patch_rows = patch_rows
        self.patch_cols = patch_cols
        self.kl = kl

        self.teacher_class_dist = None
        self.teacher_class_ent = None

        self.teacher_backb = MODELS.build(backbone)
        self.teacher_dec = MODELS.build(decode_head)

        self.x_start = 0.01
        self.x_end = 0.5
        self.total_iterations = 20000

    def _update_teacher(self, local_iter):
        alpha_teacher = min(1 - 1 / (local_iter + 1), self.ema_alpha)

        for param_name, param in self.backbone.named_parameters():
            if 'lora' in param_name:
                ema_param = self.teacher_backb.state_dict()[param_name]
                ema_param.data[:] = alpha_teacher * ema_param.data + (1 - alpha_teacher) * param.data

    def update_teacher_class_dist(self, new_dist):
        if self.teacher_class_dist is None:
            self.teacher_class_dist = new_dist
        else:
            old_dist_mean = self.teacher_class_dist.mean(dim=0)
            self.teacher_class_dist = self.class_dist_ema * old_dist_mean + (1 - self.class_dist_ema) * new_dist
            #print((new_dist -self.teacher_class_dist ).mean())
    
    def update_teacher_class_ent(self, ent):
        if self.teacher_class_ent is None:
            self.teacher_class_ent = ent
        else:
            self.teacher_class_ent =  self.ent_ema * self.teacher_class_ent + (1 - self.ent_ema) * ent
            #print((new_dist -self.teacher_class_dist ).mean())

    def loss(self, inputs: Tensor, data_samples: SampleList) -> dict:
        """Calculate losses from a batch of inputs and data samples.

        Args:
            inputs (Tensor): Input images.
            data_samples (list[:obj:`SegDataSample`]): The seg data samples.
                It usually includes information such as `metainfo` and
                `gt_sem_seg`.

        Returns:
            dict[str, Tensor]: a dictionary of loss components
        """
        losses = dict()

        if self.local_iter == 0:
            self.backbone_source_state_dict = deepcopy(self.backbone.state_dict())
            self.decoder_source_state_dict = deepcopy(self.decode_head.state_dict())
            prefix = 'teacher_backb.'
            modified_backbone_state_dict = {prefix + k: v for k, v in self.backbone_source_state_dict.items()}
            self.teacher_backb.load_state_dict(modified_backbone_state_dict, strict=False)

            dec_prefix = 'teacher_dec.'
            modified_decoder_state_dict = {dec_prefix + k: v for k, v in self.decoder_source_state_dict.items()}
            self.teacher_dec.load_state_dict(modified_decoder_state_dict, strict=False)

            for n, p in self.teacher_backb.named_parameters():
                p.requires_grad = False

            for n, p in self.teacher_dec.named_parameters():
                p.requires_grad = False

        self.teacher_backb.eval()
        self.teacher_dec.eval()
        
        if self.local_iter > 0:
            self._update_teacher(self.local_iter)

        # get weakly augmented image
        transform = A.Compose([
            A.HorizontalFlip(p=1.0),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_weak_images = []
        transformed_weak_metas = []

        for i in range(inputs.shape[0]):
            image_data = transform(image=inputs[i].permute(1, 2, 0).detach().cpu().numpy())
            image_weak_flipped = image_data["image"].unsqueeze(0).cuda()
            transformed_weak_images.append(image_weak_flipped)
            # create meta
            image_meta_weak_flipped = deepcopy(data_samples[i].metainfo)
            image_meta_weak_flipped['flip'] = True
            image_meta_weak_flipped['flip_direction'] = 'horizontal'
            transformed_weak_metas.append(image_meta_weak_flipped)

        weak_images_batch = torch.cat(transformed_weak_images, dim=0)

        with torch.no_grad():
            # whole inference
            x = self.teacher_backb(weak_images_batch)
            logits = self.teacher_dec.predict(x ,transformed_weak_metas,self.test_cfg)
            ema_softmax = torch.softmax(logits.detach(), dim=1)

            # flip back
            flip = transformed_weak_metas[0]['flip']
            if flip:
                flip_direction = transformed_weak_metas[0]['flip_direction']
                assert flip_direction in ['horizontal', 'vertical']
                if flip_direction == 'horizontal':
                    ema_softmax = ema_softmax.flip(dims=(3,))
                elif flip_direction == 'vertical':
                    ema_softmax = ema_softmax.flip(dims=(2,))

            # compute pseudo weight
            teacher_prob, teacher_pred = torch.max(ema_softmax, dim=1)
            #import pdb; pdb.set_trace()

            ps_large_p = teacher_prob.ge(self.threshold).long() == 1
            ps_size = torch.sum(ps_large_p).item()  # Compute the total number of elements in ps_large_p
            pseudo_weight = ps_size / (inputs.shape[0] * inputs.shape[2] * inputs.shape[3])  # Compute pseudo_weight

            # select most confident predictions
            mask = teacher_prob >= self.threshold
            teacher_pred = torch.where(mask, teacher_pred, 255)
            result = teacher_pred.unsqueeze(1)
            
        del transformed_weak_images
        del weak_images_batch
        
        # get strong augmentation
        strong_transform = A.Compose([
            A.Rotate(limit=30, p=1.0),
            A.Resize(960, 540),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_strong_images = []
        results_strong = []

        # To apply Color transf we need to norm and denorm images
        img_to_transform = deepcopy(inputs)
        # denormalize
        means, stds = get_mean_std(inputs)
        img_to_transform = denorm(img_to_transform, means, stds)

        for i in range(inputs.shape[0]):

            image_data = strong_transform(image=img_to_transform[i].permute(1, 2, 0).detach().cpu().numpy(),
                                          mask=result[i].permute(1, 2, 0).detach().cpu().numpy())
    
            image_strong_aug = image_data["image"].unsqueeze(0).cuda()
            transformed_strong_images.append(image_strong_aug)

            masked_result = image_data["mask"].permute(2, 0, 1).unsqueeze(0).type(torch.long).cuda()
            results_strong.append(masked_result)

        # Detach result and move to CPU
        del result, img_to_transform

        strong_images_batch = torch.cat(transformed_strong_images, dim=0)
        pseudo_labels = torch.cat(results_strong, dim=0)
        # renormalize
        strong_images_batch = renorm(strong_images_batch, means, stds)

        for sample, pseudo_label in zip(data_samples, pseudo_labels):
            sample.gt_sem_seg.data = pseudo_label
        
        #student_losses = self.loss(strong_images_batch,data_samples)
        x = self.extract_feat(strong_images_batch)
        loss_decode = self._decode_head_forward_train(x, data_samples)
        losses.update(loss_decode) 
        losses['decode.loss_ce'] *= pseudo_weight

        #### KL PART ####
        num_classes = 19
        img_paths = [sample.img_path for sample in data_samples] #for full images   
        new_size = (inputs.shape[3], inputs.shape[2]) # for opencv its reversed
        unfold = torch.nn.Unfold(kernel_size=(inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols), stride=(inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols))

        full_image = []
        for imgs in img_paths:
            # Read and convert the image
            img = cv2.imread(imgs)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            resized_image = cv2.resize(img, new_size)
            resized_image = resized_image.astype(np.float32) 
            resized_image = torch.tensor(resized_image).permute(2, 0, 1)  
            mean = torch.tensor([123.675, 116.28, 103.53]).view(3, 1, 1)
            std = torch.tensor([58.395, 57.12, 57.375]).view(3, 1, 1)
            normalized_image = (resized_image - mean) / std
            full_image.append(normalized_image)

        # Stack all images into a single tensor and move to GPU if available
        full_image = torch.stack(full_image, dim=0).cuda()


        ##Student
        logits = self._forward(full_image) # without postprocessing from student
        logits = F.interpolate(logits , size = (inputs.shape[2], inputs.shape[3]) , mode='nearest')
        preds = torch.softmax(logits , dim =1)
        preds = torch.argmax(preds , dim = 1)
        preds = preds.unsqueeze(1)
        predx = preds.float()
        x = unfold(predx) # B (c*p*p) no.of patches
        bs, c, h, w = predx.shape
        patch_h, patch_w = inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols
        num_patches = (h // patch_h) * (w // patch_w)
        a = x.reshape(bs, c, patch_h, patch_w, num_patches).permute(0, 4, 1, 2, 3)
        # a -> ( B no.of patches c p p )
        batch, num_patches, channels, patch_height, patch_width = a.shape
        z = a.reshape(batch * num_patches, patch_height * patch_width)

        preds_dist = torch.zeros(batch * num_patches, num_classes, device=z.device)
        for i in range(preds_dist.shape[0]):
            for n in range(num_classes):
                preds_dist[i,n] = (z[i] == n ).float().sum()
        preds_dist /= preds_dist.sum(dim=-1, keepdim=True)

        log_dist1 = torch.log(preds_dist + 1e-12)  # Adding small value to avoid log(0)

        ##Teacher
        with torch.no_grad():
            logits = self.teacher_backb(full_image) # without postprocessing from student
            logits = self.teacher_dec.forward(logits)
            logits = F.interpolate(logits , size = (inputs.shape[2], inputs.shape[3]) , mode='nearest')
            preds = torch.softmax(logits , dim = 1)
            preds = torch.argmax(preds , dim = 1)
            preds = preds.unsqueeze(1)
            predx = preds.float()
            x = unfold(predx)# B (c*p*p) no.of patches
        
        bs, c, h, w = predx.shape
        patch_h, patch_w = inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols
        num_patches = (h // patch_h) * (w // patch_w)
        a = x.reshape(bs, c, patch_h, patch_w, num_patches).permute(0, 4, 1, 2, 3)
        # a -> ( B no.of patches c p p )
        batch, num_patches, channels, patch_height, patch_width = a.shape
        z = a.reshape(batch * num_patches, patch_height * patch_width)

        class_dist = torch.zeros(batch * num_patches, num_classes, device=z.device)
        
        for i in range(class_dist.shape[0]):
            for n in range(num_classes):
                class_dist[i,n] = (z[i] == n ).float().sum()
        class_dist /= class_dist.sum(dim=-1, keepdim=True)

        entropy = -torch.sum(class_dist * torch.log2(class_dist + 1e-12), dim=-1)
        entropy = entropy.view(batch, num_patches)
        entropy = entropy.mean(dim = 0)

        self.update_teacher_class_ent(entropy)

        l_inverted = torch.log(1/(1e-12 + self.teacher_class_ent))
        l_normalized = (l_inverted - l_inverted.min()) / (l_inverted.max() - l_inverted.min())
           
        # Compute KL divergence 
        kl_divs = nn.KLDivLoss(reduction="none")(log_dist1, class_dist) ####### no. of patches x no. of classes
        patch_kl_divs = kl_divs.sum(dim=1) # no. of patches x 1
        patch_kl_divs = patch_kl_divs.reshape(bs,-1).mean(dim =0)
        weighted_kl = patch_kl_divs * l_normalized # entropy weighting
        mean_kl_div = weighted_kl.mean()
        losses['kl_loss'] = mean_kl_div
        dyn_kl = self.x_start + (self.x_end - self.x_start) * (self.local_iter / self.total_iterations)
        losses['total_loss'] = losses['decode.loss_ce'] + self.kl*losses['kl_loss']
        #import pdb;pdb.set_trace()'''
        self.local_iter += 1

        return losses
    
@MODELS.register_module()
class ORACLE12(EncoderDecoder):
    ## global entropy KL losss weighting info from gt ###

    def __init__(self, backbone, decode_head, init_cfg,patch_rows,patch_cols,kl, threshold=0.96, ema_alpha=0.99, class_dist_ema=0.99,ent_ema = 0.99,**kwargs):
        super().__init__(backbone=backbone, decode_head=decode_head, init_cfg=init_cfg, **kwargs)

        self.local_iter = 0
        self.threshold = threshold
        self.ema_alpha = ema_alpha
        self.class_dist_ema = class_dist_ema
        self.ent_ema = ent_ema

        self.patch_rows = patch_rows
        self.patch_cols = patch_cols
        self.kl = kl

        self.teacher_class_dist = None
        self.teacher_class_ent = None

        self.teacher_backb = MODELS.build(backbone)
        self.teacher_dec = MODELS.build(decode_head)

        self.x_start = 0.01
        self.x_end = 0.5
        self.total_iterations = 20000

    def _update_teacher(self, local_iter):
        alpha_teacher = min(1 - 1 / (local_iter + 1), self.ema_alpha)

        for param_name, param in self.backbone.named_parameters():
            if 'lora' in param_name:
                ema_param = self.teacher_backb.state_dict()[param_name]
                ema_param.data[:] = alpha_teacher * ema_param.data + (1 - alpha_teacher) * param.data

    def update_teacher_class_dist(self, new_dist):
        if self.teacher_class_dist is None:
            self.teacher_class_dist = new_dist
        else:
            old_dist_mean = self.teacher_class_dist.mean(dim=0)
            self.teacher_class_dist = self.class_dist_ema * old_dist_mean + (1 - self.class_dist_ema) * new_dist
            #print((new_dist -self.teacher_class_dist ).mean())
    
    def update_teacher_class_ent(self, ent):
        if self.teacher_class_ent is None:
            self.teacher_class_ent = ent
        else:
            self.teacher_class_ent =  self.ent_ema * self.teacher_class_ent + (1 - self.ent_ema) * ent
            #print((new_dist -self.teacher_class_dist ).mean())

    def loss(self, inputs: Tensor, data_samples: SampleList) -> dict:
        """Calculate losses from a batch of inputs and data samples.

        Args:
            inputs (Tensor): Input images.
            data_samples (list[:obj:`SegDataSample`]): The seg data samples.
                It usually includes information such as `metainfo` and
                `gt_sem_seg`.

        Returns:
            dict[str, Tensor]: a dictionary of loss components
        """
        losses = dict()

        if self.local_iter == 0:
            self.backbone_source_state_dict = deepcopy(self.backbone.state_dict())
            self.decoder_source_state_dict = deepcopy(self.decode_head.state_dict())
            prefix = 'teacher_backb.'
            modified_backbone_state_dict = {prefix + k: v for k, v in self.backbone_source_state_dict.items()}
            self.teacher_backb.load_state_dict(modified_backbone_state_dict, strict=False)

            dec_prefix = 'teacher_dec.'
            modified_decoder_state_dict = {dec_prefix + k: v for k, v in self.decoder_source_state_dict.items()}
            self.teacher_dec.load_state_dict(modified_decoder_state_dict, strict=False)

            for n, p in self.teacher_backb.named_parameters():
                p.requires_grad = False

            for n, p in self.teacher_dec.named_parameters():
                p.requires_grad = False

        self.teacher_backb.eval()
        self.teacher_dec.eval()
        
        if self.local_iter > 0:
            self._update_teacher(self.local_iter)

        # get weakly augmented image
        transform = A.Compose([
            A.HorizontalFlip(p=1.0),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_weak_images = []
        transformed_weak_metas = []

        for i in range(inputs.shape[0]):
            image_data = transform(image=inputs[i].permute(1, 2, 0).detach().cpu().numpy())
            image_weak_flipped = image_data["image"].unsqueeze(0).cuda()
            transformed_weak_images.append(image_weak_flipped)
            # create meta
            image_meta_weak_flipped = deepcopy(data_samples[i].metainfo)
            image_meta_weak_flipped['flip'] = True
            image_meta_weak_flipped['flip_direction'] = 'horizontal'
            transformed_weak_metas.append(image_meta_weak_flipped)

        weak_images_batch = torch.cat(transformed_weak_images, dim=0)

        with torch.no_grad():
            # whole inference
            x = self.teacher_backb(weak_images_batch)
            logits = self.teacher_dec.predict(x ,transformed_weak_metas,self.test_cfg)
            ema_softmax = torch.softmax(logits.detach(), dim=1)

            # flip back
            flip = transformed_weak_metas[0]['flip']
            if flip:
                flip_direction = transformed_weak_metas[0]['flip_direction']
                assert flip_direction in ['horizontal', 'vertical']
                if flip_direction == 'horizontal':
                    ema_softmax = ema_softmax.flip(dims=(3,))
                elif flip_direction == 'vertical':
                    ema_softmax = ema_softmax.flip(dims=(2,))

            # compute pseudo weight
            teacher_prob, teacher_pred = torch.max(ema_softmax, dim=1)
            #import pdb; pdb.set_trace()

            ps_large_p = teacher_prob.ge(self.threshold).long() == 1
            ps_size = torch.sum(ps_large_p).item()  # Compute the total number of elements in ps_large_p
            pseudo_weight = ps_size / (inputs.shape[0] * inputs.shape[2] * inputs.shape[3])  # Compute pseudo_weight

            # select most confident predictions
            mask = teacher_prob >= self.threshold
            teacher_pred = torch.where(mask, teacher_pred, 255)
            result = teacher_pred.unsqueeze(1)
            
        del transformed_weak_images
        del weak_images_batch
        
        # get strong augmentation
        strong_transform = A.Compose([
            A.Rotate(limit=30, p=1.0),
            A.Resize(960, 540),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_strong_images = []
        results_strong = []

        # To apply Color transf we need to norm and denorm images
        img_to_transform = deepcopy(inputs)
        # denormalize
        means, stds = get_mean_std(inputs)
        img_to_transform = denorm(img_to_transform, means, stds)

        for i in range(inputs.shape[0]):

            image_data = strong_transform(image=img_to_transform[i].permute(1, 2, 0).detach().cpu().numpy(),
                                          mask=result[i].permute(1, 2, 0).detach().cpu().numpy())
    
            image_strong_aug = image_data["image"].unsqueeze(0).cuda()
            transformed_strong_images.append(image_strong_aug)

            masked_result = image_data["mask"].permute(2, 0, 1).unsqueeze(0).type(torch.long).cuda()
            results_strong.append(masked_result)

        # Detach result and move to CPU
        del result, img_to_transform

        strong_images_batch = torch.cat(transformed_strong_images, dim=0)
        pseudo_labels = torch.cat(results_strong, dim=0)
        # renormalize
        strong_images_batch = renorm(strong_images_batch, means, stds)

        for sample, pseudo_label in zip(data_samples, pseudo_labels):
            sample.gt_sem_seg.data = pseudo_label
        
        #student_losses = self.loss(strong_images_batch,data_samples)
        x = self.extract_feat(strong_images_batch)
        loss_decode = self._decode_head_forward_train(x, data_samples)
        losses.update(loss_decode) 
        losses['decode.loss_ce'] *= pseudo_weight

        #### KL PART ####
        num_classes = 19
        img_paths = [sample.img_path for sample in data_samples] #for full images   
        new_size = (inputs.shape[3], inputs.shape[2]) # for opencv its reversed
        unfold = torch.nn.Unfold(kernel_size=(inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols), stride=(inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols))

        full_image = []
        for imgs in img_paths:
            # Read and convert the image
            img = cv2.imread(imgs)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            resized_image = cv2.resize(img, new_size)
            resized_image = resized_image.astype(np.float32) 
            resized_image = torch.tensor(resized_image).permute(2, 0, 1)  
            mean = torch.tensor([123.675, 116.28, 103.53]).view(3, 1, 1)
            std = torch.tensor([58.395, 57.12, 57.375]).view(3, 1, 1)
            normalized_image = (resized_image - mean) / std
            full_image.append(normalized_image)

        # Stack all images into a single tensor and move to GPU if available
        full_image = torch.stack(full_image, dim=0).cuda()

        ##Student
        logits = self._forward(full_image) # without postprocessing from student
        logits = F.interpolate(logits , size = (inputs.shape[2], inputs.shape[3]) , mode='nearest')
        preds = torch.softmax(logits , dim =1)
        preds = torch.argmax(preds , dim = 1)
        preds = preds.unsqueeze(1)
        predx = preds.float()
        x = unfold(predx) # B (c*p*p) no.of patches
        bs, c, h, w = predx.shape
        patch_h, patch_w = inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols
        num_patches = (h // patch_h) * (w // patch_w)
        a = x.reshape(bs, c, patch_h, patch_w, num_patches).permute(0, 4, 1, 2, 3)
        # a -> ( B no.of patches c p p )
        batch, num_patches, channels, patch_height, patch_width = a.shape
        z = a.reshape(batch * num_patches, patch_height * patch_width)

        preds_dist = torch.zeros(batch * num_patches, num_classes, device=z.device)
        for i in range(preds_dist.shape[0]):
            for n in range(num_classes):
                preds_dist[i,n] = (z[i] == n ).float().sum()
        preds_dist /= preds_dist.sum(dim=-1, keepdim=True)

        log_dist1 = torch.log(preds_dist + 1e-12)  # Adding small value to avoid log(0)

        ##Teacher

        with torch.no_grad():
            logits = self.teacher_backb(full_image) # without postprocessing from student
            logits = self.teacher_dec.forward(logits)
            logits = F.interpolate(logits , size = (inputs.shape[2], inputs.shape[3]) , mode='nearest')
            preds = torch.softmax(logits , dim = 1)
            preds = torch.argmax(preds , dim = 1)
            preds = preds.unsqueeze(1)
            predx = preds.float()
            x = unfold(predx)# B (c*p*p) no.of patches

        bs, c, h, w = predx.shape
        patch_h, patch_w = inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols
        num_patches = (h // patch_h) * (w // patch_w)
        a = x.reshape(bs, c, patch_h, patch_w, num_patches).permute(0, 4, 1, 2, 3)
        # a -> ( B no.of patches c p p )
        batch, num_patches, channels, patch_height, patch_width = a.shape
        z = a.reshape(batch * num_patches, patch_height * patch_width)

        class_dist = torch.zeros(batch * num_patches, num_classes, device=z.device)
        
        for i in range(class_dist.shape[0]):
            for n in range(num_classes):
                class_dist[i,n] = (z[i] == n ).float().sum()
        class_dist /= class_dist.sum(dim=-1, keepdim=True)

        self.update_teacher_class_dist(class_dist)

        entropy = -torch.sum(self.teacher_class_dist * torch.log2(self.teacher_class_dist + 1e-12), dim=-1)
        entropy = entropy.view(batch, num_patches)
        entropy = entropy.mean(dim = 0)

        self.update_teacher_class_ent(entropy)

        l_inverted = torch.log(1/(1e-12 + self.teacher_class_ent))
        l_normalized = (l_inverted - l_inverted.min()) / (l_inverted.max() - l_inverted.min())
           
        # Compute KL divergence 
        kl_divs = nn.KLDivLoss(reduction="none")(log_dist1, class_dist) ####### no. of patches x no. of classes
        patch_kl_divs = kl_divs.sum(dim=1) # no. of patches x 1
        patch_kl_divs = patch_kl_divs.reshape(bs,-1).mean(dim =0)
        weighted_kl = patch_kl_divs * l_normalized # entropy weighting
        mean_kl_div = weighted_kl.mean()
        losses['kl_loss'] = mean_kl_div
        dyn_kl = self.x_start + (self.x_end - self.x_start) * (self.local_iter / self.total_iterations)
        losses['total_loss'] = losses['decode.loss_ce'] + self.kl*losses['kl_loss']
        print(f'Inver weighted with KL weight {self.kl}')
        print(f'teacher dist and ent updated')
        #import pdb;pdb.set_trace()'''
        self.local_iter += 1

        return losses
    

@MODELS.register_module()
class ORACLE13(EncoderDecoder):
    ## global entropy KL losss weighting info from gt ###

    def __init__(self, backbone, decode_head, init_cfg,patch_rows,patch_cols,kl, threshold=0.96, ema_alpha=0.99, class_dist_ema=0.99,ent_ema = 0.99,**kwargs):
        super().__init__(backbone=backbone, decode_head=decode_head, init_cfg=init_cfg, **kwargs)

        self.local_iter = 0
        self.threshold = threshold
        self.ema_alpha = ema_alpha
        self.class_dist_ema = class_dist_ema
        self.ent_ema = ent_ema

        self.patch_rows = patch_rows
        self.patch_cols = patch_cols
        self.kl = kl

        self.teacher_class_dist = None
        self.teacher_class_ent = None

        self.teacher_backb = MODELS.build(backbone)
        self.teacher_dec = MODELS.build(decode_head)

        self.x_start = 0.01
        self.x_end = 0.5
        self.total_iterations = 20000

    def _update_teacher(self, local_iter):
        alpha_teacher = min(1 - 1 / (local_iter + 1), self.ema_alpha)

        for param_name, param in self.backbone.named_parameters():
            if 'lora' in param_name:
                ema_param = self.teacher_backb.state_dict()[param_name]
                ema_param.data[:] = alpha_teacher * ema_param.data + (1 - alpha_teacher) * param.data

    def update_teacher_class_dist(self, new_dist):
        if self.teacher_class_dist is None:
            self.teacher_class_dist = new_dist
        else:
            old_dist_mean = self.teacher_class_dist.mean(dim=0)
            self.teacher_class_dist = self.class_dist_ema * old_dist_mean + (1 - self.class_dist_ema) * new_dist
            #print((new_dist -self.teacher_class_dist ).mean())
    
    def update_teacher_class_ent(self, ent):
        if self.teacher_class_ent is None:
            self.teacher_class_ent = ent
        else:
            self.teacher_class_ent =  self.ent_ema * self.teacher_class_ent + (1 - self.ent_ema) * ent
            #print((new_dist -self.teacher_class_dist ).mean())

    def loss(self, inputs: Tensor, data_samples: SampleList) -> dict:
        """Calculate losses from a batch of inputs and data samples.

        Args:
            inputs (Tensor): Input images.
            data_samples (list[:obj:`SegDataSample`]): The seg data samples.
                It usually includes information such as `metainfo` and
                `gt_sem_seg`.

        Returns:
            dict[str, Tensor]: a dictionary of loss components
        """
        losses = dict()

        if self.local_iter == 0:
            self.backbone_source_state_dict = deepcopy(self.backbone.state_dict())
            self.decoder_source_state_dict = deepcopy(self.decode_head.state_dict())
            prefix = 'teacher_backb.'
            modified_backbone_state_dict = {prefix + k: v for k, v in self.backbone_source_state_dict.items()}
            self.teacher_backb.load_state_dict(modified_backbone_state_dict, strict=False)

            dec_prefix = 'teacher_dec.'
            modified_decoder_state_dict = {dec_prefix + k: v for k, v in self.decoder_source_state_dict.items()}
            self.teacher_dec.load_state_dict(modified_decoder_state_dict, strict=False)

            for n, p in self.teacher_backb.named_parameters():
                p.requires_grad = False

            for n, p in self.teacher_dec.named_parameters():
                p.requires_grad = False

        self.teacher_backb.eval()
        self.teacher_dec.eval()
        
        if self.local_iter > 0:
            self._update_teacher(self.local_iter)

        # get weakly augmented image
        transform = A.Compose([
            A.HorizontalFlip(p=1.0),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_weak_images = []
        transformed_weak_metas = []

        for i in range(inputs.shape[0]):
            image_data = transform(image=inputs[i].permute(1, 2, 0).detach().cpu().numpy())
            image_weak_flipped = image_data["image"].unsqueeze(0).cuda()
            transformed_weak_images.append(image_weak_flipped)
            # create meta
            image_meta_weak_flipped = deepcopy(data_samples[i].metainfo)
            image_meta_weak_flipped['flip'] = True
            image_meta_weak_flipped['flip_direction'] = 'horizontal'
            transformed_weak_metas.append(image_meta_weak_flipped)

        weak_images_batch = torch.cat(transformed_weak_images, dim=0)

        with torch.no_grad():
            # whole inference
            x = self.teacher_backb(weak_images_batch)
            logits = self.teacher_dec.predict(x ,transformed_weak_metas,self.test_cfg)
            ema_softmax = torch.softmax(logits.detach(), dim=1)

            # flip back
            flip = transformed_weak_metas[0]['flip']
            if flip:
                flip_direction = transformed_weak_metas[0]['flip_direction']
                assert flip_direction in ['horizontal', 'vertical']
                if flip_direction == 'horizontal':
                    ema_softmax = ema_softmax.flip(dims=(3,))
                elif flip_direction == 'vertical':
                    ema_softmax = ema_softmax.flip(dims=(2,))

            # compute pseudo weight
            teacher_prob, teacher_pred = torch.max(ema_softmax, dim=1)
            #import pdb; pdb.set_trace()

            ps_large_p = teacher_prob.ge(self.threshold).long() == 1
            ps_size = torch.sum(ps_large_p).item()  # Compute the total number of elements in ps_large_p
            pseudo_weight = ps_size / (inputs.shape[0] * inputs.shape[2] * inputs.shape[3])  # Compute pseudo_weight

            # select most confident predictions
            mask = teacher_prob >= self.threshold
            teacher_pred = torch.where(mask, teacher_pred, 255)
            result = teacher_pred.unsqueeze(1)
            
        del transformed_weak_images
        del weak_images_batch
        
        # get strong augmentation
        strong_transform = A.Compose([
            A.Rotate(limit=30, p=1.0),
            A.Resize(960, 540),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_strong_images = []
        results_strong = []

        # To apply Color transf we need to norm and denorm images
        img_to_transform = deepcopy(inputs)
        # denormalize
        means, stds = get_mean_std(inputs)
        img_to_transform = denorm(img_to_transform, means, stds)

        for i in range(inputs.shape[0]):

            image_data = strong_transform(image=img_to_transform[i].permute(1, 2, 0).detach().cpu().numpy(),
                                          mask=result[i].permute(1, 2, 0).detach().cpu().numpy())
    
            image_strong_aug = image_data["image"].unsqueeze(0).cuda()
            transformed_strong_images.append(image_strong_aug)

            masked_result = image_data["mask"].permute(2, 0, 1).unsqueeze(0).type(torch.long).cuda()
            results_strong.append(masked_result)

        # Detach result and move to CPU
        del result, img_to_transform

        strong_images_batch = torch.cat(transformed_strong_images, dim=0)
        pseudo_labels = torch.cat(results_strong, dim=0)
        # renormalize
        strong_images_batch = renorm(strong_images_batch, means, stds)

        for sample, pseudo_label in zip(data_samples, pseudo_labels):
            sample.gt_sem_seg.data = pseudo_label
        
        #student_losses = self.loss(strong_images_batch,data_samples)
        x = self.extract_feat(strong_images_batch)
        loss_decode = self._decode_head_forward_train(x, data_samples)
        losses.update(loss_decode) 
        losses['decode.loss_ce'] *= pseudo_weight
        
        
        #### KL PART ####
        
        num_classes = 19
        unfold = torch.nn.Unfold(kernel_size=(inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols), stride=(inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols))

        #Teacher
        with torch.no_grad():
            logits = self.teacher_backb(inputs) # without postprocessing from student
            logits = self.teacher_dec.forward(logits)
            logits = F.interpolate(logits , size = (inputs.shape[2], inputs.shape[3]) , mode='nearest')
            preds = torch.softmax(logits , dim = 1)
            preds = torch.argmax(preds , dim = 1)
            preds = preds.unsqueeze(1)
            predx = preds.float()
            x = unfold(predx)# B (c*p*p) no.of patches

        bs, c, h, w = predx.shape
        patch_h, patch_w = inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols
        num_patches = (h // patch_h) * (w // patch_w)
        a = x.reshape(bs, c, patch_h, patch_w, num_patches).permute(0, 4, 1, 2, 3)
        # a -> ( B no.of patches c p p )
        batch, num_patches, channels, patch_height, patch_width = a.shape
        z = a.reshape(batch * num_patches, patch_height * patch_width)

        class_dist = torch.zeros(batch * num_patches, num_classes, device=z.device)
        
        for i in range(class_dist.shape[0]):
            for n in range(num_classes):
                class_dist[i,n] = (z[i] == n ).float().sum()

        class_dist /= (class_dist.sum(dim=-1, keepdim=True) + 1e-12)

        entropy = -torch.sum(class_dist * torch.log2(class_dist + 1e-12), dim=-1)
        entropy = entropy.view(batch, num_patches)
        entropy = entropy.mean(dim = 0)

        l_inverted = torch.log(1/(1e-12 + entropy))
        l_normalized = (l_inverted - l_inverted.min()) / (l_inverted.max() - l_inverted.min()) #no. of patches x 1

        
        #Student
        img_paths = [sample.img_path for sample in data_samples] #for full images   
        new_size = (inputs.shape[2], inputs.shape[3]) # for opencv its reversed

        logits = self._forward(inputs) # without postprocessing from student
        logits = F.interpolate(logits , size = (inputs.shape[2], inputs.shape[3]) , mode='nearest')
        preds = torch.softmax(logits , dim =1)
        preds = torch.argmax(preds , dim = 1)
        preds = preds.unsqueeze(1)
        predx = preds.float()
        x = unfold(predx) # B (c*p*p) no.of patches
        bs, c, h, w = predx.shape
        patch_h, patch_w = inputs.shape[2] // self.patch_rows, inputs.shape[3] // self.patch_cols
        num_patches = (h // patch_h) * (w // patch_w)
        a = x.reshape(bs, c, patch_h, patch_w, num_patches).permute(0, 4, 1, 2, 3)
        # a -> ( B no.of patches c p p )
        batch, num_patches, channels, patch_height, patch_width = a.shape
        z = a.reshape(batch * num_patches, patch_height * patch_width)

        preds_dist = torch.zeros(batch * num_patches, num_classes, device=z.device)
        for i in range(preds_dist.shape[0]):
            for n in range(num_classes):
                preds_dist[i,n] = (z[i] == n ).float().sum()
        preds_dist /= preds_dist.sum(dim=-1, keepdim=True)

        log_dist1 = torch.log(preds_dist + 1e-12)  # Adding small value to avoid log(0)

        # Compute KL divergence 
        kl_divs = nn.KLDivLoss(reduction="none")(log_dist1, class_dist) ####### no. of patches in batch x no. of classes
        patch_kl_divs = kl_divs.sum(dim=1) # no. of patches x 1
        patch_kl_divs = patch_kl_divs.reshape(bs,-1).mean(dim = 0) # bs x no. of patches x 1 -> no. of patches x 1
        weighted_kl = patch_kl_divs * l_normalized # entropy weighting
        mean_kl_div = patch_kl_divs.mean()

        losses['kl_loss'] = mean_kl_div

        dyn_kl = self.x_start + (self.x_end - self.x_start) * (self.local_iter / self.total_iterations)
        losses['total_loss'] = losses['decode.loss_ce'] + self.kl*losses['kl_loss']
        print(f'using kl {self.kl}')

        #'import pdb;pdb.set_trace()
        
        self.local_iter += 1

        return losses


@MODELS.register_module()
class ORACLE14(EncoderDecoder):
    ## global entropy KL losss weighting info from gt ###

    def __init__(self, backbone, decode_head, init_cfg,patch_rows,patch_cols,kl, threshold=0.96, ema_alpha=0.99, class_dist_ema=0.99,ent_ema = 0.99,**kwargs):
        super().__init__(backbone=backbone, decode_head=decode_head, init_cfg=init_cfg, **kwargs)

        self.local_iter = 0
        self.threshold = threshold
        self.ema_alpha = ema_alpha
        self.class_dist_ema = class_dist_ema
        self.ent_ema = ent_ema

        self.patch_rows = patch_rows
        self.patch_cols = patch_cols
        self.kl = kl

        self.teacher_class_dist = None
        self.teacher_class_ent = None

        self.teacher_backb = MODELS.build(backbone)
        self.teacher_dec = MODELS.build(decode_head)

        self.x_start = 0.01
        self.x_end = 0.5
        self.total_iterations = 20000

    def _update_teacher(self, local_iter):
        alpha_teacher = min(1 - 1 / (local_iter + 1), self.ema_alpha)

        for param_name, param in self.backbone.named_parameters():
            if 'lora' in param_name:
                ema_param = self.teacher_backb.state_dict()[param_name]
                ema_param.data[:] = alpha_teacher * ema_param.data + (1 - alpha_teacher) * param.data

    def update_teacher_class_dist(self, new_dist):
        if self.teacher_class_dist is None:
            self.teacher_class_dist = new_dist
        else:
            old_dist_mean = self.teacher_class_dist.mean(dim=0)
            self.teacher_class_dist = self.class_dist_ema * old_dist_mean + (1 - self.class_dist_ema) * new_dist
            #print((new_dist -self.teacher_class_dist ).mean())
    
    def update_teacher_class_ent(self, ent):
        if self.teacher_class_ent is None:
            self.teacher_class_ent = ent
        else:
            self.teacher_class_ent =  self.ent_ema * self.teacher_class_ent + (1 - self.ent_ema) * ent
            #print((new_dist -self.teacher_class_dist ).mean())

    def loss(self, inputs: Tensor, data_samples: SampleList) -> dict:
        """Calculate losses from a batch of inputs and data samples.

        Args:
            inputs (Tensor): Input images.
            data_samples (list[:obj:`SegDataSample`]): The seg data samples.
                It usually includes information such as `metainfo` and
                `gt_sem_seg`.

        Returns:
            dict[str, Tensor]: a dictionary of loss components
        """
        losses = dict()

        if self.local_iter == 0:
            self.backbone_source_state_dict = deepcopy(self.backbone.state_dict())
            self.decoder_source_state_dict = deepcopy(self.decode_head.state_dict())
            prefix = 'teacher_backb.'
            modified_backbone_state_dict = {prefix + k: v for k, v in self.backbone_source_state_dict.items()}
            self.teacher_backb.load_state_dict(modified_backbone_state_dict, strict=False)

            dec_prefix = 'teacher_dec.'
            modified_decoder_state_dict = {dec_prefix + k: v for k, v in self.decoder_source_state_dict.items()}
            self.teacher_dec.load_state_dict(modified_decoder_state_dict, strict=False)

            for n, p in self.teacher_backb.named_parameters():
                p.requires_grad = False

            for n, p in self.teacher_dec.named_parameters():
                p.requires_grad = False

        self.teacher_backb.eval()
        self.teacher_dec.eval()
        
        if self.local_iter > 0:
            self._update_teacher(self.local_iter)

        # get weakly augmented image
        transform = A.Compose([
            A.HorizontalFlip(p=1.0),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_weak_images = []
        transformed_weak_metas = []

        for i in range(inputs.shape[0]):
            image_data = transform(image=inputs[i].permute(1, 2, 0).detach().cpu().numpy())
            image_weak_flipped = image_data["image"].unsqueeze(0).cuda()
            transformed_weak_images.append(image_weak_flipped)
            # create meta
            image_meta_weak_flipped = deepcopy(data_samples[i].metainfo)
            image_meta_weak_flipped['flip'] = True
            image_meta_weak_flipped['flip_direction'] = 'horizontal'
            transformed_weak_metas.append(image_meta_weak_flipped)

        weak_images_batch = torch.cat(transformed_weak_images, dim=0)

        with torch.no_grad():
            # whole inference
            x = self.teacher_backb(weak_images_batch)
            logits = self.teacher_dec.predict(x ,transformed_weak_metas,self.test_cfg)
            ema_softmax = torch.softmax(logits.detach(), dim=1)

            # flip back
            flip = transformed_weak_metas[0]['flip']
            if flip:
                flip_direction = transformed_weak_metas[0]['flip_direction']
                assert flip_direction in ['horizontal', 'vertical']
                if flip_direction == 'horizontal':
                    ema_softmax = ema_softmax.flip(dims=(3,))
                elif flip_direction == 'vertical':
                    ema_softmax = ema_softmax.flip(dims=(2,))

            # compute pseudo weight
            teacher_prob, teacher_pred = torch.max(ema_softmax, dim=1)
            #import pdb; pdb.set_trace()

            ps_large_p = teacher_prob.ge(self.threshold).long() == 1
            ps_size = torch.sum(ps_large_p).item()  # Compute the total number of elements in ps_large_p
            pseudo_weight = ps_size / (inputs.shape[0] * inputs.shape[2] * inputs.shape[3])  # Compute pseudo_weight

            # select most confident predictions
            mask = teacher_prob >= self.threshold
            teacher_pred = torch.where(mask, teacher_pred, 255)
            result = teacher_pred.unsqueeze(1)
            
        del transformed_weak_images
        del weak_images_batch
        
        # get strong augmentation
        strong_transform = A.Compose([
            A.Rotate(limit=30, p=1.0),
            A.Resize(960, 540),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_strong_images = []
        results_strong = []

        # To apply Color transf we need to norm and denorm images
        img_to_transform = deepcopy(inputs)
        # denormalize
        means, stds = get_mean_std(inputs)
        img_to_transform = denorm(img_to_transform, means, stds)

        for i in range(inputs.shape[0]):

            image_data = strong_transform(image=img_to_transform[i].permute(1, 2, 0).detach().cpu().numpy(),
                                          mask=result[i].permute(1, 2, 0).detach().cpu().numpy())
    
            image_strong_aug = image_data["image"].unsqueeze(0).cuda()
            transformed_strong_images.append(image_strong_aug)

            masked_result = image_data["mask"].permute(2, 0, 1).unsqueeze(0).type(torch.long).cuda()
            results_strong.append(masked_result)

        # Detach result and move to CPU
        del result, img_to_transform

        strong_images_batch = torch.cat(transformed_strong_images, dim=0)
        pseudo_labels = torch.cat(results_strong, dim=0)
        # renormalize
        strong_images_batch = renorm(strong_images_batch, means, stds)

        for sample, pseudo_label in zip(data_samples, pseudo_labels):
            sample.gt_sem_seg.data = pseudo_label
        
        #student_losses = self.loss(strong_images_batch,data_samples)
        x = self.extract_feat(strong_images_batch)
        loss_decode = self._decode_head_forward_train(x, data_samples)
        losses.update(loss_decode) 
        losses['decode.loss_ce'] *= pseudo_weight
        
        
        #### KL PART ####
        
        num_classes = 19
        
        img_paths = [sample.img_path for sample in data_samples] #for full images   
        new_size = (inputs.shape[3], inputs.shape[2]) # for opencv its reversed

        full_image = []
        for imgs in img_paths:
            # Read and convert the image
            img = cv2.imread(imgs)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            resized_image = cv2.resize(img, new_size)
            resized_image = resized_image.astype(np.float32) 
            resized_image = torch.tensor(resized_image).permute(2, 0, 1)  
            mean = torch.tensor([123.675, 116.28, 103.53]).view(3, 1, 1)
            std = torch.tensor([58.395, 57.12, 57.375]).view(3, 1, 1)
            normalized_image = (resized_image - mean) / std
            full_image.append(normalized_image)

        # Stack all images into a single tensor and move to GPU if available
        full_image = torch.stack(full_image, dim=0).cuda()
        
        #Teacher
        with torch.no_grad():
            logits = self.teacher_backb(full_image) # without postprocessing from student
            logits = self.teacher_dec.forward(logits)
            #logits = F.interpolate(logits , size = (inputs.shape[2], inputs.shape[3]) , mode='nearest')
            preds = torch.softmax(logits , dim = 1)
            preds = torch.argmax(preds , dim = 1)
            preds = preds.unsqueeze(1)
            predx = preds.float()
            unfoldt = torch.nn.Unfold(kernel_size=(logits.shape[2] // self.patch_rows, logits.shape[3] // self.patch_cols), stride=(logits.shape[2] // self.patch_rows, logits.shape[3] // self.patch_cols))
            x = unfoldt(predx)# B (c*p*p) no.of patches

        bs, c, h, w = predx.shape
        patch_h, patch_w = logits.shape[2] // self.patch_rows, logits.shape[3] // self.patch_cols
        num_patches = (h // patch_h) * (w // patch_w)
        a = x.reshape(bs, c, patch_h, patch_w, num_patches).permute(0, 4, 1, 2, 3)
        # a -> ( B no.of patches c p p )
        batch, num_patches, channels, patch_height, patch_width = a.shape
        z = a.reshape(batch * num_patches, patch_height * patch_width)

        class_dist = torch.zeros(batch * num_patches, num_classes, device=z.device)
        
        for i in range(class_dist.shape[0]):
            for n in range(num_classes):
                class_dist[i,n] = (z[i] == n ).float().sum()

        class_dist /= (class_dist.sum(dim=-1, keepdim=True) + 1e-12)

        entropy = -torch.sum(class_dist * torch.log2(class_dist + 1e-12), dim=-1)
        entropy = entropy.view(batch, num_patches)
        entropy = entropy.mean(dim = 0)

        l_inverted = torch.log(1/(1e-12 + entropy))
        l_normalized = (l_inverted - l_inverted.min()) / (l_inverted.max() - l_inverted.min()) #no. of patches x 1

        
        #Student
        logitx = self._forward(full_image) # without postprocessing from student
        unfold = torch.nn.Unfold(kernel_size=(logitx.shape[2] // self.patch_rows, logitx.shape[3] // self.patch_cols), stride=(logitx.shape[2] // self.patch_rows, logitx.shape[3] // self.patch_cols))
        #logits = F.interpolate(logitx , size = (inputs.shape[2], inputs.shape[3]) , mode='nearest')
        preds = torch.softmax(logitx , dim =1)
        preds = torch.argmax(preds , dim = 1)
        preds = preds.unsqueeze(1)
        predx = preds.float()
        x = unfold(predx) # B (c*p*p) no.of patches
        bs, c, h, w = predx.shape
        patch_h, patch_w = logitx.shape[2] // self.patch_rows, logitx.shape[3] // self.patch_cols
        num_patches = (h // patch_h) * (w // patch_w)
        patch = x.reshape(bs, c, patch_h, patch_w, num_patches).permute(0, 4, 1, 2, 3)
        # a -> ( B no.of patches c p p )
        batch, num_patches, channels, patch_height, patch_width = patch.shape
        z = patch.reshape(batch * num_patches, patch_height * patch_width)
        
        preds_dist = torch.zeros(batch * num_patches, num_classes, device=z.device)
        
        for i in range(preds_dist.shape[0]):
            for n in range(num_classes):
                preds_dist[i,n] = (z[i] == n ).float().sum()
                
        preds_dist /= preds_dist.sum(dim=-1, keepdim=True)
        #preds_dist = preds_dist.reshape(bs , -1 , num_classes)
        #preds_dist = preds_dist.mean(dim = 0)

        log_dist1 = torch.log(preds_dist + 1e-12)

        #import pdb;pdb.set_trace()

        # Compute KL divergence 
        kl_divs = nn.KLDivLoss(reduction="none")(log_dist1, class_dist) ####### no. of patches in batch x no. of classes
        patch_kl_divs = kl_divs.sum(dim=1) # no. of patches x 1
        patch_kl_divs = patch_kl_divs.reshape(bs,-1).mean(dim = 0) # bs x no. of patches x 1 -> no. of patches x 1
        weighted_kl = patch_kl_divs * l_normalized # entropy weighting
        mean_kl_div = patch_kl_divs.mean()

        losses['kl_loss'] = mean_kl_div

        dyn_kl = self.x_start + (self.x_end - self.x_start) * (self.local_iter / self.total_iterations)
        losses['total_loss'] = losses['decode.loss_ce'] + self.kl*losses['kl_loss']
     
        #'import pdb;pdb.set_trace()
        
        self.local_iter += 1

        return losses
    
@MODELS.register_module()
class IDASS_VPT(EncoderDecoder):

    def __init__(self, backbone, decode_head, init_cfg, threshold=0.96, ema_alpha=0.99, **kwargs):
        super().__init__(backbone=backbone, decode_head=decode_head, init_cfg=init_cfg, **kwargs)

        self.local_iter = 0
        self.threshold = threshold
        self.ema_alpha = ema_alpha

        self.teacher_backb = MODELS.build(backbone)
        self.teacher_dec = MODELS.build(decode_head)

    def _update_teacher(self, local_iter):
        alpha_teacher = min(1 - 1 / (local_iter + 1), self.ema_alpha)

        for param_name, param in self.backbone.named_parameters():
            if 'learnable_tokens' in param_name:
                ema_param = self.teacher_backb.state_dict()[param_name]
                ema_param.data[:] = alpha_teacher * ema_param.data + (1 - alpha_teacher) * param.data

    def loss(self, inputs: Tensor, data_samples: SampleList) -> dict:
        """Calculate losses from a batch of inputs and data samples.

        Args:
            inputs (Tensor): Input images.
            data_samples (list[:obj:`SegDataSample`]): The seg data samples.
                It usually includes information such as `metainfo` and
                `gt_sem_seg`.

        Returns:
            dict[str, Tensor]: a dictionary of loss components
        """
        losses = dict()

        if self.local_iter == 0:
            self.backbone_source_state_dict = deepcopy(self.backbone.state_dict())
            self.decoder_source_state_dict = deepcopy(self.decode_head.state_dict())
            prefix = 'teacher_backb.'
            modified_backbone_state_dict = {prefix + k: v for k, v in self.backbone_source_state_dict.items()}
            self.teacher_backb.load_state_dict(modified_backbone_state_dict, strict=False)

            dec_prefix = 'teacher_dec.'
            modified_decoder_state_dict = {dec_prefix + k: v for k, v in self.decoder_source_state_dict.items()}
            self.teacher_dec.load_state_dict(modified_decoder_state_dict, strict=False)

            for n, p in self.teacher_backb.named_parameters():
                p.requires_grad = False

            for n, p in self.teacher_dec.named_parameters():
                p.requires_grad = False

        self.teacher_backb.eval()
        self.teacher_dec.eval()
        
        if self.local_iter > 0:
            self._update_teacher(self.local_iter)

        # get weakly augmented image
        transform = A.Compose([
            A.HorizontalFlip(p=1.0),
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_weak_images = []
        transformed_weak_metas = []

        for i in range(inputs.shape[0]):
            image_data = transform(image=inputs[i].permute(1, 2, 0).detach().cpu().numpy())
            image_weak_flipped = image_data["image"].unsqueeze(0).cuda()
            transformed_weak_images.append(image_weak_flipped)
            # create meta
            image_meta_weak_flipped = deepcopy(data_samples[i].metainfo)
            image_meta_weak_flipped['flip'] = True
            image_meta_weak_flipped['flip_direction'] = 'horizontal'
            transformed_weak_metas.append(image_meta_weak_flipped)

        weak_images_batch = torch.cat(transformed_weak_images, dim=0)

        with torch.no_grad():
            # whole inference
            x = self.teacher_backb(weak_images_batch)
            logits = self.teacher_dec.predict(x ,transformed_weak_metas,self.test_cfg)
            ema_softmax = torch.softmax(logits.detach(), dim=1)

            # flip back
            flip = transformed_weak_metas[0]['flip']
            if flip:
                flip_direction = transformed_weak_metas[0]['flip_direction']
                assert flip_direction in ['horizontal', 'vertical']
                if flip_direction == 'horizontal':
                    ema_softmax = ema_softmax.flip(dims=(3,))
                elif flip_direction == 'vertical':
                    ema_softmax = ema_softmax.flip(dims=(2,))

            # compute pseudo weight
            teacher_prob, teacher_pred = torch.max(ema_softmax, dim=1)
            #import pdb; pdb.set_trace()

            ps_large_p = teacher_prob.ge(self.threshold).long() == 1
            ps_size = torch.sum(ps_large_p).item()  # Compute the total number of elements in ps_large_p
            pseudo_weight = ps_size / (inputs.shape[0] * inputs.shape[2] * inputs.shape[3])  # Compute pseudo_weight

            # select most confident predictions
            mask = teacher_prob >= self.threshold
            teacher_pred = torch.where(mask, teacher_pred, 255)
            result = teacher_pred.unsqueeze(1)
            
        del transformed_weak_images
        del weak_images_batch
        
        # get strong augmentation
        strong_transform = A.Compose([
            A.Rotate(limit=30, p=1.0),
            A.Resize(960, 540), # same as crop_size, change this when using differnt cropsize
            ToTensorV2()
        ], is_check_shapes=False)

        transformed_strong_images = []
        results_strong = []

        # To apply Color transf we need to norm and denorm images
        img_to_transform = deepcopy(inputs)
        # denormalize
        means, stds = get_mean_std(inputs)
        img_to_transform = denorm(img_to_transform, means, stds)

        for i in range(inputs.shape[0]):

            image_data = strong_transform(image=img_to_transform[i].permute(1, 2, 0).detach().cpu().numpy(),
                                          mask=result[i].permute(1, 2, 0).detach().cpu().numpy())
    
            
            image_strong_aug = image_data["image"].unsqueeze(0).cuda()
            transformed_strong_images.append(image_strong_aug)

            masked_result = image_data["mask"].permute(2, 0, 1).unsqueeze(0).type(torch.long).cuda()
            results_strong.append(masked_result)

        
        # Detach result and move to CPU
        del result, img_to_transform

        strong_images_batch = torch.cat(transformed_strong_images, dim=0)
        pseudo_labels = torch.cat(results_strong, dim=0)
        # renormalize
        strong_images_batch = renorm(strong_images_batch, means, stds)

        for sample, pseudo_label in zip(data_samples, pseudo_labels):
            sample.gt_sem_seg.data = pseudo_label
        
        #student_losses = self.loss(strong_images_batch,data_samples)
        x = self.extract_feat(strong_images_batch)

        student_losses = dict()     

        loss_decode = self._decode_head_forward_train(x, data_samples)
        
        student_losses.update(loss_decode) 

        student_losses['decode.loss_ce'] *= pseudo_weight
        
        losses.update(student_losses)

        self.local_iter += 1

        return losses
    


class GaussianNoise(torch.nn.Module):
    def __init__(self, mean=0., std=0.7):
        super().__init__()
        self.std = std
        self.mean = mean

    def forward(self, img):
        noise = torch.randn(img.size()) * self.std + self.mean
        noise = noise.to(img.device)
        return img + noise

    def __repr__(self):
        return self.__class__.__name__ + '(mean={0}, std={1})'.format(self.mean, self.std)

class MultiScaleAug(object):
    def __init__(self, scale_factors=[0.5, 0.75, 1.0, 1.25, 1.5, 1.75, 2.0]):
        self.scale_factors = scale_factors

    def __call__(self, img):
        c, h, w = img.shape
        augmented_imgs = []

        for scale in self.scale_factors:
            # Scale the image
            scaled_h, scaled_w = int(h * scale), int(w * scale)
            scaled_img = F.interpolate(img.unsqueeze(0), size=(scaled_h, scaled_w), mode='bilinear', align_corners=False).squeeze(0)
            
            # Add the scaled image
            augmented_imgs.append(scaled_img)
        return augmented_imgs

def augs1():
    train_aug = transforms.Compose([
        transforms.ColorJitter(brightness=0.15, contrast=0.15, saturation=0.15, hue=0.15)])
    return train_aug

def augs2():
    train_aug = transforms.Compose([
        transforms.GaussianBlur(kernel_size=(55,93), sigma= [0.1, 0.9])])
    return train_aug

def augs3():
    train_aug = transforms.Compose([
        GaussianNoise()])
    return train_aug

@MODELS.register_module()
class COTTA(EncoderDecoder):

    def __init__(self, backbone, decode_head, init_cfg, threshold=0.96, ema_alpha=0.99, **kwargs):
        super().__init__(backbone=backbone, decode_head=decode_head, init_cfg=init_cfg, **kwargs)

        self.local_iter = 0
        self.threshold = threshold
        self.ema_alpha = ema_alpha
        self.transform1 = augs1()  
        self.transform2 = augs2()
        self.transform3 = augs3()
        self.multi_scale_aug = MultiScaleAug()

        self.teacher_backb = MODELS.build(backbone)
        self.teacher_dec = MODELS.build(decode_head)

        '''self.model_state = {}
        for n,p in self.backbone.named_parameters():
            n_modified = n.replace('layers', 'backbone', 1)
            self.model_state[n_modified] = deepcopy(p)
        for n,p in self.decode_head.named_parameters():
            n_modified = n.replace('convs', 'decode_head', 1)
            self.model_state[n] = deepcopy(p)'''

    def _update_teacher(self, local_iter):
        alpha_teacher = min(1 - 1 / (local_iter + 1), self.ema_alpha)

        for param_name, param in self.backbone.named_parameters():
                ema_param = self.teacher_backb.state_dict()[param_name]
                ema_param.data[:] = alpha_teacher * ema_param.data + (1 - alpha_teacher) * param.data
        
        for param_name, param in self.decode_head.named_parameters():
                ema_param = self.teacher_dec.state_dict()[param_name]
                ema_param.data[:] = alpha_teacher * ema_param.data + (1 - alpha_teacher) * param.data
    
    '''def stochastic_restoration(self):
        """Applies stochastic restoration to the student's model parameters."""
        for module_name, state_dict in [("backbone", self.backbone), ("decode_head", self.decode_head)]:
            #import pdb;pdb.set_trace()
            for n, p in state_dict.named_parameters():
                if any(x in n for x in ['weight', 'bias']) and p.requires_grad:
                    mask = (torch.rand(p.shape) < 0.001).float().cuda()
                    with torch.no_grad():
                        if (module_name == 'backbone'):
                            parts = n.split('.')
                            new_n = '.'.join(parts[1:])
                            #import pdb;pdb.set_trace()
                            p.data = self.model_state[f"{module_name}.{new_n}"].cuda() * mask + p * (1.0 - mask)'''
     
    def loss(self, inputs: Tensor, data_samples: SampleList) -> dict:
        losses = dict()

        if self.local_iter == 0:
            self.backbone_source_state_dict = deepcopy(self.backbone.state_dict())
            self.decoder_source_state_dict = deepcopy(self.decode_head.state_dict())
            prefix = 'teacher_backb.'
            modified_backbone_state_dict = {prefix + k: v for k, v in self.backbone_source_state_dict.items()}
            self.teacher_backb.load_state_dict(modified_backbone_state_dict, strict=False)

            dec_prefix = 'teacher_dec.'
            modified_decoder_state_dict = {dec_prefix + k: v for k, v in self.decoder_source_state_dict.items()}
            self.teacher_dec.load_state_dict(modified_decoder_state_dict, strict=False)

            for n, p in self.teacher_backb.named_parameters():
                p.requires_grad = False

            for n, p in self.teacher_dec.named_parameters():
                p.requires_grad = False

        self.teacher_backb.eval()
        self.teacher_dec.eval()
        
        if self.local_iter > 0:
            self._update_teacher(self.local_iter)
            #self.stochastic_restoration()

        img_paths = [sample.img_path for sample in data_samples] #for full images   
        new_size = (inputs.shape[3], inputs.shape[2]) 
        full_image = []
        for imgs in img_paths:
            # Read and convert the image
            img = cv2.imread(imgs)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            resized_image = cv2.resize(img, new_size)
            resized_image = resized_image.astype(np.float32) 
            resized_image = torch.tensor(resized_image).permute(2, 0, 1)  
            mean = torch.tensor([123.675, 116.28, 103.53]).view(3, 1, 1)
            std = torch.tensor([58.395, 57.12, 57.375]).view(3, 1, 1)
            normalized_image = (resized_image - mean) / std
            full_image.append(normalized_image)

        # Stack all images into a single tensor and move to GPU if available
        full_image = torch.stack(full_image, dim=0).cuda()

        batch_img_metas = [
                    dict(
                        ori_shape=full_image.shape[2:],
                        img_shape=full_image.shape[2:],
                        pad_shape=full_image.shape[2:],
                        padding_size=[0, 0, 0, 0]
                    )
                ] * full_image.shape[0]

        with torch.no_grad():
            x = self.transform1(full_image)
            y = self.transform2(full_image)
            z = self.transform3(full_image)

            logits1 = self.teacher_backb(x) # without postprocessing from student
            logits1 = self.teacher_dec.forward(logits1)

            logits2 = self.teacher_backb(y) # without postprocessing from student
            logits2 = self.teacher_dec.forward(logits2)

            logits3 = self.teacher_backb(z) # without postprocessing from student
            logits3 = self.teacher_dec.forward(logits3)
            

            logits = (logits1 + logits2 + logits3  )/3.
            logits = F.interpolate(logits , size = (inputs.shape[2], inputs.shape[3]) ,mode='bilinear', align_corners=False)
            ema_softmax = torch.softmax(logits , dim = 1)

            # compute pseudo weight
            teacher_prob, teacher_pred = torch.max(ema_softmax, dim=1)
            
            ps_large_p = teacher_prob.ge(self.threshold).long() == 1
            ps_size = torch.sum(ps_large_p).item()  # Compute the total number of elements in ps_large_p
            pseudo_weight = ps_size / (inputs.shape[0] * inputs.shape[2] * inputs.shape[3])  # Compute pseudo_weight

            # select most confident predictions
            mask = teacher_prob >= self.threshold
            teacher_pred = torch.where(mask, teacher_pred, 255)
            #result = teacher_pred.unsqueeze(1)

            #standard_ema = self.t_model.module.encode_decode(full_image, batch_img_metas)
            standard_ema = self.teacher_backb(full_image) # without postprocessing from student
            standard_ema = self.teacher_dec.forward(standard_ema)
            standard_ema = F.interpolate(standard_ema , size = (inputs.shape[2], inputs.shape[3]) ,mode='bilinear', align_corners=False)
            standard_ema = F.softmax(standard_ema, dim=1)
            standard_ema= torch.argmax(standard_ema, dim=1)
            t_pred = torch.where(mask, standard_ema, teacher_pred)
            #import pdb;pdb.set_trace()

        for sample, pseudo_label in zip(data_samples, t_pred):
            sample.gt_sem_seg.data = pseudo_label
        
        #student_losses = self.loss(strong_images_batch,data_samples)
        s_pred = self.extract_feat(full_image)

        student_losses = dict()     

        loss_decode = self._decode_head_forward_train(s_pred, data_samples)
        
        student_losses.update(loss_decode) 

        #student_losses['decode.loss_ce'] *= pseudo_weight
        
        losses.update(student_losses)

        self.local_iter += 1

        return losses
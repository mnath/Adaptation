import warnings
warnings.simplefilter("ignore", UserWarning)
import torch.nn.functional as F
from mmseg.ops import resize

import argparse
import copy
import mmcv
import os
import os.path as osp
import time
import torch
from mmcv.runner import init_dist, load_checkpoint
from mmcv.utils import Config, DictAction, get_git_hash
import torch.nn as nn

import numpy as np
import torch.optim as optim

from mmseg import __version__
from mmseg.apis import set_random_seed, train_segmentor
from mmseg.datasets import build_dataset
from mmseg.models import build_segmentor, build_train_model, build_moe
from mmseg.utils import collect_env, get_root_logger
import loralib as lora
from torch.utils.tensorboard import SummaryWriter
import torch.nn as nn
from mmseg.datasets import build_dataloader, build_dataset
from torch.utils.data import DataLoader, Dataset
from torchvision.transforms import ToTensor
import random

import albumentations as A
from albumentations.pytorch import ToTensorV2
import matplotlib.pyplot as plt
from mmcv.parallel import MMDataParallel, MMDistributedDataParallel
from mmcv.runner import get_dist_info, init_dist, load_checkpoint

from mmseg.apis import multi_gpu_test, single_gpu_test,  single_gpu_test_gate
from mmseg.datasets import build_dataloader, build_dataset
from mmseg.models import build_segmentor


def load_models():
    fog_config = "/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/local_configs/segformer/B5/segformer.b5.1024x1024.acdc_fog_lora.160k.py"
    fog_idas = Config.fromfile(fog_config)
    model_fog = build_train_model(
        fog_idas,
        train_cfg=fog_idas.get('train_cfg'),
        test_cfg=fog_idas.get('test_cfg'))

    checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/run11/latest.pth'
    checkpoint = load_checkpoint(model_fog, checkpoint, map_location='cpu')
    model_fog = MMDataParallel(model_fog.model_teacher, device_ids=[0])

    # Night
    night_config = "/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/local_configs/segformer/B5/segformer.b5.1024x1024.acdc_night_lora.160k.py"
    night_idas = Config.fromfile(night_config)
    
    model_night = build_train_model(
        night_idas,
        train_cfg=night_idas.get('train_cfg'),
        test_cfg=night_idas.get('test_cfg'))

    checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/run9/latest.pth'
    checkpoint = load_checkpoint(model_night, checkpoint, map_location='cpu')
    model_night = MMDataParallel(model_night.model_teacher, device_ids=[0])
    
    # Rain
    rain_config = "/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/local_configs/segformer/B5/segformer.b5.1024x1024.acdc_rain_lora.160k.py"
    rain_config = Config.fromfile(rain_config)
    model_rain = build_train_model(
        rain_config,
        train_cfg=rain_config.get('train_cfg'),
        test_cfg=rain_config.get('test_cfg'))

    checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/run12/latest.pth'
    checkpoint = load_checkpoint(model_rain, checkpoint, map_location='cpu')
    model_rain = MMDataParallel(model_rain.model_teacher, device_ids=[0])
    
    # Snow
    snow_config = "/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/local_configs/segformer/B5/segformer.b5.1024x1024.acdc_snow_lora.160k.py"
    snow_config = Config.fromfile(snow_config)
    model_snow = build_train_model(
        snow_config,
        train_cfg=snow_config.get('train_cfg'),
        test_cfg=snow_config.get('test_cfg'))

    checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/run13/latest.pth'
    checkpoint = load_checkpoint(model_snow, checkpoint, map_location='cpu')
    model_snow = MMDataParallel(model_snow.model_teacher, device_ids=[0])#.model_teacher
    
    # set models to device
    model_fog.cuda()
    model_night.cuda()
    model_rain.cuda()
    model_snow.cuda()
    
    model_fog.eval()
    model_night.eval()
    model_rain.eval()
    model_snow.eval()
    
    return model_fog, model_night, model_rain, model_snow
    
    
def load_datasets(cfg, distributed):
    datasets = [build_dataset(cfg.data.train1), build_dataset(cfg.data.train2),
                build_dataset(cfg.data.train3), build_dataset(cfg.data.train4)]

    dataset = datasets if isinstance(datasets, (list, tuple)) else [datasets]
    data_loaders = [
        build_dataloader(
            ds,
            cfg.data.samples_per_gpu,
            cfg.data.workers_per_gpu,
            len(cfg.gpu_ids),
            dist=distributed,
            seed=0,
            drop_last=True) for ds in dataset
    ]
    
    val_dataset = [build_dataset(cfg.data.val, dict(test_mode=True)),
                   build_dataset(cfg.data.val2, dict(test_mode=True)),
                   build_dataset(cfg.data.val3, dict(test_mode=True)), 
                   build_dataset(cfg.data.val4, dict(test_mode=True))]
    
    val_dataloader = [build_dataloader(
        ds,
        samples_per_gpu=1,
        workers_per_gpu=cfg.data.workers_per_gpu,
        dist=distributed,
        shuffle=False) for ds in val_dataset]
    
    return data_loaders, val_dataset, val_dataloader
    
    
# define gating network
class GatingNet(nn.Module):
    def __init__(self):
        super(GatingNet, self).__init__()
        self.linear = nn.Linear(261120, 2)
        
    def forward(self, x):
        x = x.view(x.size(0), -1)
        x = self.linear(x)
        return x
    
    def increase_linear(self):
        self.linear = increase_classifier(self.linear)

        
def increase_classifier(linear_layer):
    old_shape = linear_layer.weight.shape
    new_layer = nn.Linear(old_shape[1], old_shape[0] + 1).cuda()
    
    return new_layer
    
    
# define replay buffer    
class ReplayBuffer(Dataset):
    def __init__(self, buffer_size):
        self.buffer_size = buffer_size
        self.buffer = []

    def add_dict(self, data):
        if len(self.buffer) >= self.buffer_size:
            self.buffer.pop(0)
        self.buffer.append(data)

    def sample_images(self, dataloader, num_samples):
        dataset = dataloader.dataset
        # Randomly sample images from the dataloader
        indices = random.sample(range(len(dataset)), num_samples)

        for idx in indices:
            data = dataset[idx]
            self.add_dict(data)
            
    def __len__(self):
        return len(self.buffer)
    
    def __getitem__(self, idx):
        return self.buffer[idx]
    
    
def parse_args():
    parser = argparse.ArgumentParser(description='Train a segmentor')
    parser.add_argument('config', help='train config file path')
    parser.add_argument('--work-dir', help='the dir to save logs and models')
    parser.add_argument(
        '--load-from', help='the checkpoint file to load weights from')
    parser.add_argument(
        '--resume-from', help='the checkpoint file to resume from')
    parser.add_argument(
        '--no-validate',
        action='store_true',
        help='whether not to evaluate the checkpoint during training')
    group_gpus = parser.add_mutually_exclusive_group()
    group_gpus.add_argument(
        '--gpus',
        type=int,
        help='number of gpus to use '
             '(only applicable to non-distributed training)')
    group_gpus.add_argument(
        '--gpu-ids',
        type=int,
        nargs='+',
        help='ids of gpus to use '
             '(only applicable to non-distributed training)')
    parser.add_argument('--seed', type=int, default=None, help='random seed')
    parser.add_argument(
        '--deterministic',
        action='store_true',
        help='whether to set deterministic options for CUDNN backend.')
    parser.add_argument(
        '--options', nargs='+', action=DictAction, help='custom options')
    parser.add_argument(
        '--launcher',
        choices=['none', 'pytorch', 'slurm', 'mpi'],
        default='none',
        help='job launcher')
    parser.add_argument('--local_rank', type=int, default=0)
    args = parser.parse_args()
    if 'LOCAL_RANK' not in os.environ:
        os.environ['LOCAL_RANK'] = str(args.local_rank)

    return args


def main():
    args = parse_args()
    replay_buffer = ReplayBuffer(buffer_size=400)
    set_random_seed(0, deterministic=True)

    args.eval = "mIoU"
    args.out = None
    args.format_only = False
    
    print("Step 1. Load GatingNet model")
    gate_model = GatingNet().cuda()
        
    cfg = Config.fromfile(args.config)
    print("cfg ", cfg)
    if args.gpu_ids is not None:
        cfg.gpu_ids = args.gpu_ids
    else:
        cfg.gpu_ids = range(1) if args.gpus is None else range(args.gpus)

    if args.launcher == 'none':
        distributed = False
    else:
        distributed = True
        init_dist(args.launcher, **cfg.dist_params)
        
    print("Step 2. Load models")
    model_fog, model_night, model_rain, model_snow = load_models()

    print("Step 3. Load datasets")
    data_loaders, val_dataset, val_dataloader = load_datasets(cfg, distributed)

    
    # criteria and optimiser
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(gate_model.parameters(), lr=0.00001, momentum=0.9)

    # add first 10 random images from fog
    print("Add 10 random fog images")
    replay_buffer.sample_images(data_loaders[0], 10)
    
    for dl in range(1, len(data_loaders)):
        replay_buffer.sample_images(data_loaders[dl], 10)
        replay_loader = build_dataloader(
            replay_buffer,
            cfg.data.samples_per_gpu,
            cfg.data.workers_per_gpu,
            len(cfg.gpu_ids),
            dist=distributed,
            seed=0,
            drop_last=True)
        print(len(replay_loader.dataset))

        # training loop for gate
        for epoch in range(50):
            run_loss = 0.0
 
            for batch_ndx, sample in enumerate(replay_loader):
                img = sample['img'].data[0].cuda()
                img_metas = sample['img_metas'].data[0]
                embeds = []
                
                optimizer.zero_grad()
                with torch.no_grad():
                    if dl == 1:
                        fog_embeds = model_fog.module.extract_feat(img)[3]
                        night_embeds = model_night.module.extract_feat(img)[3]
                        embeds.append(fog_embeds)
                        embeds.append(night_embeds)
                
                    elif dl == 2:
                        fog_embeds = model_fog.module.extract_feat(img)[3]
                        night_embeds = model_night.module.extract_feat(img)[3]
                        rain_embeds = model_rain.module.extract_feat(img)[3]
                        embeds.append(fog_embeds)
                        embeds.append(night_embeds)
                        embeds.append(rain_embeds)
                
                    else:
                        fog_embeds = model_fog.module.extract_feat(img)[3]
                        night_embeds = model_night.module.extract_feat(img)[3]
                        rain_embeds = model_rain.module.extract_feat(img)[3]
                        snow_embeds = model_snow.module.extract_feat(img)[3]
                        
                        embeds.append(fog_embeds)
                        embeds.append(night_embeds)
                        embeds.append(rain_embeds)
                        embeds.append(snow_embeds)
                
                embeds = torch.cat(embeds, dim = 0)    
                
                gate_model.train()
                output = gate_model(embeds) 
       
                labels = torch.zeros(img.size(0)).cuda().long()
                for i in range(img.size(0)):
                    if img_metas[i]['filename'].find("fog_all") != -1:
                        labels[i] = 0
                    elif img_metas[i]['filename'].find("night_all") != -1:
                        labels[i] = 1
                    elif img_metas[i]['filename'].find("rain_all") != -1:
                        labels[i] = 2
                    elif img_metas[i]['filename'].find("snow_all") != -1:
                        labels[i] = 3
                
                labels = torch.cat([labels] * (dl + 1), dim=0)
            
                loss = criterion(output, labels)
                loss.backward()
                optimizer.step()

                # print statistics
                run_loss += loss.item()
                if batch_ndx % 10 == 0:  
                    print(f'[epoch {epoch + 1}, batch_nr {batch_ndx + 1:5d}] loss: {run_loss / 5:.8f}')
                    run_loss = 0.0
        
#         k = 0
#         for dataset, data_loader in zip(val_dataset, val_dataloader):
#                 outputs = single_gpu_test_gate(
#                                           gate_model, 
#                                           model_fog, 
#                                           model_night, 
#                                           model_rain,
#                                           model_snow,
#                                           data_loader,
#                                           dl,
#                                           None, None,False)
#                 rank, _ = get_dist_info()
#                 if rank == 0:
#                     if args.out:
#                         print(f'\nwriting results to {args.out}')
#                         mmcv.dump(outputs, args.out)
#                     kwargs = {} 
#                     if args.format_only:
#                         dataset.format_results(outputs, **kwargs)
#                     if args.eval:
#                         dataset.evaluate(outputs, args.eval, **kwargs)        
#                 k += 1
#         PATH = f"/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/gate_weights/test_gate_b6/dletetest_gatebatch6_10imgs_epoch{epoch}_dl{dl}_final.pth"
#         torch.save({  'epoch': epoch,
#                        'model_state_dict': gate_model.state_dict(),
#                         'optimizer_state_dict': optimizer.state_dict(),
#                        'loss': run_loss,
#                        }, PATH)

        print("Increase classifier's classes by one.")
        gate_model.increase_linear()
        print("Update classifier")
        optimizer = optim.SGD(gate_model.parameters(), lr=0.00001, momentum=0.9)

if __name__ == '__main__':
    main()
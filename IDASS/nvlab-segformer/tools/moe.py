import argparse
import copy
import mmcv
import os
import os.path as osp
import time
import torch
from mmcv.runner import init_dist, load_checkpoint
from mmcv.utils import Config, DictAction, get_git_hash

import numpy as np
import torch.optim as optim

from mmseg import __version__
from mmseg.apis import set_random_seed, train_segmentor
from mmseg.datasets import build_dataset
from mmseg.models import build_segmentor, build_train_model, build_moe
from mmseg.utils import collect_env, get_root_logger
import loralib as lora
from torch.utils.tensorboard import SummaryWriter
import torch.nn as nn
from mmseg.datasets import build_dataloader, build_dataset


# define gating network
class GatingNet(nn.Module):
    def __init__(self):
        super(GatingNet, self).__init__()

        self.linear = nn.Linear(261120, 4)
        self.softmax = nn.Softmax(dim=1)

    def forward(self, x):
        x = x.view(x.size(0), -1)
        x = self.linear(x)
        x = self.softmax(x)
        return x


def parse_args():
    parser = argparse.ArgumentParser(description='Train a segmentor')
    parser.add_argument('config', help='train config file path')
    parser.add_argument('--work-dir', help='the dir to save logs and models')
    parser.add_argument(
        '--load-from', help='the checkpoint file to load weights from')
    parser.add_argument(
        '--resume-from', help='the checkpoint file to resume from')
    parser.add_argument(
        '--no-validate',
        action='store_true',
        help='whether not to evaluate the checkpoint during training')
    group_gpus = parser.add_mutually_exclusive_group()
    group_gpus.add_argument(
        '--gpus',
        type=int,
        help='number of gpus to use '
             '(only applicable to non-distributed training)')
    group_gpus.add_argument(
        '--gpu-ids',
        type=int,
        nargs='+',
        help='ids of gpus to use '
             '(only applicable to non-distributed training)')
    parser.add_argument('--seed', type=int, default=None, help='random seed')
    parser.add_argument(
        '--deterministic',
        action='store_true',
        help='whether to set deterministic options for CUDNN backend.')
    parser.add_argument(
        '--options', nargs='+', action=DictAction, help='custom options')
    parser.add_argument(
        '--launcher',
        choices=['none', 'pytorch', 'slurm', 'mpi'],
        default='none',
        help='job launcher')
    parser.add_argument('--local_rank', type=int, default=0)
    args = parser.parse_args()
    if 'LOCAL_RANK' not in os.environ:
        os.environ['LOCAL_RANK'] = str(args.local_rank)

    return args


def main():
    print("Train gate network")
    args = parse_args()
    PATH = "/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/run18/gate_fn.pth"

    # set seed to 0
    set_random_seed(0, deterministic=True)

    print("Step 1. Load GatinNet model")
    gate_model = GatingNet()
    cfg = Config.fromfile(args.config)

    if args.gpu_ids is not None:
        cfg.gpu_ids = args.gpu_ids
    else:
        cfg.gpu_ids = range(1) if args.gpus is None else range(args.gpus)

    # init distributed env first, since logger depends on the dist info.
    if args.launcher == 'none':
        distributed = False
    else:
        distributed = True
        init_dist(args.launcher, **cfg.dist_params)

    print("Step 2. Load fog and rain models")

    das_config = "/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/local_configs/segformer/B5/segformer.b5.1024x1024.acdc_night_lora.160k.py"
    cfg_idas = Config.fromfile(das_config)
    
    # Fog
    model_fog = build_train_model(
        cfg_idas,
        train_cfg=cfg_idas.get('train_cfg'),
        test_cfg=cfg_idas.get('test_cfg'))

    checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/run11/latest.pth'
    load_checkpoint(model_fog, checkpoint, map_location='cpu')
    
    # Rain
    model_rain = build_train_model(
        cfg_idas,
        train_cfg=cfg_idas.get('train_cfg'),
        test_cfg=cfg_idas.get('test_cfg'))

    checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/run9/latest.pth'
    load_checkpoint(model_rain, checkpoint, map_location='cpu')
    # set models to device
    model_fog = model_fog.cuda()
    model_rain = model_rain.cuda()

    # set gate modls to device
    gate_model = gate_model.cuda()
    checkpoint = torch.load(PATH)
    gate_model.load_state_dict(checkpoint['model_state_dict'])

    model_fog.eval()
    model_rain.eval()
    gate_model.eval()

    val_dataset = build_dataset([cfg.data.val, cfg.data.val2], dict(test_mode=True))  # , cfg.data.val4, cfg.data.val3
    val_dataloader = build_dataloader(
        val_dataset,
        samples_per_gpu=1,
        workers_per_gpu=cfg.data.workers_per_gpu,
        dist=distributed,
        shuffle=True)

    print("Set validation, gate network in eval mode.")
    gate_model.eval()

    running_vloss = 0.
    total_correct = 0.
    total_instances = 0.
    criterion = nn.CrossEntropyLoss()
    with torch.no_grad():
        for i, vdata in enumerate(val_dataloader):
            img = vdata['img'][0].cuda()
            img_metas = vdata['img_metas'][0].data[0]
        
            embeds_fog = model_fog.extract_feat(img)[3]
            embeds_rain = model_rain.extract_feat(img)[3]

            embeds = torch.cat([embeds_fog, embeds_rain], dim=0)
            voutputs = gate_model(embeds)

            vlabels = torch.zeros(img.size(0)).cuda().long()
            for i in range(img.size(0)):
                print(img_metas[i]['filename'])
                if 'fog' in img_metas[i]['filename']:
                    vlabels[i] = 0
                elif 'night' in img_metas[i]['filename']:
                    vlabels[i] = 1
                elif 'rain' in img_metas[i]['filename']:
                    vlabels[i] = 2
                elif 'snow' in img_metas[i]['filename']:
                    vlabels[i] = 3

            vlabels = torch.cat([vlabels, vlabels], dim=0)
            vloss = criterion(voutputs, vlabels)
            running_vloss += vloss
            print("voutputs ", voutputs)
            classifications = torch.argmax(voutputs, dim=1)
            print(f"classifications {classifications}")
            print(f"vlabels {vlabels}")
            correct_predictions = sum(classifications == vlabels).item()
            total_correct += correct_predictions
            total_instances += vlabels.shape[0]

    print("Accuracy validation: ", 100 * total_correct / total_instances)
    print("Loss validation: ", running_vloss / total_instances)


if __name__ == '__main__':
    main()

import argparse
import os

import mmcv
import torch
from mmcv.parallel import MMDataParallel, MMDistributedDataParallel
from mmcv.runner import get_dist_info, init_dist, load_checkpoint
from mmcv.utils import DictAction

from mmseg.apis import multi_gpu_test, single_gpu_test
from mmseg.datasets import build_dataloader, build_dataset
from mmseg.models import build_segmentor
from IPython import embed
from mmseg.models import build_train_model
import loralib as lora
import json
import numpy as np
import torch
import albumentations as A



def parse_args():
    parser = argparse.ArgumentParser(
        description='mmseg test (and eval) a model')
    parser.add_argument('config', help='test config file path')
    parser.add_argument('checkpoint', help='checkpoint file')
    parser.add_argument(
        '--aug-test', action='store_true', help='Use Flip and Multi scale aug')
    parser.add_argument('--out', default='work_dirs/res.pkl', help='output result file in pickle format')
    parser.add_argument(
        '--format-only',
        action='store_true',
        help='Format the output results without perform evaluation. It is'
        'useful when you want to format the result to a specific format and '
        'submit it to the test server')
    parser.add_argument(
        '--eval',
        type=str,
        nargs='+',
        default='mIoU',
        help='evaluation metrics, which depends on the dataset, e.g., "mIoU"'
        ' for generic datasets, and "cityscapes" for Cityscapes')
    parser.add_argument('--show', action='store_true', help='show results')
    parser.add_argument(
        '--show-dir', help='directory where painted images will be saved')
    parser.add_argument(
        '--gpu-collect',
        action='store_true',
        help='whether to use gpu to collect results.')
    parser.add_argument(
        '--tmpdir',
        help='tmp directory used for collecting results from multiple '
        'workers, available when gpu_collect is not specified')
    parser.add_argument(
        '--options', nargs='+', action=DictAction, help='custom options')
    parser.add_argument(
        '--eval-options',
        nargs='+',
        action=DictAction,
        help='custom options for evaluation')
    parser.add_argument(
        '--launcher',
        choices=['none', 'pytorch', 'slurm', 'mpi'],
        default='none',
        help='job launcher')
    parser.add_argument('--local_rank', type=int, default=0)
    args = parser.parse_args()
    if 'LOCAL_RANK' not in os.environ:
        os.environ['LOCAL_RANK'] = str(args.local_rank)
    return args

def compute_adjustment(gt_seg_maps):
    """compute the base probabilities"""
    tro = 1.0
    class_frequencies = np.zeros(256)
    
    for i in range(len(gt_seg_maps)):
        target = torch.from_numpy(gt_seg_maps[i]).cuda()
        class_frequencies += calculate_class_frequencies(target)

    class_frequencies /= (len(gt_seg_maps) - 1)
    adjustments = class_frequencies
#     adjustments = np.log(class_frequencies ** tro + 1e-12)
    adjustments = torch.from_numpy(adjustments).to("cuda")

    return adjustments

def calculate_class_frequencies(segmentation_labels):
    class_counts = np.bincount(segmentation_labels.cpu().flatten())
    class_frequencies = class_counts / segmentation_labels.cpu().flatten().size()
    return class_frequencies


def main():
    args = parse_args()
    assert args.out or args.eval or args.format_only or args.show \
        or args.show_dir, \
        ('Please specify at least one operation (save/eval/format/show the '
         'results / save the results) with the argument "--out", "--eval"'
         ', "--format-only", "--show" or "--show-dir"')

    if 'None' in args.eval:
        args.eval = None
    if args.eval and args.format_only:

        raise ValueError('--eval and --format_only cannot be both specified')

    if args.out is not None and not args.out.endswith(('.pkl', '.pickle')):
        raise ValueError('The output file must be a pkl file.')

    cfg = mmcv.Config.fromfile(args.config)
    if args.options is not None:
        cfg.merge_from_dict(args.options)
    # set cudnn_benchmark
    if cfg.get('cudnn_benchmark', False):
        torch.backends.cudnn.benchmark = True
        
    if args.aug_test:
        if cfg.data.test.type == 'CityscapesDataset':
            # hard code index
            cfg.data.test.pipeline[1].img_ratios = [
                0.5, 0.75, 1.0, 1.25, 1.5, 1.75, 2.0
            ]
            cfg.data.test.pipeline[1].flip = True
        elif cfg.data.test.type == 'ADE20KDataset':
            # hard code index
            cfg.data.test.pipeline[1].img_ratios = [
                0.75, 0.875, 1.0, 1.125, 1.25
            ]
            cfg.data.test.pipeline[1].flip = True
        else:
            # hard code index
            cfg.data.test.pipeline[1].img_ratios = [
                0.5, 0.75, 1.0, 1.25, 1.5, 1.75
            ]
            cfg.data.test.pipeline[1].flip = True

    cfg.model.pretrained = None
    cfg.data.test.test_mode = True

    # init distributed env first, since logger depends on the dist info.
    if args.launcher == 'none':
        distributed = False
    else:
        distributed = True
        init_dist(args.launcher, **cfg.dist_params)

    if cfg.data.test is not None:
        datasets = [build_dataset(cfg.data.test)]

        data_loaders = [build_dataloader(
            dataset,
            samples_per_gpu=1,
            workers_per_gpu=cfg.data.workers_per_gpu,
            dist=distributed,
            shuffle=False) for dataset in datasets]
        
    # build the model and load checkpoint
    cfg.model.train_cfg = None
    
    model = build_segmentor(
        cfg.model,
        train_cfg=cfg.get('train_cfg'),
        test_cfg=cfg.get('test_cfg'))
    
#     model = build_train_model(
#         cfg,
#         train_cfg=cfg.get('train_cfg'),
#         test_cfg=cfg.get('test_cfg'))
    

    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/run44/iter_20000.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/city_snowy/iter_20000.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/city_snowy/iter_20000.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/finetune_city/fogy_city/iter_32000.pth'

    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/finetune_city/fogy_city/iter_40000.pth'
    
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/city_200_lr/iter_20000.pth'
    
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/run9/lora_final_night_test.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/run9/lora_final_night_test.pth'
    
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/nvlab_segformer/nvlab-segformer/experiments/finetuning/fog1/latest.pth'
    
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/run13/latest.pth'
    
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/nvlab_segformer/nvlab-segformer/baseline/finetuning/snow/iter_80000.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/nvlab_segformer/nvlab-segformer/experiments/finetuning/night/iter_80000.pth'
    
    source_checkpoint = '/BS/IDASS/nobackup/datasets/code/nvlab_segformer/nvlab-segformer/segformer.b5.1024x1024.city.160k.pth'
    
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/nvlab_segformer/nvlab-segformer/baseline/finetuning/snow/iter_80000.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/run13/latest.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/ablations/snow1/latest.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/incremental/eain/latest.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/incremental/snow/latest.pth'

    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/nvlab_segformer/nvlab-segformer/baseline/finetuning/snow/iter_80000.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/city_snowy/iter_20000.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/city_fogy/iter_20000.pth'
    
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/finetune_city/snowy_city/iter_32000.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/finetune_city/fogy_city/iter_40000.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/run38/latest.pth'
    
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/finetune_city/city_200f/iter_64000.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/finetune_city/city_100f/iter_64000.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/finetune_city/city_75f/iter_64000.pth'
    
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/city_75_lr/iter_20000.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/city_100_lr/iter_20000.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/city_200_lr/iter_20000.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/run_source/night8/latest.pth'
    
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/nvlab_segformer/nvlab-segformer/experiments/finetuning/fog1/latest.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/nvlab_segformer/nvlab-segformer/experiments/finetuning/night/iter_80000.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/nvlab_segformer/nvlab-segformer/experiments/finetuning/rain/iter_80000.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/nvlab_segformer/nvlab-segformer/baseline/finetuning/snow/iter_80000.pth'
    
    # loras ACDC
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/run9/latest.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/run12/latest.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/run13/latest.pth'
    
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/run_source/jointcityc/iter_72000.pth'  
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/finetune_city/fogy_city/iter_40000.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/run_source/jointcityrain/iter_72000.pth'  
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/run_source/jointacdclora/latest.pth'  
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/ablations/tta1/latest.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/run_source/rain_fda/latest.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/run_source/snow_fda/latest.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/run_source/night8/latest.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/run11/latest.pth'
    args.checkpoint = '/BS/IDASS/nobackup/datasets/code/segformer_g22/nvlab-segformer/exp_ckp/ablations/adapter/iter_800.pth'

    checkpoint = load_checkpoint(model, source_checkpoint, map_location='cpu')
#     model = model.model_teacher

    model.CLASSES = checkpoint['meta']['CLASSES']
    model.PALETTE = checkpoint['meta']['PALETTE']
    
    efficient_test = True #False
    if args.eval_options is not None:
        efficient_test = args.eval_options.get('efficient_test', False)

    if not distributed:
        model = MMDataParallel(model, device_ids=[0])#.model_teacher
        for dataset, data_loader in zip(datasets, data_loaders):
            outputs = single_gpu_test(model, data_loader, args.show, args.show_dir,
                                    efficient_test)
            rank, _ = get_dist_info()
            if rank == 0:
                if args.out:
                    print(f'\nwriting results to {args.out}')
                    mmcv.dump(outputs, args.out)
                kwargs = {} if args.eval_options is None else args.eval_options
                if args.format_only:
                    dataset.format_results(outputs, **kwargs)
                if args.eval:
                    dataset.evaluate(outputs, args.eval, **kwargs)
    else:
        model = MMDistributedDataParallel(
            model.cuda(),#.model.model_teacher
            device_ids=[torch.cuda.current_device()],
            broadcast_buffers=False)
        for dataset, data_loader in zip(datasets, data_loaders):
            outputs = multi_gpu_test(model, data_loader, args.tmpdir,
                                 args.gpu_collect, efficient_test)

            rank, _ = get_dist_info()
            if rank == 0:
                if args.out:
                    print(f'\nwriting results to {args.out}')
                    mmcv.dump(outputs, args.out)
                kwargs = {} if args.eval_options is None else args.eval_options
                if args.format_only:
                    dataset.format_results(outputs, **kwargs)
                if args.eval:
                    dataset.evaluate(outputs, args.eval, **kwargs)


if __name__ == '__main__':
    main()

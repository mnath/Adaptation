# Baseline IDASS
_base_ = [
    '../../_base_/datasets/acdc_pipeline_lora.py',
    '../../_base_/default_runtime.py',
    '../../_base_/schedules/schedule_20k.py'
]

model = dict(
    type='MixtExperts',
    head=dict(
        type='GatingNet',
        loss=dict(type='CrossEntropyLoss', use_sigmoid=False, loss_weight=1.0, reduction='none')),
)

# data
data = dict(samples_per_gpu=2, workers_per_gpu=4)
evaluation = dict(interval=400000, metric='mIoU')

# optimizer
optimizer = dict(type='SGD', lr=0.000001, momentum=0.9, weight_decay=0.0005)

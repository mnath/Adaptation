# dataset settings
dataset_type = 'CityscapesDataset'
data_root = '/BS/IDASS/nobackup/datasets/'

img_norm_cfg = dict(
    mean=[123.675, 116.28, 103.53], std=[58.395, 57.12, 57.375], to_rgb=True)

crop_size = (1024, 512) 

train_pipeline = [
    dict(type='LoadImageFromFile'),
    dict(type='LoadAnnotations'),
    dict(type='Resize', img_scale=(1024, 512), ratio_range=(0.5, 2.0)),
    dict(type='RandomCrop', crop_size=crop_size, cat_max_ratio=0.75),
    dict(type='RandomFlip', prob=0.0),
    dict(type='Normalize', **img_norm_cfg),
    dict(type='Pad', size=crop_size, pad_val=0, seg_pad_val=255),
    dict(type='DefaultFormatBundle'),
    dict(type='Collect', keys=['img', 'gt_semantic_seg']),
]

test_pipeline = [
    dict(type='LoadImageFromFile'),
    dict(
        type='MultiScaleFlipAug',
        img_scale=(2048, 1024),
        # img_ratios=[0.5, 0.75, 1.0, 1.25, 1.5, 1.75],
        flip=False,
        transforms=[
            dict(type='Resize', keep_ratio=True),
            dict(type='RandomFlip'),
            dict(type='Normalize', **img_norm_cfg),
            dict(type='ImageToTensor', keys=['img']),
            dict(type='Collect', keys=['img']),
        ])
]

data = dict(
    samples_per_gpu=2,
    workers_per_gpu=4,
    trainfog=dict(
        type=dataset_type,
        img_dir='/BS/IDASS/nobackup/datasets/cityscapes-c/manual/leftImg8bit_trainvaltest_fog-5/leftImg8bit/train/',
        ann_dir='/BS/IDASS/nobackup/datasets/cityscapes/gtFine/train/',
        pipeline=train_pipeline),
    trainsnow=dict(
        type=dataset_type,
        img_dir='/BS/IDASS/nobackup/datasets/cityscapes-c/manual/leftImg8bit_trainvaltest_snow-5/leftImg8bit/train/',
        ann_dir='/BS/IDASS/nobackup/datasets/cityscapes/gtFine/train/',
        pipeline=train_pipeline),
    train75=dict(
        type=dataset_type,
        img_dir='/BS/IDASS/nobackup/datasets/weather_cityscapes/weather_cityscapes/leftImg8bit/train/rain/75mm/rainy_image/',
        ann_dir='/BS/IDASS/nobackup/datasets/cityscapes/gtFine/train/',
        pipeline=train_pipeline),
    train100=dict(
        type=dataset_type,
        img_dir='/BS/IDASS/nobackup/datasets/weather_cityscapes/weather_cityscapes/leftImg8bit/train/rain/100mm/rainy_image/',
        ann_dir='/BS/IDASS/nobackup/datasets/cityscapes/gtFine/train/',
        pipeline=train_pipeline),
    train200=dict(
        type=dataset_type,
        img_dir='/BS/IDASS/nobackup/datasets/weather_cityscapes/weather_cityscapes/leftImg8bit/train/rain/200mm/rainy_image/',
        ann_dir='/BS/IDASS/nobackup/datasets/cityscapes/gtFine/train/',
        pipeline=train_pipeline),
    valfog=dict(
        type=dataset_type,
        img_dir='/BS/IDASS/nobackup/datasets/cityscapes-c/manual/leftImg8bit_trainvaltest_fog-5/leftImg8bit/val/', #, 
        ann_dir='/BS/IDASS/nobackup/datasets/cityscapes/gtFine/val/',
        pipeline=test_pipeline),
    valsnow=dict(
        type=dataset_type,
        img_dir='/BS/IDASS/nobackup/datasets/cityscapes-c/manual/leftImg8bit_trainvaltest_snow-5/leftImg8bit/val/',  #, 
        ann_dir='/BS/IDASS/nobackup/datasets/cityscapes/gtFine/val/',
        pipeline=test_pipeline),
    val200=dict(
        type=dataset_type,
        img_dir='/BS/IDASS/nobackup/datasets/weather_cityscapes/weather_cityscapes/leftImg8bit/val/rain/200mm/rainy_image/', #, 
        ann_dir='/BS/IDASS/nobackup/datasets/cityscapes/gtFine/val/',
        pipeline=test_pipeline),
    val75=dict(
        type=dataset_type,
        img_dir='/BS/IDASS/nobackup/datasets/weather_cityscapes/weather_cityscapes/leftImg8bit/val/rain/75mm/rainy_image/', #, 
        ann_dir='/BS/IDASS/nobackup/datasets/cityscapes/gtFine/val/',
        pipeline=test_pipeline),
    val100=dict(
        type=dataset_type,
        img_dir='/BS/IDASS/nobackup/datasets/weather_cityscapes/weather_cityscapes/leftImg8bit/val/rain/100mm/rainy_image/', #, 
        ann_dir='/BS/IDASS/nobackup/datasets/cityscapes/gtFine/val/',
        pipeline=test_pipeline),
    test=dict(
        type=dataset_type,
        img_dir='/BS/IDASS/nobackup/datasets/cityscapes/leftImg8bit/val',
        ann_dir='/BS/IDASS/nobackup/datasets/cityscapes/gtFine/val',
        pipeline=test_pipeline))


# dataset settings
dataset_type = 'ACDCDataset'
data_root = '/BS/IDASS/nobackup/datasets/ACDC/'

img_norm_cfg = dict(
    mean=[123.675, 116.28, 103.53], std=[58.395, 57.12, 57.375], to_rgb=True)
crop_size = (960, 540)
train_pipeline = [
    dict(type='LoadImageFromFile'),
    dict(type='LoadAnnotations'),
    dict(type='Resize', img_scale=(1920, 1080), ratio_range=(0.5, 2.0)),
    dict(type='RandomCrop', crop_size=crop_size, cat_max_ratio=0.75),
    dict(type='RandomFlip', prob=0.5),
    dict(type='PhotoMetricDistortion'),
    dict(type='Normalize', **img_norm_cfg),
    dict(type='Pad', size=crop_size, pad_val=0, seg_pad_val=255),
    dict(type='DefaultFormatBundle'),
    dict(type='Collect', keys=['img', 'gt_semantic_seg']),
]
test_pipeline = [
    dict(type='LoadImageFromFile'),
    dict(
        type='MultiScaleFlipAug',
        img_scale=(1920//2, 1080//2),
        # img_ratios=[0.5, 0.75, 1.0, 1.25, 1.5, 1.75],
        flip=False,
        transforms=[
            dict(type='Resize', keep_ratio=True),
            dict(type='RandomFlip'),
            dict(type='Normalize', **img_norm_cfg),
            dict(type='ImageToTensor', keys=['img']),
            dict(type='Collect', keys=['img']),
        ])
]

data = dict(
    samples_per_gpu=1,
    workers_per_gpu=4,
    trainfog=dict(
            type=dataset_type,
            data_root=data_root,
            img_dir='rgb_anon/fog_all/train', 
            ann_dir='gt/fog_all/train',
            pipeline=train_pipeline),
    trainnight=dict(
        type=dataset_type,
        data_root=data_root,
        img_dir='rgb_anon/night_all/train',
        ann_dir='gt/night_all/train',
        pipeline=train_pipeline),
    trainrain=dict(
        type=dataset_type,
        data_root=data_root,
        img_dir='rgb_anon/rain_all/train',
        ann_dir='gt/rain_all/train',
        pipeline=train_pipeline),
    trainsnow=dict(
        type=dataset_type,
        data_root=data_root,
        img_dir='rgb_anon/snow_all/train',
        ann_dir='gt/snow_all/train',
        pipeline=train_pipeline),
    val1=dict(
            type=dataset_type,
            data_root=data_root,
            img_dir='rgb_anon/fog_all/val',
            ann_dir='gt/fog_all/val',
            pipeline=test_pipeline),
    val2=dict(
            type=dataset_type,
            data_root=data_root,
            img_dir='rgb_anon/night_all/val',
            ann_dir='gt/night_all/val',
            pipeline=test_pipeline),
    val3=dict(
            type=dataset_type,
            data_root=data_root,
            img_dir='rgb_anon/rain_all/val',
            ann_dir='gt/rain_all/val',
            pipeline=test_pipeline),
    val4=dict(
            type=dataset_type,
            data_root=data_root,
            img_dir='rgb_anon/snow_all/val',
            ann_dir='gt/snow_all/val',
            pipeline=test_pipeline),
    test=dict(
        type=dataset_type,
        data_root=data_root,
        img_dir='rgb_anon/fog_all/train',
        ann_dir='gt/fog_all/train',
        pipeline=test_pipeline),
)



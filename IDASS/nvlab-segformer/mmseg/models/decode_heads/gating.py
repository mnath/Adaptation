import torch.nn as nn
from mmcls.models.builder import HEADS
from mmcls.models.heads import ClsHead

from ..builder import build_loss


@HEADS.register_module()
class GatingNet(ClsHead):
    def __init__(self, loss):
        super(GatingNet, self).__init__()
        self.linear = nn.Linear(261120, 2)
        self.loss = build_loss(loss)
        self.softmax = nn.Softmax(dim=1)
    def forward(self, x):
        x = x.view(x.size(0), -1)
        x = self.linear(x)
        x = self.softmax(x)
        return x

    def init_weights(self):
        normal_init(self.fc, mean=0, std=0.01, bias=0)

    def forward_train(self, inputs, labels,  **kwargs):
        losses = self.loss(inputs, labels)
        return losses

import torch
from mmcv.parallel import MMDistributedDataParallel

from mmseg.models import BaseSegmentor
from mmcls.models import BaseClassifier
from ..builder import MOE
from mmseg.core import add_prefix
import matplotlib.pyplot as plt
import torch.nn.functional as F
from .. import builder

from mmseg.models.losses import CrossEntropyLoss
from mmcls.models.builder import MODELS
from mmcls.models.builder import build_head
def get_module(module):
    """Get `nn.ModuleDict` to fit the `MMDistributedDataParallel` interface.

    Args:
        module (MMDistributedDataParallel | nn.ModuleDict): The input
            module that needs processing.

    Returns:
        nn.ModuleDict: The ModuleDict of multiple networks.
    """
    if isinstance(module, MMDistributedDataParallel):
        return module.module

    return module


@MODELS.register_module()
class MixtExperts(BaseClassifier):

    def __init__(self, head, **cfg):
        super(BaseClassifier, self).__init__()
        self.model = build_head(head)
        self.fog_model = None
        self.night_model = None

    def setup_before_train(self):
        print("Freeze teacher model")
        for n, p in self.fog_model.named_parameters():
            p.requires_grad = False
        for n, p in self.rain_model.named_parameters():
            p.requires_grad = False
        self.fog_model.eval()
        self.rain_model.eval()

    def extract_feat(self, input):
        # Implement the logic for extracting features from the input
        pass

    def simple_test(self, input):
        # Implement the logic for performing a simple test on the input
        pass
    def train_step(self, data_batch, optimizer, **kwargs):

        losses = self(**data_batch)
        outputs = dict(
            loss=losses['loss'],
            num_samples=len(data_batch['img'].data))

        return outputs

    def forward_train(self,  img, img_metas):
        """Forward function for training.

        Args:
            img (Tensor): Input images.
            img_metas (list[dict]): List of image info dict where each dict
                has: 'img_shape', 'scale_factor', 'flip', and may also contain
                'filename', 'ori_shape', 'pad_shape', and 'img_norm_cfg'.
                For details on the values of these keys see
                `mmseg/datasets/pipelines/formatting.py:Collect`.
            gt_semantic_seg (Tensor): Semantic segmentation masks
                used if the architecture supports semantic segmentation task.

        Returns:
            dict[str, Tensor]: a dictionary of loss components
        """
        self.fog_model.eval()
        self.rain_model.eval()

        losses = dict()

        return losses

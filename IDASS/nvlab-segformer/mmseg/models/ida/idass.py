# Obtained from: https://github.com/lhoyer/DAFormer
# Modifications:
# - Delete tensors after usage to free GPU memory
# - Add HRDA debug visualizations
# - Support ImageNet feature distance for LR and HR predictions of HRDA
# - Add masked image consistency
# - Update debug image system
# ---------------------------------------------------------------
# Copyright (c) 2021-2022 ETH Zurich, Lukas Hoyer. All rights reserved.
# Licensed under the Apache License, Version 2.0
# ---------------------------------------------------------------

# The ema model update and the domain-mixing are based on:
# https://github.com/vikolss/DACS
# Copyright (c) 2020 vikolss. Licensed under the MIT License.
# A copy of the license is available at resources/license_dacs

from abc import ABC
import torch.nn.functional as F

import math
import random
import tempfile
from copy import deepcopy
import torchvision.transforms as T
from matplotlib import pyplot as plt

import numpy as np
import torch
from torchvision import transforms
from PIL import Image

from mmseg.models import build_segmentor
from mmseg.models.ida.cl_decorator import CLDecorator, get_module
import loralib as lora
from mmseg.models.segmentors import EncoderDecoder
from mmseg.registry import MODELS
from torch.autograd import Variable
from mmcv.image import tensor2imgs
import mmcv
import os.path as osp
from ..builder import IDA
from mmcv.parallel import MMDistributedDataParallel
import torch.nn as nn
import albumentations as A
import matplotlib.pyplot as plt
import numpy as np
from albumentations.pytorch import ToTensorV2
import torch.nn.functional as F



def extract_ampl_phase(fft_im):
    # fft_im: size should be bx3xhxwx2
    fft_amp = fft_im[:, :, :, :, 0] ** 2 + fft_im[:, :, :, :, 1] ** 2
    fft_amp = torch.sqrt(fft_amp)
    fft_pha = torch.atan2(fft_im[:, :, :, :, 1], fft_im[:, :, :, :, 0])
    return fft_amp, fft_pha


def low_freq_mutate(amp_src, amp_trg, L=0.1):
    _, _, h, w = amp_src.size()
    b = (np.floor(np.amin((h, w)) * L)).astype(int)  # get b
    amp_src[:, :, 0:b, 0:b] = amp_trg[:, :, 0:b, 0:b]  # top left
    amp_src[:, :, 0:b, w - b:w] = amp_trg[:, :, 0:b, w - b:w]  # top right
    amp_src[:, :, h - b:h, 0:b] = amp_trg[:, :, h - b:h, 0:b]  # bottom left
    amp_src[:, :, h - b:h, w - b:w] = amp_trg[:, :, h - b:h, w - b:w]  # bottom right
    return amp_src


def low_freq_mutate_np(amp_src, amp_trg, L=0.1):
    a_src = np.fft.fftshift(amp_src, axes=(-2, -1))
    a_trg = np.fft.fftshift(amp_trg, axes=(-2, -1))

    _, h, w = a_src.shape
    b = (np.floor(np.amin((h, w)) * L)).astype(int)
    c_h = np.floor(h / 2.0).astype(int)
    c_w = np.floor(w / 2.0).astype(int)

    h1 = c_h - b
    h2 = c_h + b + 1
    w1 = c_w - b
    w2 = c_w + b + 1

    a_src[:, h1:h2, w1:w2] = a_trg[:, h1:h2, w1:w2]
    a_src = np.fft.ifftshift(a_src, axes=(-2, -1))
    return a_src


def FDA_source_to_target_np(src_img, trg_img, L=0.1):
    # exchange magnitude
    # input: src_img, trg_img
    src_img = src_img.detach().cpu().numpy()
    trg_img = trg_img.detach().cpu().numpy()
    src_img_np = src_img  # .cpu().numpy()
    trg_img_np = trg_img  # .cpu().numpy()

    # get fft of both source and target
    fft_src_np = np.fft.fft2(src_img_np, axes=(-2, -1))

    fft_trg_np = np.fft.fft2(trg_img_np, axes=(-2, -1))

    # extract amplitude and phase of both ffts
    amp_src, pha_src = np.abs(fft_src_np), np.angle(fft_src_np)
    amp_trg, pha_trg = np.abs(fft_trg_np), np.angle(fft_trg_np)

    # mutate the amplitude part of source with target
    amp_src_ = low_freq_mutate_np(amp_src, amp_trg, L=L)

    # mutated fft of source
    fft_src_ = amp_src_ * np.exp(1j * pha_src)

    # get the mutated image
    src_in_trg = np.fft.ifft2(fft_src_, axes=(-2, -1))
    src_in_trg = np.real(src_in_trg)

    return src_in_trg


def np2tmp(array, temp_file_name=None):
    """Save ndarray to local numpy file.

    Args:
        array (ndarray): Ndarray to save.
        temp_file_name (str): Numpy file name. If 'temp_file_name=None', this
            function will generate a file name with tempfile.NamedTemporaryFile
            to save ndarray. Default: None.

    Returns:
        str: The numpy file name.
    """

    if temp_file_name is None:
        temp_file_name = tempfile.NamedTemporaryFile(
            suffix='.npy', delete=False).name
    np.save(temp_file_name, array)
    return temp_file_name


def _params_equal(ema_model, model):
    for ema_param, param in zip(ema_model.named_parameters(), model.named_parameters()):
        if not torch.equal(ema_param[1].data, param[1].data):
            return False
    return True


def calc_grad_magnitude(grads, norm_type=2.0):
    norm_type = float(norm_type)
    if norm_type == math.inf:
        norm = max(p.abs().max() for p in grads)
    else:
        norm = torch.norm(
            torch.stack([torch.norm(p, norm_type) for p in grads]), norm_type)

    return norm


def get_mean_std(img_metas, dev):
    mean = [
        torch.as_tensor(img_metas[i]['img_norm_cfg']['mean'], device=dev)
        for i in range(len(img_metas))
    ]
    mean = torch.stack(mean).view(-1, 3, 1, 1)
    std = [
        torch.as_tensor(img_metas[i]['img_norm_cfg']['std'], device=dev)
        for i in range(len(img_metas))
    ]
    std = torch.stack(std).view(-1, 3, 1, 1)
    return mean, std


def denorm(img, mean, std):
    return img.mul(std).add(mean).div(255.0)  #for 0-1 float32 range


def renorm(img, mean, std):
    return img.mul(255.0).sub(mean).div(std) #


@MODELS.register_module()
class IDASS(EncoderDecoder):
    
    def __init__(self, **cfg):
        super(IDASS, self).__init__(**cfg)
        self.local_iter = 0
        self.alpha_teacher = 1.0
        cfg_source = deepcopy(cfg['model'])
        self.model_teacher = build_segmentor(cfg_source)
        self.model_source_state_dict = None        
        
    def get_teacher_model(self):
        return get_module(self.model_teacher)    
        
    
    def simple_test(self, img, img_meta, rescale=True):
        """Simple test with single image."""
        self.get_teacher_model().eval()
        return self.get_teacher_model().simple_test(img, img_meta, rescale)

    
    def _update_teacher(self, local_iter):
        alpha_teacher = min(1 - 1 / (local_iter + 1), self.alpha_teacher)
        for ema_param, param in zip(self.get_teacher_model().parameters(), self.get_model().parameters()):
            if param.requires_grad == True:
                ema_param.data[:] = alpha_teacher * ema_param[:].data[:] + (1 - alpha_teacher) * param[:].data[:]


    def setup_before_train(self):
        print("Init teacher and source weights")
        self.model_source_state_dict = deepcopy(self.get_model().state_dict())
        self.get_teacher_model().load_state_dict(self.model_source_state_dict)

        print("Check teacher weights")
        assert _params_equal(self.get_teacher_model(), self.get_model()), "Teacher Student init not equal!"

        print("Freeze teacher model")
        for n, p in self.get_teacher_model().named_parameters():
            p.requires_grad = False
            
        self.get_teacher_model().eval()

    def forward_train(self,
                      img,
                      img_metas,
                      gt_semantic_seg):
        """Forward function for training.

        Args:
            img (Tensor): Input images.
            img_metas (list[dict]): List of image info dict where each dict
                has: 'img_shape', 'scale_factor', 'flip', and may also contain
                'filename', 'ori_shape', 'pad_shape', and 'img_norm_cfg'.
                For details on the values of these keys see
                `mmseg/datasets/pipelines/formatting.py:Collect`.
            gt_semantic_seg (Tensor): Semantic segmentation masks
                used if the architecture supports semantic segmentation task.

        Returns:
            dict[str, Tensor]: a dictionary of loss components
        """
        losses = dict()

        # check if input image is not augmented
        assert img_metas[0]['flip'] == False, "Augmented image!"

        # set Teacher model in eval mode
        self.get_teacher_model().eval()

        # update teacher after first iteration
        if self.local_iter > 0:
            self._update_teacher(self.local_iter)

        # get weakly augmented image
        transform = A.Compose([
            A.HorizontalFlip(p=1.0),
            ToTensorV2()
        ])

        transformed_weak_images = []
        transformed_weak_metas = []

        for i in range(img.shape[0]):
            image_data = transform(image=img[i].detach().cpu().numpy().transpose(1, 2, 0))
            image_weak_flipped = image_data["image"].unsqueeze(0).cuda()
            transformed_weak_images.append(image_weak_flipped)
            # create meta
            image_meta_weak_flipped = deepcopy(img_metas[i])
            image_meta_weak_flipped['flip'] = True
            image_meta_weak_flipped['flip_direction'] = 'horizontal'
            transformed_weak_metas.append(image_meta_weak_flipped)

        weak_images_batch = torch.cat(transformed_weak_images, dim=0)

        with torch.no_grad():
            # whole inference
            logits = self.get_teacher_model().encode_decode(weak_images_batch, transformed_weak_metas)
            ema_softmax = torch.softmax(logits.detach(), dim=1)

            # flip back
            flip = transformed_weak_metas[0]['flip']
            if flip:
                flip_direction = transformed_weak_metas[0]['flip_direction']
                assert flip_direction in ['horizontal', 'vertical']
                if flip_direction == 'horizontal':
                    ema_softmax = ema_softmax.flip(dims=(3,))
                elif flip_direction == 'vertical':
                    ema_softmax = ema_softmax.flip(dims=(2,))

            # compute pseudo weight
            teacher_prob, teacher_pred = torch.max(ema_softmax, dim=1)

            ps_large_p = teacher_prob.ge(0.962).long() == 1
            ps_size = np.size(np.array(teacher_pred.cpu()))
            pseudo_weight = torch.sum(ps_large_p).item() / ps_size

            # select most confident predictions
            mask = teacher_prob >= 0.962
            teacher_pred = torch.where(mask, teacher_pred, 255)
            result = teacher_pred.unsqueeze(1)
            
        # get strong augmentation
        strong_transform = A.Compose([
            A.Rotate(limit=30, p=1.0),
            ToTensorV2()
        ])

        transformed_strong_images = []
        results_strong = []

        # To apply Color transf we need to norm and denorm images
        img_to_transform = deepcopy(img)
        # denormalize
        means, stds = get_mean_std(img_metas, "cuda")
        img_to_transform = denorm(img_to_transform, means, stds)

        for i in range(img.shape[0]):
            image_data = strong_transform(image=img_to_transform[i].detach().cpu().numpy().transpose(1, 2, 0),
                                          mask=result[i].detach().cpu().numpy().transpose(1, 2, 0))

            image_strong_aug = image_data["image"].unsqueeze(0).cuda()
            transformed_strong_images.append(image_strong_aug)

            masked_result = image_data["mask"].permute(2, 0, 1).unsqueeze(0).type(torch.long).cuda()
            results_strong.append(masked_result)

        strong_images_batch = torch.cat(transformed_strong_images, dim=0)
        pseudo_labels = torch.cat(results_strong, dim=0)
        # renormalize
        strong_images_batch = renorm(strong_images_batch, means, stds)

        self.get_model().train()
        student_losses = self.get_model().forward_train(img=strong_images_batch,
                                                        img_metas=img_metas,
                                                        gt_semantic_seg=pseudo_labels)
        
        student_losses['decode.loss_seg'] *= pseudo_weight
        losses.update(student_losses)

        self.local_iter += 1

        return losses


# MoUDE: Source-Free Incremental Domain Learning for Semantic Segmentation via Mixtures of Unsupervised Domain Experts

This repository contains the implementation of the MoUDE framework for source-free incremental domain learning in semantic segmentation.

---

## Project Structure

- **`IDASS/nvlab-segformer/`**: Contains the Segformer backbone implementation used in the MoUDE framework. This includes model definitions and associated utilities.

- **`libraries/`**: Contains utility libraries and additional backbone implementations, including support for LoRA-based methods.

- **`segformer_test/`**: Includes test scripts and evaluation metrics for validating the performance of the Segformer model.

- **`src_files/`**: Main source files for the project, including configuration files and Python scripts to run the MoUDE framework. Key components include:
  - Configuration setup for training and evaluation.
  - Data preprocessing and augmentation logic.
  - Model initialization and domain expert mixing strategies.

- **`tools/`**: Contains utility scripts for preprocessing, logging, and model checkpoint management.

- **`mmsegmentation/`**: A submodule or reference implementation for integrating segmentation models with the MoUDE framework.

- **`.gitignore`**: Standard file to exclude unnecessary files from being tracked in version control.

- **`idass.py`**: Main script to run and experiment with the MoUDE framework. It integrates the modules and ensures smooth execution of the workflow.

import copy
import math
import warnings
from typing import Sequence

import torch
import torch.nn as nn
import torch.nn.functional as F
from mmengine.model import BaseModule
from mmengine.registry import MODELS
from mmengine.utils import deprecated_api_warning

from mmcv.cnn.bricks.drop import build_dropout
import loratorch as lora

import torch
import torch.nn as nn
import torch.nn.functional as F

class MultiheadAttentionx(nn.Module):
    def __init__(self, dmodel, num_heads):
        super().__init__()
        self.num_heads = num_heads
        self.dmodel = dmodel
        self.head_dim = dmodel // num_heads

        self.proj_q = lora.Linear(self.dmodel, self.num_heads * dmodel,r=4)
        self.proj_k = nn.Linear(self.dmodel, self.num_heads * dmodel)
        self.proj_v = lora.Linear(self.dmodel, self.num_heads * dmodel,r=4)

        nn.init.constant_(self.proj_q.bias, 0.)
        nn.init.constant_(self.proj_k.bias, 0.)
        nn.init.constant_(self.proj_v.bias, 0.)

        self.output_proj = nn.Linear(self.num_heads * dmodel, dmodel, bias=False)

        self.register_buffer('scale', torch.tensor(self.head_dim ** -0.5))

    def forward(self, q, k, v):
        # seqlen,batch, dmodel
        q = q.transpose(0, 1)
        k = k.transpose(0, 1)
        v = v.transpose(0, 1)

        # batch, seqlen, dmodel
        q = self.proj_q(q).view(q.size(0), q.size(1), self.num_heads, -1).transpose(1, 2)
        k = self.proj_k(k).view(k.size(0), k.size(1), self.num_heads, -1).transpose(1, 2)
        v = self.proj_v(v).view(v.size(0), v.size(1), self.num_heads, -1).transpose(1, 2)
        # batch, head, seqlen, dk|dv

        logits = torch.matmul(q, k.transpose(-2, -1)) * self.scale
        # batch, head, qlen, kvlen
        weighted_v = F.softmax(logits, dim=-1) @ v
        # batch, head, qlen, dv
        heads = torch.cat(weighted_v.unbind(1), dim=-1)
        # batch, qlen, dv * head
        output = self.output_proj(heads)
        # batch, qlen, dmodel
        output = output.transpose(0, 1)
        return output


@MODELS.register_module()
class Lora_MultiheadAttention(BaseModule):
    """A wrapper for ``torch.nn.MultiheadAttention``.

    This module implements MultiheadAttention with identity connection,
    and positional encoding  is also passed as input.

    Args:
        embed_dims (int): The embedding dimension.
        num_heads (int): Parallel attention heads.
        attn_drop (float): A Dropout layer on attn_output_weights.
            Default: 0.0.
        proj_drop (float): A Dropout layer after `nn.MultiheadAttention`.
            Default: 0.0.
        dropout_layer (obj:`ConfigDict`): The dropout_layer used
            when adding the shortcut.
        init_cfg (obj:`mmcv.ConfigDict`): The Config for initialization.
            Default: None.
        batch_first (bool): When it is True,  Key, Query and Value are shape of
            (batch, n, embed_dim), otherwise (n, batch, embed_dim).
             Default to False.
    """

    def __init__(self,
                 embed_dims,
                 num_heads,
                 attn_drop=0.,
                 proj_drop=0.,
                 dropout_layer=dict(type='Dropout', drop_prob=0.),
                 init_cfg=None,
                 batch_first=False,
                 **kwargs):
        super().__init__(init_cfg)
        if 'dropout' in kwargs:
            warnings.warn(
                'The arguments `dropout` in MultiheadAttention '
                'has been deprecated, now you can separately '
                'set `attn_drop`(float), proj_drop(float), '
                'and `dropout_layer`(dict) ', DeprecationWarning)
            attn_drop = kwargs['dropout']
            dropout_layer['drop_prob'] = kwargs.pop('dropout')

        self.embed_dims = embed_dims
        self.num_heads = num_heads
        self.batch_first = batch_first

        self.attn = MultiheadAttentionx(embed_dims, num_heads)

        self.proj_drop = nn.Dropout(proj_drop)
        self.dropout_layer = build_dropout(
            dropout_layer) if dropout_layer else nn.Identity()


    @deprecated_api_warning({'residual': 'identity'},
                            cls_name='MultiheadAttention')
    def forward(self,
                query,
                key=None,
                value=None,
                identity=None,
                query_pos=None,
                key_pos=None,
                attn_mask=None,
                key_padding_mask=None,
                **kwargs):
        """Forward function for `MultiheadAttention`.

        **kwargs allow passing a more general data flow when combining
        with other operations in `transformerlayer`.

        Args:
            query (Tensor): The input query with shape [num_queries, bs,
                embed_dims] if self.batch_first is False, else
                [bs, num_queries embed_dims].
            key (Tensor): The key tensor with shape [num_keys, bs,
                embed_dims] if self.batch_first is False, else
                [bs, num_keys, embed_dims] .
                If None, the ``query`` will be used. Defaults to None.
            value (Tensor): The value tensor with same shape as `key`.
                Same in `nn.MultiheadAttention.forward`. Defaults to None.
                If None, the `key` will be used.
            identity (Tensor): This tensor, with the same shape as x,
                will be used for the identity link.
                If None, `x` will be used. Defaults to None.
            query_pos (Tensor): The positional encoding for query, with
                the same shape as `x`. If not None, it will
                be added to `x` before forward function. Defaults to None.
            key_pos (Tensor): The positional encoding for `key`, with the
                same shape as `key`. Defaults to None. If not None, it will
                be added to `key` before forward function. If None, and
                `query_pos` has the same shape as `key`, then `query_pos`
                will be used for `key_pos`. Defaults to None.
            attn_mask (Tensor): ByteTensor mask with shape [num_queries,
                num_keys]. Same in `nn.MultiheadAttention.forward`.
                Defaults to None.
            key_padding_mask (Tensor): ByteTensor with shape [bs, num_keys].
                Defaults to None.

        Returns:
            Tensor: forwarded results with shape
            [num_queries, bs, embed_dims]
            if self.batch_first is False, else
            [bs, num_queries embed_dims].
        """

        if key is None:
            key = query
        if value is None:
            value = key
        if identity is None:
            identity = query
        if key_pos is None:
            if query_pos is not None:
                # use query_pos if key_pos is not available
                if query_pos.shape == key.shape:
                    key_pos = query_pos
                else:
                    warnings.warn(f'position encoding of key is'
                                  f'missing in {self.__class__.__name__}.')
        if query_pos is not None:
            query = query + query_pos
        if key_pos is not None:
            key = key + key_pos

        # Because the dataflow('key', 'query', 'value') of
        # ``torch.nn.MultiheadAttention`` is (num_query, batch,
        # embed_dims), We should adjust the shape of dataflow from
        # batch_first (batch, num_query, embed_dims) to num_query_first
        # (num_query ,batch, embed_dims), and recover ``attn_output``
        # from num_query_first to batch_first.
        if self.batch_first:
            query = query.transpose(0, 1)
            key = key.transpose(0, 1)
            value = value.transpose(0, 1)

        out = self.attn(
            q=query,
            k=key,
            v=value)

        if self.batch_first:
            out = out.transpose(0, 1)

        

        return identity + self.dropout_layer(self.proj_drop(out))



